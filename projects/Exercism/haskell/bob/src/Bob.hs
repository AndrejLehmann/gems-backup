module Bob (responseFor) where

import Data.Char (isSpace)

--import Data.Text (Text)
--import qualified Data.Text as T

--module Main where

responseFor :: String -> String
responseFor xs
  | silence = "Fine. Be that way!"
  | yelling && question = "Calm down, I know what I'm doing!"
  | yelling = "Whoa, chill out!"
  | question = "Sure."
  | otherwise = "Whatever."
  where
    yelling = (and [x `elem` (['A' .. 'Z'] ++ ['1' .. '9'] ++ " ,!?%^@#$(*") | x <- xs]) && or [x `elem` ['A' .. 'Z'] | x <- xs]
    question = last xs == '?'
    silence = (and [x `elem` [' ', '\t'] | x <- xs]) || (xs == [])

--main :: IO ()
--main = print (responseFor "")
