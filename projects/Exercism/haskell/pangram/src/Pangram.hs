--module Pangram (isPangram) where
module Main where

import Data.Char (toLower)

isPangram :: String -> Bool
isPangram text = and [c `elem` lowerCaseText | c <- ['a' .. 'z']]
  where
    lowerCaseText = map toLower text

{- Other solutions

isPangram text = all (`elem` map toUpper text) ['A'..'Z'] -- src_haskell{all} verifies if all the elements from a list satisfy a certain condition. For example : all (<5) [1,2,3] == True

isPangram text = null (['a' .. 'z'] \\ (map toLower text)) -- src_haskell{\\} is the list difference operator.

-}

main :: IO ()
main = print (isPangram "The quick brown fox jumps over the lazy dog") -- True
