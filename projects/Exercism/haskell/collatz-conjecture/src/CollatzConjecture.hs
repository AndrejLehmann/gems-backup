-- module CollatzConjecture (collatz) where

module Main where

{- Alternatives:

-- my solution :

nextCollatzNum :: Integer -> Integer
nextCollatzNum n
  | even n = n `div` 2
  | otherwise = 3 * n + 1 -- odd n

collatzSequence :: Integer -> [Integer]
collatzSequence n
  | (n == 1) = []
  | otherwise = [n] ++ collatzSequence (nextCollatzNum n)

collatz :: Integer -> Maybe Integer
collatz n
  | (n <= 0) = Nothing
  | otherwise = Just . toInteger . length . collatzSequence $ n

-- apply src_haskell{succ} to the result of the recursive call :

collatz :: Integer -> Maybe Integer
collatz n | n <= 0 = Nothing
          | n == 1 = Just 0
          | even n     = succ <$> collatz (n `div` 2) -- src_haskell{<$>} is infix notation for src_haskell{fmap}
                                                      -- src_haskell{fmap} works like src_haskell{map} but is applicable also for Maybe types
          | otherwise  = succ <$> collatz (3 * n + 1)

-- with counter :

collatz :: Integer -> Maybe Integer
collatz n
  | n <= 0 = Nothing
  | otherwise = Just $ collatz' n 0
  where
  collatz' :: Integer -> Integer -> Integer
  collatz' 1 count  = count
  collatz' n count
    | even n = collatz' (n `div` 2) (count + 1)
    | otherwise = collatz' (n * 3 + 1) (count + 1)
-}

-- [[http://learnyouahaskell.com/higher-order-functions#curried-functions]]
chain :: (Integral a) => a -> [a]
chain 1 = []
chain n
  | even n = n : chain (n `div` 2)
  | odd n = n : chain (n * 3 + 1)

collatz :: Integer -> Maybe Integer
collatz n
  | n <= 0 = Nothing
  | otherwise = Just . toInteger . length . chain $ n

main :: IO ()
main = print . collatz $ 16
