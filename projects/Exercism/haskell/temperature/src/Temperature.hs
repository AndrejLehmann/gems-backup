--module Temperature (tempToC, tempToF) where
module Main where

{- converts Fahrenheit to Celsius -}
tempToC :: Integer -> Float
tempToC temp = fromInteger (temp - 32) / 1.8

{- converts Celsius to Fahrenheit -}
tempToF :: Float -> Integer
tempToF temp = ceiling (1.8 * temp + 32)

main :: IO ()
main = print (tempToC 32)
