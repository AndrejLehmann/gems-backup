--module LeapYear (isLeapYear) where
module Main where

isLeapYear :: Integer -> Bool
isLeapYear year = mod year 4 == 0 && ((mod year 100 /= 0) || (mod year 400 == 0))

{- alternative
isLeapYear yr = case (yr `mod` 4, yr `mod` 100, yr `mod` 400) of
                     (0, 0, 0) -> True
                     (0, 0, _) -> False
                     (0, _, _) -> True
                     (_,_,_)   -> False
-}

main :: IO ()
main = print (isLeapYear 2000)
