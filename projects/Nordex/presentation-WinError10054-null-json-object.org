#+title: Presentation Win Error10054 Null Json Object

* The Issue

- A python program (SAT) that sends a JSON file with data to a sever written in C#.
  - (Screenshot of the file)
- Server code receives the JSON file as null object most of the time but not always
- This issue come up rather recently so newest implementation was in question
  - (Screenshot of meta data in loads entry: json, model in python, C# class)
- But meta data in loads entry has the same structure as fatigue loads, list of dictionaries
- So me and Aarthy did the implementation for meta data in the loads entry as for fatigue loads

* What we tried

- Checked if we really did everything the same as for fatigue loads
- Checked if json file has the corrected syntax
  - In python, you actually do it by just opening the file with =open('filename.json'. 'r')=.
- Checked if the server code actually receives the json file
  - In C# code changed =Roobobject= to =JsonElement= in the signature of the post method:
    =public IActionResult Post([FromBody] Rootobject rootobject) {...}= -> =public IActionResult Post([FromBody] JsonElement rootobject) {...}=
- Removed SAT from process:
 =$ curl -X POST -H "Content-Type: application/json" --ntlm --user : https://localhost:44397/api/v1/Turbine/ -d @"C:\Users\LehmannA\PycharmProjects\notes\projects\B-1000-1158-ALR\input.json" -x ""=
- Commented out meta data in the loads entry
- Reduced data in the json file to a minimum
#+begin_src json
{
 "Version": 0,
 "Comment": "Initial Version",
 "Transaction": 934259357
}
#+end_src
- Changed the entries and implemented new class in C# for them
#+begin_src json
{
 "a": 1,
 "b": "abc",
 "c": 2
}
#+end_src

- NOW IT WORKED!

* Solution

The problem was casting a large integer value for =transaction= in =class Meta= that is too large for an =int=.
In python this function was used for generating values for transaction ID

#+begin_src python :results output :exports both
transaction = uuid.uuid1().fields[0]
#+end_src

=uuid.uuid1()= generates a UUID (Universally Unique Identifier) using a host ID, sequence number, and the current time.
It produces values in the range of 32-bit unsigned integer. In python there is no data type for unsigned integer,
just =int=. In Python, =int= are objects that can grow as large as your system’s memory can store, then it start
swapping. So sometimes the values generated was too large for an =int= in C#. So the whole object was set to =null= by
C#.
