:PROPERTIES:
:ID:       8d4dca7f-2733-48b9-801d-af5f7bf1a974
:END:
#+title: Total and Partial Derivatives
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Ambiguity with partial $\partial$ notation [cite:@ambiguity-with-partial-derivative-notation]

** $z(x,y)$, $x(t),y(t)$

\begin{align*}
    \frac{\d z}{\d t} = \frac{\partial z}{\partial x}\frac{\d x}{\d t} + \frac{\partial z}{\partial y}\frac{\d y}{\d t}
\end{align*}

** $z(x,y)$, $x(s,t),y(s,t)$

\begin{align*}
    \frac{\partial z}{\partial t} = \frac{\partial z}{\partial x}\frac{\partial x}{\partial t} + \frac{\partial z}{\partial y}\frac{\partial y}{\partial t}
\end{align*}

** $u(x,y,t)$, $x(t),y(t)$

\begin{align*}
    \frac{\d u}{\d t} = \frac{\partial u}{\partial t} + \underbrace{\frac{\partial u}{\partial x}\frac{\d x}{\d t} + \frac{\partial u}{\partial y}\frac{\d y}{\d t}}_{(\nabla_{x,y} \,u) \,\cdot\, v}
\end{align*}

E.g. Consider $u$ being temperature of a plate
The temperature changes spatially in the $x\text{-}y$ plane and in time $t$.
Further $x(t)$ and $y(t)$ denote your position while you move with velocity $v$.
So the total change in the temperature experienced by you depends on temperature change in time and your velocity.

** Ambiguity
*** $u(x,y,t,z)$, $x(t),y(t)$

Now $\partial u / \partial t$ is ambiguous.
Does it mean derivative with respect to only explicit dependence of $u$ on $t$ or also implicit through $x$ and $y$?
*** Resolutions
***** 1
\begin{align*}
    \frac{\partial u(x,y,t,z)}{\partial t}
\end{align*}
means derivative with respect to only explicit dependence on $t$
And
\begin{align*}
    \frac{\partial u(x(t),y(t),t,z)}{\partial t}
\end{align*}
means derivative with respect to $t$ explicitly and implicitly through $x$ and $y$ as well.

***** 2
\begin{align*}
    \left( \frac{\partial u}{\partial t} \right)_{x,y}
\end{align*}
means derivative with respect to only explicit dependence on $t$, since $x$ and $y$ are held constant
And
\begin{align*}
    \frac{\partial u}{\partial t}
\end{align*}
means derivative with respect to $t$ explicitly and implicitly through $x$ and $y$ as well.

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
