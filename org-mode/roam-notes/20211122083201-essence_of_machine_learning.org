:properties:
:ID:       6fc424f1-638e-4127-b17e-22d468c0643f
:end:
#+title: Essence of machine learning
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :data-science:

- [ ] A pattern exists
      But even if a pattern does not exists the learning algorithm will tell you that no pattern is found.
      So no harm done.
- [ ] We can not pin it down mathematically
      But even if we could pin it down mathematically one still can apply a learning model.
      But that won't be the optimal way to solve the problem since mathematical approach is the optimal one.
- [ ] Data availability

\begin{tikzpicture}[node distance=2cm]

\node (TargetFunc) [stdnode] {\begin{tabular}{c} unknown target function \\ $f: X \to Y$ \end{tabular}};
\node (TrainingExamples) [stdnode, below of=TargetFunc] {\begin{tabular}{c} training examples \\ $(\vec{x}_1,\vec{y}_1), \dots, (\vec{x}_N,\vec{y}_N)$ \end{tabular}};
\node (Algorithm) [stdnode, below right of=TrainingExamples, node distance=3.0cm] {\begin{tabular}{c} learning algorithm  \\ $A$ \end{tabular}};
\node (HypothesisSet) [stdnode, below left of=Algorithm, node distance=3.0cm] {\begin{tabular}{c} hypothesis set  \\ $H$ \end{tabular}};
\node (FinalHypothesis) [stdnode, right of=Algorithm, node distance=4cm] {\begin{tabular}{c} final hypothesis \\ $g \approx f$ \end{tabular}};

\draw [arrow] (TargetFunc) -- (TrainingExamples);
\draw [arrow] (TrainingExamples) |- ([yshift=+3mm]Algorithm);
\draw [arrow] (HypothesisSet) |- ([yshift=-3mm]Algorithm);
\draw [arrow] (Algorithm) -- (FinalHypothesis);

\node[draw, dotted, fit=(HypothesisSet) (Algorithm), label={[shift={(1.5,-2.2)}]learning model}] {};

\end{tikzpicture}
