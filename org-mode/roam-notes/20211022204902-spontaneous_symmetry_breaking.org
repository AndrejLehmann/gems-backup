:properties:
:ID:       d7c7e167-ea46-421a-af23-a48e3a0e82a3
:end:
#+title: Spontaneous symmetry breaking
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

The fundamental equations of a system evince a symmetry that is not present in the ground state.

* E.g. Ferromagnet:

The Hamiltonian is given by
\begin{align*}
    H = - \sum_{i,j} J_{ij}\, \vec{S}_i \vec{S}_j
\end{align*}
which is rotationally invariant.
However in the ground state (temperature $T < T_{\rm c}$ ) magnetic moments spontaneously align in some direction.

If a phase preserves all symmetries it can only be identified by it's responses (e.g. simple metal [$\mathrm{Cu}$], simple insulator [$\mathrm{Si}$]).
