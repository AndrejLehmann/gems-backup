:PROPERTIES:
:ID:       60295158-faf1-4ac3-b3cf-820ca8786e99
:END:
#+title: Cpp: Compiler and linker
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:cpp:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* General

Compiler looks only for declarations of functions and does not look whether you actually implemented them.
Linker is the one who looks for the implementations of functions.
For the linker the implementations need to be just in some file in the project.

* Compiler [cite:@cpp-compiler-cherno]

Compiler also executes the preprocessor directives.
The declaration (in the header files) are for the compiler (not for the linker).
And he optimizes the code.
E.g.

#+begin_src cpp
int function(int a, int b){
    int res = a + b ;
    return res ;}
#+end_src

becomes

#+begin_src cpp
int function(int a, int b){
    return a + b ;}
#+end_src

Or

#+begin_src cpp
int fuction(){
    return 5 + 2 ;}
#+end_src

becomes

#+begin_src cpp
int function(){
    return 7 ;}
#+end_src

* Linker [cite:@cpp-linker-cherno]

If some function is used only in the file where it is defined, one can write the key word src_cpp{static} in the head of the function to specify that for the linker.

** Multiple definitions error

add.hpp:
#+begin_src cpp
int add(int a, int b){
    return a + b;
}
#+end_src

math.cpp:
#+begin_src cpp
#include  "add.hpp"

void SomethingWithAdd(){
    // some code
}
#+end_src

main.cpp:
#+begin_src cpp
#include "add.hpp"

int main(){
    int sum = add(1, 3);
}
#+end_src

Since src_cpp{#include} copy pastes code, linker finds 2 definitions of src_cpp{int add(int a, int b)}, one in math.cpp and one in main.cpp


#+begin_export latex
    \clearpage
#+end_export
* References
#+print_bibliography:
