:PROPERTIES:
:ID:       e6961022-285c-428f-a585-f5a14e982816
:END:
#+title: Wannier functions fuer Mo's bachelor thesis
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :Mos-bachelor-thesis:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

For the materials with the wide spread s- and p-shell valence electrons the extended Bloch states are well suited as the orthogonal one particle basis.
In this case the long ranged Coulomb electron-electron interaction is well screen and one can approximate the system as a collection of non-interacting electron.
But for systems with d- and f-shell valence electrons the electron wave functions are rather localized, the Coulomb interaction is not well screened and remains rather sizable.
The simultaneous presence of the non-negligible Coulomb interaction and the kinetic energy of the electrons give rise to correlation effects that is the focus of this thesis.
We (will) discuss(ed) correlation effects in more detail in the next (previous) section.
For this cases, where the repulsion of the Coulomb interaction causes the electrons to localize, one can choose as orthogonal one particle basis the exponentially localized Wannier functions, which more closely resemble the underlying physical behavior of the electrons.
Wannier functions in generall $w_n$ are defined as Fourier transform of the Bloch functions $\psi_{n,\vec{k}}$
\begin{align*}
    w_n(\vec{r-R}) = \frac{V}{(2\pi)^d} \sum_{\vec{k} \in {\rm BZ}} \e^{-i\vec{k}\vec{R}} \; \psi_{n,\vec{k}}(\vec{r})
\end{align*}
where $d$ is dimension.
To obtain exponentially localized Wannier functions we can use that this definition is not unique and one can perform gauge transformation to the Bloch states.
For each $\vec{ k}$ we can choose a phase $\phi(\vec{k})$ of the Bloch function and apply the gauge transformation $\psi_{n,\vec{k}}(\vec{r}) \to \e^{i\phi(\vec{k})}\psi_{n,\vec{k}}(\vec{r})$ such that
Wannier functions are exponentially localized.
One can show that, since Bloch functions are translationally symmetric, Wannier functions at different unit cells are orthogonal to each other.
The reverse transformation is accordingly defined as
\begin{align*}
    \psi_{n,\vec{k}}(\vec{r}) = \sum_{\vec{R}} \e^{i\vec{k}\vec{R}} \, w_n(\vec{r-R}) \, .
\end{align*}

In order to generalize the definition of Wannier functions for a multi-band system, multiple Wannier and Bloch functions are composed into vectors $\vec{w}=\left( w_{n_{\rm min}}, \dots , w_{n_{\rm max}} \right)^{\mathrm{T}}$ and $\vec{\psi}_{\vec{k}} = \left( \psi_{n_{\rm min},\vec{k}}, \dots, \psi_{n_{\rm max},\vec{k} \right)^{\mathrm{T}}$ and the definition is given by
\begin{align*}
    \vec{w}(\vec{r-R}) = \frac{V}{(2\pi)^d} \int_{\rm BZ} d\vec{k} \; \vec{U}(\vec{k}) \vec{\psi}_{\vec{k}}(\vec{r})
\end{align*}
where $U(\vec{k})$ are a unitary matrices.
Here one can choose $U(\vec{k})$ in order to localize the Wannier functions.[cite: @max-loc-wannier-func-for-composite-bands, @wannier90-package]
And the inverse relation is given by a quasi-inverse transformation
\begin{align*}
    \tilde{\psi}_{\vec{k}}(\vec{r}) = \sum_{\vec{R}} \e^{i\vec{k}\vec{R}} \, \vec{w}(\vec{r-R})
\end{align*}
where the quasi Bloch functions $\tilde{\psi}_{\vec{k}}(\vec{r})$ even though are not eigenstates of the Hamiltonian but still exhibit the translation invariance of the Bloch states $u_{n,\vec{k}}(\vec{r}) = u_{n,\vec{k}}(\vec{r+R})$.

#+begin_export latex
  \clearpage
#+end_export
* References
#+print_bibliography:
