:PROPERTIES:
:ID:       20b6d804-85af-4a3f-934a-76979832ebca
:END:
#+title: Haskell: usage in org-mode
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:haskell:org-mode:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* Backticks src_haskell{``}

deactivate org-cdlatex-mode with =M-x org-cdlatex-mode= so you can type the backtick =`= for infix functions

* Writing multiline Haskell code [cite: @haskell-with-org-mode]

The default way that org-babel compiles your code is using GHCi, so if you have to write a multiline code, then you need to do it as if we were inside a GHCi buffer:

#+begin_src haskell
:{
-- a very verbose way to sum a sequence of numbers:
sumInts :: Int -> Int -> Int
sumInts a b =
  if a == b
    then b
    else (+ a) $ (sumInts (a + 1) b)
:}

map (\[a,b] -> sumInts a b) [[0, 1] , [1, 3], [1,5], [2,10]]
#+end_src

#+RESULTS:
: Prelude> [1,6,15,54]

i.e. we need to put the multiline part of the code inside ={:{ :}= and what we want to be on the output on the last line.
Also, it is important to note that, since it is running inside a GHCi, we will only see the result of the last call.

#+begin_export latex
    \clearpage
#+end_export
* References
#+print_bibliography:
