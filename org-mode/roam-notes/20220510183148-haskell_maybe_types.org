:PROPERTIES:
:ID:       126843d2-e37b-44f5-aa0b-3f4b09b731f3
:END:
#+title: Haskell: Maybe types
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:haskell:

/A value of type =Maybe= a represents a value of type a with the context of possible failure attached.
/A value of =Just= "dharma" means that the string "dharma" is there whereas a value of =Nothing= represents its absence,/
/or if you look at the string as the result of a computation, it means that the computation has failed./ [cite: @learn-you-a-haskell]

Compare

#+begin_src haskell :exports both
3/0
#+end_src

#+RESULTS:
: Infinity

#+begin_src haskell :exports both
3 `div` 0
#+end_src

#+RESULTS:
: *** Exception: divide by zero

#+begin_src haskell :session :exports both
:{
divide a b
  | (b == 0) = Nothing
  | otherwise = Just $ a / b
:}
3 `divide` 0
#+end_src

#+RESULTS:
: Nothing

* Further reading

- [[id:c82cbe25-4684-41e9-890a-ec4980bb9546][Haskell: fmap]]
