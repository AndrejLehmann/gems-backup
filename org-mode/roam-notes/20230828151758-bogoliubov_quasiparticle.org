:PROPERTIES:
:ID:       19cb7825-9a36-432b-9107-0cc10a3548db
:END:
#+title: Bogoliubov quasiparticle
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:physics:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Bogoliubov quasiparticle [cite:@Bogoliubov-quasiparticle-wiki]

/Bogoliubov quasiparticle or Bogoliubon is a quasiparticle that occurs in superconductors./
/Whereas superconductivity is characterized by the condensation of Cooper pairs into the same ground quantum state, Bogoliubov quasiparticles are elementary excitations above the ground state, which are superpositions (linear combinations) of the excitations of negatively charged electrons and positively charged electron holes, and are therefore neutral spin-1/2 fermions./
/Sometimes these quasiparticles are also called Majorana modes, in analogy with the equations for Majorana fermions./

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
