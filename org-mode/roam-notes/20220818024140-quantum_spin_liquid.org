:PROPERTIES:
:ID:       ad3e577c-a3cb-4108-b2d6-ffd2f1baf500
:END:
#+title: Quantum spin liquid
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:experiments:materials:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

- phase where spins are long range entangled but not long range ordered
- in contrast to a triangular structure, which is edge sharing, kagome structure is corner sharing.
  A corner sharing structure has higher frustration.

* Physical realization of a quantum spin liquid based on a complex frustration mechanism [cite:@Ca10Cr7O28-bilayer-spin-liquid]

Spin liquid material with new kind of frustration.
Read article.

#+caption: Snapshot of the fluctuating ground state spin arrangement for one bi-triangle consisting of a ferromagnetic upper triangle coupled to an antiferromagnetic lower triangle. This corresponds to the frustrated unit in ${\rm Ca_{10}Cr_{7}O_{28}}$.
#+attr_org: :width 300
#+attr_latex: :width 10cm :placement [!htpb]
#+name:   fig:frustration-in-Ca10Cr7O28-spin-liquid
[[./images/frustration-in-Ca10Cr7O28-spin-liquid.png]]

* Quantum spin liquids: a review [cite:@quantum-spin-liquid-review]

- read whole paper

** Introduction

- /A spin liquid is a system of spins which are highly correlated with one another due to their mutual interactions, yet do not order even at very low or zero temperature./
  /This view includes classical or effectively classical magnets in which ordering is prevented by thermal fluctuations owing to a high degree of degeneracy./
  /This definition of a spin liquid is inadequate and unsatisfying, insofar as it is mainly a statement of what these spins do not do./
  /It is actually hard to agree on a positive definition of a QSL./
  /Here, we will take the point of view that the essential ingredient of a QSL is not the lack of order, but the presence of an anomalously high degree of entanglement, or massive quantum superposition./
  /QSLs are exemplars of highly entangled quantum matter in the context of magnetism./
  /By a highly entangled state we roughly mean one which is not smoothly connected to a product state over any finite spatial blocks./
- /This many-body entanglement support non-local excitations/
  /That is, excitations which individually cannot be created by any local operator, but only by an infinite product of local operators, can have finite energy and even behave as sharp quasiparticles./
- /The term ‘quantum spin liquid’ (QSL) originates from Anderson’s 1973 paper describing a ‘quantum liquid’ ground state of an antiferromagnet, which he called a ‘resonating valence bond’ (RVB) state./ [cite:@quatum-spin-liquid-philip-anderson]
  RVB is a quantum superposition of different pairings of spins into singlets. (But vague formulation of the state.)
  He suggested that RVB may /describe the spin-1/2 Heisenberg antiferromagnet on the triangular lattice, which in the end proved not to be correct/.
- 1987 /QSLs experienced a revival in interest, stimulated by the discovery of high-temperature superconductivity./
  /Anderson, and Baskaran, Zou proposed a specific form of RVB state by beginning with a wavefunction for fermions with spin and projecting it, using a method developed by Gutzwiller, to the spin subspace./
  /These works suggested superconductivity might emerge by doping a QSL, and also identified the underlying 'pseudo-fermions', now usually called ‘spinons’, as possible quasiparticles of the QSL: spin-1/2 electrically neutral fermions./
- /In parallel, Kalmeyer and Laughlin proposed a 'chiral spin liquid' state, which connected QSLs to the fractional quantum Hall effect (FQHE), and thereby to topology./
- Skepticism on possibility of experimental realization
- /Kitaev realized that topological phases could be important for quantum computation, and introduced his toric code model./
- /A few years later, Kitaev's honeycomb model appeared, which provided a simple Hamiltonian with nearest-neighbor interactions between spin-1/2 spins with an exact solution and a stable gapless QSL phase./

** Highly entangled quantum matter

- /we look for ground states that cannot be continuously deformed into a product state while staying within the phase./
  /One possible but restrictive definition is to say that deformations correspond to continuous changes of a ground state wavefunction in response to variation of parameters of a *local* Hamiltonian, such that throughout the variation a non-zero gap is maintained above the ground state./
  /We will also later encounter other phases which are gapless, so this definition is inadequate./
  /However, rigorous definitions are for mathematicians, not physicists, so we do not worry about this further./

*** Example: the toric code

- /Topological phases are by far the most studied and best understood examples of highly entangled quantum phases, and the subject is quite evolved./

* A field guide to spin liquids [cite:@field-guide-to-spin-liquids-review]
** The quest for spin liquids
- /frustrated interactions for which the corresponding classical spin systems display a large ground state degeneracy, because the local energetics cannot be minimized in a unique way/

*** What Is a Spin Liquid?

- /A constitutive concept for a spin liquid is the absence of magnetic order of a system of interacting spins at temperatures smaller than the interaction scale/
- /Akin to the Mermin–Wagner theorem, which forbids spontaneous breaking of continuous symmetry (SSB or spontaneous symmetry breaking) at finite temperatures in dimensions $d \le 2$, there is a rigorous result for spin systems with short-range interactions./
  /For systems with half-odd integer spin per unit cell, hence proper Mott insulators, and without symmetry breaking, the Lieb-Schultz–Mattis theorem states that the ground state is either unique with gapless excitations or degenerate with a gap to excitations./
  /It establishes to some level of mathematical rigor the possibility of gapless QSLs or gapped ones with topological order./
- /fractionalized excitations and emergent gauge fields are intimately linked because standard spin flip excitations always lead to integer changes of the total spin./
  /Therefore, excitations labeled by fractions of such quantum numbers, e.g., quasiparticles carrying half-integer spin, need to be created in even numbers and, once they are separated, we can think of the emergent background gauge field as taking care of the global constraint./

*** How to Tell One, as a Matter of Principle ...

- /When a pair of Laughlin quasiparticles of charge $\pm e/3$ is created from a ground state, and one member of the pair moves around a noncontractible loop of the torus before annihilating the other, the system moves from one ground state to another./
  /Only once three such particles, and hence an electron with unit charge, have made such a trajectory does the system return to the original ground state./
- /In analogy to long-range order of conventional phases the long-range entanglement can serve as an order parameter of topological phases./
  /The entanglement entropy of a ground-state wave function can be calculated from a reduced density operator with one part of the total degrees of freedom of a bipartitioned system traced out (with a smooth boundary of length $L$ separating the two regions)./
  /For gapped phases it follows a universal scaling form, $S = cL - \gamma + \dots$ ./
  /$\gamma$ quantifies the long-range entanglement (it is independent of the length of the boundary of the partition)./
  /It is only nonzero in a topological phase and is directly related to the emergent gauge structure of the QSL phase./

*** ... And in Practice

- /Above, we called these diagnostics comparatively straightforward because the experimental situation is considerably less promising./

* Further reading

- [cite:@classical-spiral-spin-liquids]
- [cite:@quantum-spin-liquid-a-tail-of-emergence-from-frustration]
- [cite:@an-end-to-the-drought-of-quantum-spin-liquids]
- [[https://www.youtube.com/watch?v=rcTWMFnYQ_M&t=666s][Quantum spin liquid - Frederic Mila - Lecture]]
- [[https://www.youtube.com/watch?v=CUg-sGeuJA8&t=303s][Prof. Leon Balents: "Spin Liquids" - Lecture]]

* References
#+print_bibliography:
