:PROPERTIES:
:ID:       b13a32e3-f05d-4587-8a49-7ebae0b00674
:END:
#+title: C#: deconstructor
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :C#:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Used for unpacking classes field values and assigning them.

#+begin_src csharp
var john = new Person("John Doe", 30);
var (name, age) = john; // Deconstructing the 'john' object
#+end_src


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
