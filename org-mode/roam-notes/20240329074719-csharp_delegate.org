:PROPERTIES:
:ID:       9f47c56b-00c2-4c2e-9da9-a80331cc936c
:END:
#+title: C#: =delegate=
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :C#:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Similar to function pointers in C++, however, delegates are type-safe and secure.
Delegates are the basis for Events.


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
