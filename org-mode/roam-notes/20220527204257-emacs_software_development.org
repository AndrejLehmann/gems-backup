:PROPERTIES:
:ID:       ced4a35e-e3b4-4527-817e-71cccae2a471
:END:
#+title: Emacs: software development
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :emacs:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Development on emacs

#+caption: Emacs functions and functionalities for software development. This functionalities have been tested for python. See [cite:@emacs-IDE-python] for reference and more (pytests, debugging, virtual enviroment, ...)
| bindings                               | function / functionality                                                      |
|----------------------------------------+-------------------------------------------------------------------------------|
| ALT+;                                  | in normal mode: add comment at the end of a line                              |
|                                        | in visual mode: comment out                                                   |
| SPC c l                                | lsp prefix                                                                    |
| auto loaded in config.el               | src_elisp{lsp-headerline-breadcrumb-mode} shows where you are in the file     |
| C-c ! l                                | flycheck-list-errors                                                          |
| SPC c l g r                            | lsp find references                                                           |
| SPC c l r r                            | lsp rename (more intelligent than just replacing string everywhere)           |
|                                        | signature and arguments explanation when typing function and paranthesis      |
| src_elisp{lsp-treemacs-symbols}        | hierarchically shows symbols                                                  |
| src_elisp{dap-breakpoint-toggle}       | add / removes breakpoint                                                      |
| src_elisp{dap-debug}                   | after setting a break point one can use the debug UI at the top of the screen |
| src_elisp{dap-ui-repl}                 | after setting a break point and running src_elisp{dap-debug}                  |
|                                        | one can open repl and e.g. print the values of variables                      |
| SPC c l d                              | dap-hyra (debugging short cuts)                                               |
| src_elisp{lsp-ui-doc-focus-frame}      | jump to the doc frame                                                         |
| src_elisp{lsp-ui-doc-unfocus-frame}    | jump back from the doc frame                                                  |
| src_elisp{lsp-ui-peek-find-references} | shows two in-buffer frames with a list of references (right)                  |
|                                        | and a corresponding peek of the code for the selected reference               |
| src_elisp{lsp-ivy}                     | type symbol, ivy searches for it and then you can jump to it                  |

- [ ] more functionalities in [[https://www.youtube.com/watch?v=0bilcQVSlbM][System Crafters dap-mode]]
- [ ] Check this functionalities with what just doom emacs provides.
      by commenting out config.el LSP concerning lines 407-451, 457-466.

** Edit debug configuration

Run src_elisp{dap-debug-edit-template}.
Then choose template.
Edit like change name, the working directory src_elisp{:cwd}, ...
Save buffer.
Then it should appear when run src_elisp{dap-debug}.

#+begin_export latex
  \clearpage
#+end_export

** Virtual enviroment

One can create a directory as virtual enviroment, so the project dependencies are installed there without polluting your global installation of packages.
Once the directory with virtual enviroment is created (e.g. with src_bash{virtualenv .env}) and activated (e.g. with src_bash{. .env/bin/activate}), one can run src_elisp{pyvenv-activate} and choose the directory with the enviroment.
That will then be passed to src_elisp{lsp-mode} and src_elisp{dap-mode}.

** Issues
**** TypeError

Error:
#+begin_quote
"/usr/lib/python3.10/site-packages/debugpy/_vendored/pydevd/_pydevd_bundle/pydevd_net_command_factory_json.py", line 281, in make_get_thread_stack_message end = min(start + levels, total_frames) TypeError: unsupported operand type(s) for +: 'NoneType' and 'int'
#+end_quote

From [[https://github.com/emacs-lsp/dap-mode/issues/625][github dap-mode issues]]:

#+begin_quote
Looks like it's related to this upstream issue(/pull request to fix that issue) with debugpy: [[https://github.com/microsoft/debugpy/pull/928]]

I used PIP to install at the exact revision of the fix:
pip install git+https://github.com/microsoft/debugpy.git@78b030f5092d91df64860914962333e89852ea9b

Tested dap-debug with a python file and was SUCCESSFUL! So the issue with this TypeError stack-trace is upstream and resolved :)
#+end_quote


** LSP
[[https://emacs-lsp.github.io/lsp-mode/tutorials/how-to-turn-off/][How to turn off features]]

** TRAMP and LSP (Didn't work on physnet)

For remote editing one can use [[id:f0a3ac8c-a19f-47f6-9b63-be4d3899b798][TRAMP]].
TRAMP on emacs is faster then [[id:e28ccfd2-1319-42d2-8f27-aeca5e5ab97e][sshfs]].
For IDE like support one has to [[https://emacs-lsp.github.io/lsp-mode/page/languages/][install LSP]] (Language Server Protocol) for the specific language on the remote maschine.
And in config.el [[https://emacs-lsp.github.io/lsp-mode/page/remote/][edit the configurations like]]
#+begin_src elisp
(require 'tramp)
(setq tramp-default-method "ssh")
(add-to-list 'tramp-remote-path 'tramp-own-remote-path) ; so TRAMP finds lsp binary via $PATH in .profile file of the remote maschine.
                                                        ; src_elisp{'tramp-own-remote-path} stores $PATH of the remote maschine, which is added to src_elisp{'tramp-remote-path}.
                                                        ; Alternative one can try to set src_elisp{'tramp-remote-path} to some thing like "/ssh:alehmann@login1.physnet.uni-hamburg.de:/path/to/bin".
                                                        ; But it didn't quite work out when I tried.

(use-package! lsp-mode
  :config
  (lsp-register-client
    (make-lsp-client :new-connection (lsp-tramp-connection "<binary name (e. g. pyls, pyright-langserver)>")
                     :major-modes '(python-mode)
                     :remote? t
                     :server-id 'pyls-remote))) ; or 'pyright-remote
#+end_src

*** physnet issue

But for some reason on physnet the LSP server dies when I start lsp mode by opening a python file or src_elisp{M-x lsp} .
Maybe there is conflict between python2 and python3.
Or I have to ssh to the compile node via TRAMP not the login node, which doesn't work by just /ssh:alehmann@compile1.physnet.uni-hamburg.de: .
Or the above configuration is wrong.
Who knows ...

** TRAMP, remote shell, X11, debugger on a remote host, ...

[[https://www.gnu.org/software/emacs/manual/html_node/tramp/Remote-processes.html#Running-shell-on-a-remote-host][Integration with other Emacs packages]]


#+begin_export latex
    \clearpage
#+end_export

* References
#+print_bibliography:
