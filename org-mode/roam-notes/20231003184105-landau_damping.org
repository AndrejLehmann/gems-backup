:PROPERTIES:
:ID:       d7e4056c-92e4-4e38-ac84-61d4721caacc
:END:
#+title: Landau damping
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:physiscs:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Introduction to Landau damping [cite:@introduction-to-Landau-damping]

/Landau damping is damping of a collective mode of oscillations of the density of charged particles (plasma) with long-range interactions without collisions of charged particles./
Consider electrons on the background of fixed in place positive ions, the plasma.
When displacing the electrons out of equilibrium (by a small amount), [[fig:out-of-equilibrium-plasma][the resulting electric field]] acts as a restoring force on the electrons.
So the particles producing the field are influenced by that field and the change of their behavior changes the field.
So we can expect a self-consistent treatment.

#+caption: /Plasma with disturbance and restoring field./
#+attr_org: :width 600
#+attr_latex: :width 10cm :placement [!htpb]
#+name:   fig:out-of-equilibrium-plasma
[[./images/out-of-equilibrium-plasma.png]]

This can form a standing wave with frequency $\omega_{\rm p}^{2} = \frac{n e^{2}}{m\epsilon_0}$ with $n$ the density of electrons, $e$ the electric charge, $m$ the effective mass and $\epsilon_0$ the permittivity of free space.
We can write this mode as
\begin{align*}
    E(x,t) = E_{0} \,\e^{\i(kx - \omega t)}
\end{align*}
and the wave (phase) velocity is given by $v = \omega/k$.
If $\omega = \omega_r + \i \omega_i$ is complex we can write the mode as
\begin{align*}
    E(x,t) = E_{0} \,\e^{\i(kx - \omega_{r} t)} \, \e^{\omega_{i} t}
\end{align*}
So if $\omega_i < 0$ the oscillation is damped.
Since the velocities of particles are different forming a distribution we utilize the Liouville equation.
Given a density distribution function $\psi(\vec{q},\vec{p},t)$ with $\int \psi(\vec{q},\vec{p},t)\,\d q^n \d p^n = N$
\begin{align*}
    \frac{\d \psi}{\d t} = \frac{\partial \psi}{\partial t} + \sum_{i} \left( \frac{\partial \psi}{\partial q_{i}} \dot q_{i} + \frac{\partial \psi}{\partial p_{i}} \dot p_{i} \right) = 0
\end{align*}
From that we obtain the Boltzman equation
\begin{align*}
    \frac{\d \psi}{\d t} = \frac{\partial \psi}{\partial t} + \vec{v} \frac{\partial \psi}{\partial \vec{x}} + \frac{1}{m} \vec{F}(\vec{x},t) \frac{\partial \psi}{\partial \vec{v}} \; + \underbrace{\Omega(\psi)}_{\text{collisions}}
\end{align*}
By ignoring collisions we get the Vlasov equation
\begin{align*}
    \frac{\d \psi}{\d t} = \frac{\partial \psi}{\partial t} + \vec{v} \frac{\partial \psi}{\partial \vec{x}} + \frac{1}{m} \vec{F}(\vec{x},t) \frac{\partial \psi}{\partial \vec{v}} = 0
\end{align*}
$\vec{F}$ is the force of the field on the particles.
By considering the following the relations
\begin{align*}
    &\vec{F} = e \cdot \vec{E} \\
    &\vec{E} = - \nabla \Phi \\
    & \Delta \Phi = - \frac{\rho}{\epsilon_{0}} = - \frac{e}{\epsilon_{0}} \int \!\psi \;\d v
\end{align*}
we see that the force/field produced by the distribution $\psi$ in turn depends on $\psi$ itself.
We make the ansatz
\begin{align*}
    \psi(\vec{x},\vec{v},t) = \psi_{0}(\vec{v}) + \psi_{1}(\vec{x},\vec{v},t)
\end{align*}
where $\psi_0$ is the stationary distribution and $\psi_1$ non-stationary perturbation,
so that
\begin{align*}
    \frac{\d \psi}{\d t} = \frac{\partial \psi_{1}}{\partial t} + \vec{v} \frac{\partial \psi_{1}}{\partial \vec{x}} + \frac{e}{m} \vec{E}(\vec{x},t) \frac{\partial \psi_{0}}{\partial \vec{v}} = 0
\end{align*}
and
\begin{align*}
    \Delta \Phi = - \frac{\rho}{\epsilon_{0}} = - \frac{e}{\epsilon_{0}} \int \!\psi_{1} \;\d v
\end{align*}
and we need to find the solution for $\psi_1$.
Landau (unlike Vlasov) recognized that the solution is an initial-value problem (in particular for $x=0, v'=0$).
His approach was to write $\psi_1$ and $\vec{E}$ as Fourier transform in the space domain and as Laplace transform in the time domain (instead of $\psi_1$ and $\Phi$ as double Fourier transform in space and time like Vlasov)
and insert them in the Vlasov equation obtaining the dispersion relation
\begin{align*}
    1 + \frac{\omega^{2}_{\rm p}}{k} \left( \mathcal{P} \int  \frac{\partial \psi_{0} / \partial v }{\omega - k v} \d v - \underbrace{ \frac{\i \pi}{k} \left( \frac{\partial \psi_{0}}{\partial v} \right)_{v=\omega/k} }_{\text{damping}} \right) = 0
\end{align*}
where $\mathcal{P}$ is the [[https://en.wikipedia.org/wiki/Cauchy_principal_value][Cauchy principle value integral]] (i.e. $\int_{a}^b 1/\omega \; \d \omega = \lim_{\varepsilon \to 0^+} \int_{a}^{-\varepsilon} 1/\omega \; \d \omega + \int_{\varepsilon}^{b} 1/\omega \; \d \omega = 0$).
The damping term results from the initial conditions (which was missing in Vlaskov solution and lead to divergence for $\omega = kv$).
So the Landau damping occurs when $\left( \frac{\partial \psi}{\partial v} \right)_{v=\omega/k} < 0$.
This can be interpreted as following.
When plotting the velocities distribution, the slope being negative at the wave (phase) velocity means that the wave velocity is on the "right side" of the distribution and there are more particles that move slower than the wave absorbing the energy from the mode and damping it.


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
