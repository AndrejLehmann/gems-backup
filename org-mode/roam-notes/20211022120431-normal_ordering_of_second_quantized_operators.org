:properties:
:ID:       b0d0521c-534d-4ec4-bf1c-4bdf5a896f92
:end:
#+title: Normal ordering of second quantized operators
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* src [[https://www.youtube.com/watch?v=bv77qlOkIEIk][Pretty Much Physics - YT]]

- Why interesting?
  + $\bra{0}\mathopen:\widehat{O}\mathclose:\ket{0}=0$ since $\bra{0}a^{\dagger} \dots = 0$ and $\dots a \ket{0} = 0$
  + [[id:a2a95c6b-32a1-4c8f-8283-f6b1885a65a8][Wick's theorem]]: $\mathcal{T}(\dots) = \;\, : \dots + \text{all contractions} :$

- How to?
  + exchange $a$'s and $a^{\dagger}$'s while pretending that all (anti)commutantors are 0.
    Pretending than (anti)commutators are 0 is important to avoid contradictions like $a^{\dagger}a = \mathopen: a a^{\dagger} \mathclose: = \mathopen: a^\dagger a + 1 \mathclose: = \mathopen: a^\dagger a \mathclose: + \mathopen: 1 \mathclose: = a^{\dagger}a + 1$.
    So define $\mathopen: a a^{\dagger} \mathclose: = \mathopen: a^{\dagger} a \mathclose:$ and $\mathopen: c c^{\dagger} \mathclose: = - \mathopen: c^{\dagger} c \mathclose:$ .
    Thus normal ordering is linear $\mathopen: \alpha_1 \widehat{O}_1 + \alpha_2 \widehat{O}_2 \mathclose: = \alpha_1 \mathopen: \widehat{O}_1 \mathclose: + \alpha_2 \mathopen: \widehat{O}_2 \mathclose:$ if $\alpha_1,\alpha_2 \in \mathbb{C}$ and $\widehat{O}_1,\widehat{O}_2 \in \text{"free algebra"}$.

- $\mathopen: c c^{\dagger} \mathclose: = c c^{\dagger} - 1$

* src [[https://www.youtube.com/watch?v=7SepOSfG-To&t=2s][Review of many body field theory - YT]]

Operators are normal ordered (creation operators on the left, annihilation operators on the right) in order to prevent single particle to interact with it self.
Interaction happens only if there are at least 2 particles.
It should be a pairwise interaction.

* src [[https://en.wikipedia.org/wiki/Normal_order][Wikipedia]]

The normal ordering of an operator $\widehat{O}$ fixes the choice that
\begin{align*}
    \bra{0} \mathopen:\widehat{O}\mathclose: \ket{0} = 0 \; .
\end{align*}
Especially for an normal ordered Hamiltonian $\widehat{H}$ the ground energy is zero $\bra{0} \widehat{H} \ket{0} = 0$

** Free fields

For two free fields $\phi$ and $\chi$
\begin{align*}
   \mathopen: \phi(x) \chi(y) \mathclose: \, = \phi(x) \chi(y) - \bra{0} \phi(x) \chi(y) \ket{0}
\end{align*}
Each of the two terms on the right typically divergies for $x \to y$ but the difference is well defined and so is $\mathclose: \phi(x) \chi(y) \mathopen:$

* src [[https://www.quora.com/Is-there-a-fundamental-reason-for-normal-ordering-in-quantum-field-theory-Why-do-we-have-to-put-the-ladder-operator-first-rather-than-an-annihilation-operator?share=1][quora forum]]

/There is some freedom of choice in quantum field theory especially when normal ordering, or as it is also called, Wick ordering, the creation and annihilation operators in the Hamiltonian, so that all annihilation operators are to the right and all creation operators to the left./
/There is an ambiguity ordering problem in constructing the quantum Hamiltonian. There is no unique way to do it, starting from a classical Hamiltonian - instead there are several possible quantum Hamiltonians for a given classical theory./
/This choice, normal order rather than say anti-normal order, is a natural one for evaluating vacuum expectation values of time ordered products of field operators, time-ordered products of the vacuum expectation values of the field operators being sufficient to completely define the field theory if in fact they can all be evaluated./
/The calculations are simplified in this way - the annihilation operators in the vacuum expectation value of a normal ordered product of field operators hit the vacuum and annihilate it, thus simplifying the calculations greatly since many terms in the time-ordered product vanish. There are identities that relate the two kinds of ordered operator products, in the form of Wick’s theorem./
/If the Hamiltonian is not normal ordered - then you in effect shift the vacuum energy by an infinite constant in quantum field theory, which is the sum of the zero point energies of the modes of the field./
/It’s conventional and in various situations completely legitimate to treat this infinite constant as if it were zero and to ignore it - but there are cases, such as the Casimir effect, where it matters./

#+begin_export latex
    \clearpage
#+end_export

* References
#+print_bibliography:
