:PROPERTIES:
:ID:       a01382d3-d378-40f4-9dee-2efe1b443187
:END:
#+title: Cpp: control flow
#+setupfile: ~/gems-backup/org-mode/general-latex-header.org
#+setupfile: ~/gems-backup/org-mode/math-latex-header.org
#+bibliography: references.bib
#+cite_export: csl ../citation-styles/ieee.csl
#+filetags: :programming:cpp:

In loops and switch statements one can use src_cpp{continue}, src_cpp{break}, src_cpp{return} in order to stop current iteration and continue with the next iteration, break (stop) the loop, stop the loop and return a value.
