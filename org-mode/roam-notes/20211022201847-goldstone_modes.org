:properties:
:ID:       62d53f6d-f7f3-4ad9-ae2d-a7d1de5fb692
:end:
#+title: Goldstone modes
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

* Spontaneous Symmetry Breaking and Goldstone Modes [cite: @AI]

/Spontaneous symmetry breaking (SSB) is a fundamental concept in physics, particularly in fields like condensed matter physics, particle physics, and cosmology./
/It occurs when a system that is symmetric under some transformation (like rotations, translations, or gauge transformations) ends up in a state that does not exhibit that symmetry./
/Here’s a more detailed look at SSB and its implications, particularly regarding Goldstone modes:/

** Spontaneous Symmetry Breaking

1. *Definition*: /In a system with a continuous symmetry, the laws governing the system are invariant under certain transformations. However, the ground state (or vacuum state) of the system does not respect this symmetry. This means that while the equations of motion are symmetric, the lowest energy state is not./

2. *Example*: /A classic example is the ferromagnet. At high temperatures, the spins in a ferromagnetic material are disordered and point in random directions, maintaining rotational symmetry. Below a critical temperature (the Curie temperature), the spins align in a particular direction, breaking the rotational symmetry./

3. *Potential Energy Landscape*: /In many cases, the potential energy of the system has a "Mexican hat" shape. The peak of the hat represents the symmetric state, while the valleys represent the different possible ground states. When the system settles into one of these valleys, it chooses a specific direction (or configuration), thus breaking the symmetry./

** Goldstone Modes

1. *Goldstone's Theorem*: /This theorem states that for every continuous symmetry that is spontaneously broken, there exists a massless scalar excitation, known as a Goldstone mode. These modes correspond to the fluctuations around the vacuum state./

2. *Physical Interpretation*: /Goldstone modes can be thought of as low energy excitations that arise when the system is perturbed slightly (soft excitations). For example, in a ferromagnet, if you slightly rotate the direction of the spins, the system can respond with a low-energy excitation that corresponds to the Goldstone mode./

3. *Number of Goldstone Modes*: /The number of Goldstone modes is equal to the number of broken continuous symmetries. For instance, if a system has a U(1) symmetry (like phase rotation) that is broken, there will be one Goldstone mode. If a system has an O(3) symmetry (like rotations in three-dimensional space) that is broken down to O(2), there will be two Goldstone modes./

4. *Masslessness*: /The Goldstone modes are massless in the sense that their energy goes to zero as their momentum goes to zero. This is a consequence of the symmetry breaking; the system can fluctuate freely in the direction of the broken symmetry without incurring a cost in energy./

5. *Applications*: /Goldstone modes are important in various physical theories. In particle physics, they are associated with the Higgs mechanism, where the breaking of electroweak symmetry leads to massive gauge bosons and a massless scalar particle (the Higgs boson). In condensed matter physics, they can manifest as phonons in superfluid helium or as collective excitations in other ordered phases./

** Summary

/Spontaneous symmetry breaking is a crucial phenomenon that leads to the emergence of distinct physical states from symmetric laws./
/Goldstone modes are the massless excitations that arise from this breaking, providing insight into the dynamics and properties of the system./
/Understanding these concepts is essential for exploring various physical systems, from magnets to fundamental particles./

** Potential Energy Shape in Ferromagnets

/In a ferromagnet, the potential energy landscape that leads to Goldstone modes typically has a "Mexican hat" shape./
/This shape can be visualized as follows:/

- *Symmetric State*: /At high temperatures, the spins are disordered, and the system is in a symmetric state where the potential energy is at a maximum. This corresponds to the top of the "hat."/

- *Ground States*: /As the temperature decreases and the system undergoes a phase transition, it settles into one of the degenerate ground states represented by the valleys of the "hat." Each valley corresponds to a different direction of spin alignment, breaking the rotational symmetry./

- *Fluctuations*: /The valleys represent the different configurations that the system can adopt, and small fluctuations around these configurations correspond to the Goldstone modes. The energy cost for these fluctuations is low, leading to the massless nature of the Goldstone modes./


* Symmetry Breaking and Magnetism - Prof Stephen Blundell - OUPS Lecture [cite:@symmetry-breaking-and-magnetism-YT]

| Phenomenon        | Excitations |
|-------------------+-------------|
| crystal           | phonons     |
| (anti)ferromagnet | magnons     |
| ...               | ...         |


Goldstone's Theorem: /[[id:d7c7e167-ea46-421a-af23-a48e3a0e82a3][Breaking a continuous symmetry]] always results in a massless excitation, known as a Goldstone mode./
Excitations with dispersion that goes through zero energy are called massless.
This is due to the relativistic dispersion relation $E^2 = \vec{p}^2 c^2 + m^2 c^4$ where for zero momentum the energy finite (non-zero) and corresponds to the rest mass.
Thus if the energy is zero (non-finite) for zero momentum the excitations are called massless.

When you break continuous symmetry, you break the generators of the symmetry.
The number of broken generators is equal to the number of Goldstone modes.
But this is only true for Lorentz invariant systems.
It was developed for particle physics.
In solid state physics we solve the [[id:c171c1c5-49b9-4368-88c1-1cd7581f6418][Schroedinger equation]], which is non-relativistic and thus we do not need Lorentz invariance.
So the number of Goldstone modes is not (always) equal to the number of broken generators.
E.g. Heisenberg ferromagnet has only one Goldstone mode instead of two.
This is due to $J_x$, $J_y$ being coupled so they describe only one degree of freedom.
One can see in Fig.\ref{fig:spin-wave} that $x$ and $y$ degrees of freedom are coupled in the movement of the wave.

#+caption: Schematic depiction of a spin wave.
#+attr_org: :width 600
#+attr_latex: :width 10cm :placement [!htpb]
#+name:   fig:spin-wave
[[./images/spin-wave.png]]

One more example.
/Phonons break translational and rotational degrees of freedom but only phonons for $p_i$ (3 modes = 2 transverse + 1 longitudinal) instead of 6./
/This is due to redundancy, modes for rotations are the same as for translations $R^{0i} = \epsilon_{ijk} x^j T^{0k}$. The pairing occurs when the generators have a nonzero expectation value of their commutator./ [cite:@Goldstone-bosons-without-Lorentz-invariance]

* Elementary excitations [cite:@examine-elementary-excitations]

Consider as an example sound waves in a crystal.
We characterize a displacements by $\vec{u}(\vec{x}_i)$ where $\vec{x}_i$ is the equilibrium position of the i-th site.
So the position of the i-th site is $\vec{x}_i + \vec{u}(\vec{x}_i)$.
First we observe that displacing all sites by the same $\vec{u}$ (no symmetry breaking) does not cost any energy since the interaction between the sites does not change.
If we deviate a bit from the constant shift (symmetry breaking) we obtain a wave with a large wave length.
The effect is spread out over large space.
Imagine pushing on one end of a rigid rod. The rod bends.
Since the deviation from the constant shift is small the required energy is also small so the corresponding frequency is low.
So we deal with low frequency modes $\omega$ with small momentum $\vec{k}$.
Jeffrey Goldstone: whenever a continuous symmetry (rotations, translations, SU(3), ...) is broken, long-wavelength modulations in the symmetry direction should have low frequencies.

#+begin_quote
What about superconductors? They've got a broken gauge symmetry, and have a stiffness to decays in the superconducting current.
What is the low energy excitation? It doesn't have one.
But what about Goldstone's theorem? Well, you know about physicists and theorems ...
That's actually quite unfair: Goldstone surely had conditions on his theorem which excluded superconductors.
It's just that everybody forgot the extra conditions, and just remembered that you always got a low frequency mode when you broke a continuous symmetry.
We of course understood all along why there isn't a Goldstone mode for superconductors: my advisor, Phillip W. Anderson, showed that it's related to the Meissner effect.
The high energy physicists forgot, though, and had to rediscover it for themselves.
Now we all call the loophole in Goldstone's theorem the Higgs mechanism, because (to be truthful) Higgs and his high-energy friends found a much simpler and more elegant explanation than we had.
#+end_quote

Note that a symmetry of a system might [[id:d7c7e167-ea46-421a-af23-a48e3a0e82a3][break spontaneously]] as for example in ferromagnetic materials.

#+begin_export latex
    \clearpage
#+end_export


* References
#+print_bibliography:
