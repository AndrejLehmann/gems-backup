:PROPERTIES:
:ID:       693c658f-cb4b-414c-a22a-32a265ea0d47
:END:
#+title: Teaching: Wie man Computern das Ableiten beibringt oder: Warum Lisp?
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :teaching:programming:lisp:math:interdisciplinary:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Durch pattern matching kann man leicht die Ableitungsregeln in lisp implementieren. [cite:@lisp-computern-ableiten-beibringen]
Damit zeigt man das Potenzial einer ProgrammierSprache.
Choose right tool for the job.

* Further projects

[[https://github.com/norvig/paip-lisp][Paradigms of Artificial Intelligence Programming]]


#+begin_export latex
  \clearpage
#+end_export
* References
#+print_bibliography:
