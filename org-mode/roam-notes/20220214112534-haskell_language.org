:PROPERTIES:
:ID:       79268d5b-6946-48f9-abd7-be99237cabdf
:END:
#+title: Haskell language
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:haskell:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* [[id:20b6d804-85af-4a3f-934a-76979832ebca][usage in org-mode]]
* [[https://github.com/krispo/awesome-haskell][links, frameworks, libraries and software]]
* [[https://gist.github.com/ecthiender/b9db474e80113bdc18d472de1593eb3c][Setup instructions]]
* [[http://haskell.github.io/haskell-mode/manual/latest/][haskell mode doc]]
* [[id:844a1025-c44e-4b7a-8219-8e5c9162208f][Basic basics]]
* [[id:4accf282-7046-404e-ac1b-80577f42ebfa][Types and Typesclasses]]
* [[id:b429cfe8-4bf6-45d9-8181-c1a4a7d628bc][case expressions vs pattern matching]]
* [[id:bb92255f-a167-474a-bd4d-b24a4e4c9b18][Interactive programms with side effects]]
