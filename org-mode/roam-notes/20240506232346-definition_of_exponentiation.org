:PROPERTIES:
:ID:       6603edac-34e6-4406-897f-968003308e4a
:END:
#+title: Definition of Exponentiation
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Definition of Exponentiation in $\mathbb{R}$

Consider for $a \in \mathbb{R}, a > 0$ and $m, n \in \mathbb{Z}/\{0\}$
\begin{align*}
    a^{\frac{m}{n}} &= \left( a^{\frac{1}{n}} \right)^{m} \\
    &= \exp \left( \log \left( \left( a^{\frac{1}{n}} \right)^{m} \right) \right) \\
    &= \exp \left( m \log \left( a^{\frac{1}{n}} \right) \right) \\
    &= \exp \left( \frac{m}{n} \log \left( a \right) \right)
\end{align*}
Note that for $a \le 0$, $\log \left( a \right)$ is not defined.
We can use that for extending the definition to irrational exponentials.
For $a \in \mathbb{R}, a > 0$ and $x \in \mathbb{R}$ define
\begin{align*}
    a^{x} \coloneqq \exp \left( x \log \left( a \right) \right)
\end{align*}

* Definition of Exponentiation in $\mathbb{C}$

Just like in $\mathbb{R}$ we define
\begin{align*}
    a^{z} \coloneqq \exp \left( z \log \left( a \right) \right)
\end{align*}
where $z \in \mathbb{C}$, $\log \left( a \right)$ is [[id:a48a73d7-40aa-4fe1-b237-de9d12c22469][principle value of the logarithm]] and $a \in \mathrm{D}_{-\pi}$ it's domain.
Thus this becomes the principle value of the power.

** Failures of power and logarithm identities [cite:@exponentiation-wiki]

The following hold only for $a,b \in \mathbb{R}$, $a,b > 0$ and $x,y \in \mathbb{R}$
- $\log \left( a^{x} \right) = x \log \left( a \right)$
- $\left( a b \right)^{x} = a^{x} b^{x}$ and $\left( a / b \right)^{x} = a^{x} / b^{x}$
- $\left( \e^{x} \right)^{y} = \e^{xy}$ since [[id:a48a73d7-40aa-4fe1-b237-de9d12c22469][complex logarithm is multivalued]] and it must be replaced by  $\left( \e^{x} \right)^{y} = \e^{y \log \left( \e^{x} \right)}$, which is true for multivalued functions
- Also $\log \left( \exp z \right) = z$ is wrong due to [[id:a48a73d7-40aa-4fe1-b237-de9d12c22469][multivalued logarithm]].

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
