:PROPERTIES:
:ID:       8aad215b-12ce-41d1-a412-5401a8dc5d6b
:END:
#+title: DMFT for Mo's bachelor thesis
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export


* DMFT - Potthoff
** [[file:~/gems-backup/literature/physics/Theorie-der-kondensierter-Materie-Potthoff/lecture-notes/skript12.pdf][Hubbard model in the limit of large number of spacial dimensions]]

Let us consider as an example a $D$ -dimensional hyper cubic lattice (for large number of spacial dimensions is the form of lattice not relevant, what is relevant is the number of neighbors scales linearly with $D$) with nearest neighbor hopping $t>0$.
\begin{align*}
    H = -t \sum_i \sum_{j \in {\rm NN}(i)} \sum_{\sigma} \hat{c}^{\dagger}_{i\sigma} \hat{c}_{j\sigma} + \frac{U}{2} \sum_{i\sigma} \hat{n}_{i,\sigma} \hat{n}_{i,-\sigma}
\end{align*}
In order to keep physical quantities like kinetic energy or free DOS finite when $D \to \infty$ we need to rescale the hopping parameter $t=t^{ * }/\sqrt{D}$, where $t^{ * } = {\rm cont}$.
No other scaling results in finite quantities for $D \to \infty$.
For other scalings the quantities become $0$ or $\infty$.
So there is no ambiguity.
One can show by an exhaustive discussion of diagrams that in order to obtain finite physical observables the non-local Green's function has to scale with $D$ as
\begin{align*}
    \left\langle \hat{c}^{\dagger}_{i\sigma} \hat{c}_{j\sigma} \right\rangle = O \left( \frac{1}{\sqrt{D}} \right)
\end{align*}
for $i$ and $j$ being nearest neighbors.
In order to make it plausible consider energy per site
\begin{align*}
    \frac{\left\langle H \right\rangle}{L} = \underbrace{ -t }_{ O(1/\sqrt{D}) } \;\; \sum_{j \in {\rm NN}(i)} \sum_{\sigma} \left\langle \hat{c}^{\dagger}_{i\sigma} \hat{c}_{j\sigma} \right\rangle + \underbrace{ \frac{U}{2} \sum_{\sigma} \left\langle \hat{n}_{i,\sigma} \hat{n}_{i,-\sigma} \right\rangle }_{ O(1) }
\end{align*}
Since the number of nearest neighbors is proportional to $D$ the term $\left\langle \hat{c}^{\dagger}_{i\sigma} \hat{c}_{j\sigma} \right\rangle$ should scale as $O(1/\sqrt{D})$ in order for $\langle H \rangle/L$ to stay finite for $D \to \infty$.
Thus also the interplay between the kinetic energy and interaction remains non-trivial.
But for the local Green's function in the limit $D \to \infty$ holds
\begin{align*}
    G_{ii}(\omega) = O(1)
\end{align*}
as for the scaling of the local self energy
\begin{align*}
    \Sigma_{ii}(\omega) = O(1)
\end{align*}
For the non-local self energy
\begin{align*}
    \Sigma_{ij}(\omega) = O \left( \frac{1}{\sqrt{D}^3} \right)
\end{align*}
where $i$ and $j$ are nearest neighbors.
Note the non-trivial energy dependence in contrast to [[id:02995f52-921c-43d5-b6fe-dd7befe17ba0][mean field theories]] and the $\vec{k}$ -independence for the local self-energy and Green's function.

The diagrams can be discussed starting from the equation of motion for free one particle Green's function
\begin{align*}
    \left( \omega - \mu \right) G^{(0)}_{ii} (\omega) = 1 + \sum_{j}  t_{ij} G^{(0)}_{ji}(\omega)
\end{align*}
But first we observe that since the free DOS is proportional to $\Im\;G^{(0)}_{ii}(\omega+i0^+)$ the free local one particle Green's function has to be of order $O(1)$ so that the free DOS is finite.
Then from the equation of motion we see that e.g. for nearest neighbors Green's function $\sum_{j}  t_{ij} G^{(0)}_{ji}(\omega) = -t \, q \, G_{\rm NN}^{(0)}(\omega)$ has to scale with $O(1/\sqrt{D})$ since $t$ scales as $O(1/\sqrt{D})$ and $q$ as $D$.
In the same way one can deduce for the general case
\begin{align*}
    G^{(0)}_{ij}(\omega) = O\left(\frac{1}{D^{\frac{1}{2}\parallel i - j \parallel}}\right)
\end{align*}
where $\parallel ... \parallel$ is the Manhattan metric.

So in conclusion in order to obtain finite self-energy and Green's function only local diagrams have to survive the limit $D \to \infty$.
Compared to [[id:02995f52-921c-43d5-b6fe-dd7befe17ba0][mean field theories]] with are also exact in the limit of infinite dimensions DMFT only neglects spacial fluctuation (thus $\vec{k}$ -independence) but retains the temporal fluctuations.

** [[file:~/gems-backup/literature/physics/Theorie-der-kondensierter-Materie-Potthoff/lecture-notes/skript14.pdf][SIAM]]

#+name: eq:H_SIAM_Potthoff
\begin{align*}
    H_{\rm SIAM} = &\quad\sum_{\sigma} \varepsilon_{\rm imp} \; \hat{c}^{\dagger}_{\sigma} \hat{c}_{\sigma} + \frac{U}{2} \sum_{\sigma} \hat{n}_{\sigma} \hat{n}_{-\sigma} \quad \text{(correlated impurity)} \\
                 &+ \sum_{\vec{k}} \hat{a}^{\dagger}_{\vec{k}\sigma} \hat{a}_{\hat{k}\sigma} \quad \text{(uncorrelated bath)} \\
                 &+ \sum_{\vec{k}\sigma} V_{\vec{k}}(\hat{a}^{\dagger}_{\vec{k}\sigma} \hat{c}_{\sigma} + \hat{c}^{\dagger}_{\sigma} \hat{a}_{\vec{k}\sigma}) \quad \text{(hybridisation)}
\end{align*}

For $V_{\vec{k}}=0$ the impurity and the bath are decoupled.
Then the energy spectrum of the impurity are just $\delta$ -peaks at $\varepsilon_{\rm imp} - \mu$ and $\varepsilon_{\rm imp} - \mu + U$.
(And the energy spectrum of the bath are bands obtained from the free dispersion.)
By turning $V_{\vec{ k}}$ on the $\delta$ -peaks broaden.

In order to derive the interacting Green's function of the SIAM $G_{\rm SIAM}$ let's first consider the non-interacting case with $U=0$.
Then the Dyson equation is perturbative expansion in $V_{\vec{ k}}$
\begin{align*}
    G^{(0)}_{\rm SIAM}(\omega,\vec{k}) = G_{\rm imp}(\omega,\vec{k}) = G^{(0)}_{\rm imp}(\omega) \;\;+\;\; G^{(0)}_{\rm imp}(\omega)\;V_{\vec{k}}\;G^{(0)}_{\rm bath}(\omega,\vec{k})\;V_{\vec{k}}\;G^{(0)}_{\rm SIAM}(\omega,\vec{k})
\end{align*}
where
\begin{align*}
    G^{(0)}_{\rm imp}(\omega) = \frac{1}{\omega + \mu - \varepsilon_{\rm imp}}
\end{align*}
the free impurity Green's function and
\begin{align*}
    G^{(0)}_{\rm bath}(\omega,\vec{k}) = \frac{1}{\omega + \mu - \varepsilon_{\vec{k}}}
\end{align*}
the free bath Green's function.
This Dyson equation describes processes where the particle can hop from the impurity into bath, propagate in the bath and hop back.
Note that since we set $U=0$ there are no correlation effects induced by interaction $U$.
Here we can define hybridisation function
\begin{align*}
    \Delta(\omega) = \sum_{\vec{k}} V_{\vec{k}}\;G^{(0)}_{\rm bath}(\omega,\vec{k})\;V_{\vec{k}} = \sum_{\vec{k}} \frac{V_{\vec{k}}^2}{\omega + \mu - \varepsilon_{\vec{k}}}
\end{align*}
Then the solution of the non-interacting Dyson equation can be written as
\begin{align*}
    G^{(0)}_{\rm SIAM}(\omega) = G_{\rm imp}(\omega) = \frac{1}{\omega + \mu - \varepsilon_{\rm imp} - \Delta(\omega)}
\end{align*}
Now we can also write the Dyson equation for the interacting Green's function
\begin{align*}
    G_{\rm SIAM}(\omega) = G^{(0)}_{\rm SIAM}(\omega)\;+\;G^{(0)}_{\rm SIAM}(\omega)\;\Sigma_{\rm imp}(\omega)\;G_{\rm SIAM}(\omega)
\end{align*}
which is solved by
\begin{align*}
    G_{\rm SIAM}(\omega) = \frac{1}{\omega + \mu - \varepsilon_{\rm imp} - \Delta(\omega) - \Sigma_{\rm imp}(\omega)}
\end{align*}

** [[file:~/gems-backup/literature/physics/Theorie-der-kondensierter-Materie-Potthoff/lecture-notes/skript15.pdf][Mapping the Hubbard model in infinity spacial dimensions on the SIAM]]

The self energy of the Hubbard model in the limit $D \to \infty$ which is purely local can be written as diagramatic expansion with interacting Green's functions as propagators which are also purely local.
So the self energy written as functional is
\begin{align*}
    \Sigma^{\rm Hub} = \Sigma_{\rm loc}(\omega)\left[ G_{ii}(\omega) \right]
\end{align*}
The same is also true for SIAM
\begin{align*}
    \Sigma_{\rm imp} = \Sigma_{\rm loc}(\omega) \left[ G_{\rm SIAM}(\omega) \right]
\end{align*}
So in order to solve the Hubbard model in the limit of infinite dimensions one can solve the corresponding SIAM, which can be done numerically.
In order to map the $D \to \infty$ Hubbard model on the SIAM the parameters of the impurity problem we simply set $\varepsilon_{\rm imp} = t_{ii}$, set the free dispersion of the Hubbard model equal to the free dispersion of the bath, and determine the hybridisation self consistently.
The self consistent cycle is [[fig:DMFT-self-consistent-cycle]].
#+caption: DMFT self consistent cycle
#+attr_org: :width 800
#+attr_latex: :width 10cm :placement [!htpb]
#+name:   fig:DMFT-self-consistent-cycle
[[./images/DMFT-self-consistent-cycle.png]]
with the self consistency condition
\begin{align*}
    G_{ii}(\omega) = G_{\rm SIAM}(\omega)
\end{align*}
with ensures internal consistency of the theory.
The key approximation is here
\begin{align*}
   \Sigma_{ij}(\omega) = \delta_{ij} \Sigma_{ii}(\omega)
\end{align*}
The numerical solution of the SIAM is the challenging part of the loop.


#+begin_export latex
  \clearpage
#+end_export


* References
#+print_bibliography:
