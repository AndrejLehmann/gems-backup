% Created 2022-07-06 Wed 18:20
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{mathtools}
\usepackage{stackrel}
\usepackage{dsfont}
\usepackage{bbold}
\usepackage{mathtools}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows, positioning, automata, fit}
\tikzstyle{stdnode} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=grey!78]
\tikzstyle{arrow} = [thick, ->, >=stealth]%, red]
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\renewcommand{\mat}[1]{\boldsymbol{#1}}
\renewcommand{\d}{\mathrm{d}}
\renewcommand{\Tr}{\mathrm{Tr}}
\newcommand{\bra}[1]{\langle #1 \rvert}
\newcommand{\ket}[1]{\lvert #1 \rangle}
\newcommand{\braket}[2]{\langle #1 \rvert #2 \rangle}
\newcommand{\expect}[1]{\left\langle #1 \right\rangle}
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\e}{\mathrm{e}}
\newcommand{\ln}{\mathrm{ln}}
\newcommand{\T}{\mathrm{T}}
\renewcommand{\Re}{\mathrm{Re}}
\renewcommand{\Im}{\mathrm{Im}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\up}{\uparrow}
\newcommand{\dn}{\downarrow}
\newcommand{\implies}{\Rightarrow}
\author{Andrej Lehmann}
\date{\today}
\title{Exact solution of the many body Schroedinger equation}
\hypersetup{
 pdfauthor={Andrej Lehmann},
 pdftitle={Exact solution of the many body Schroedinger equation},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.6)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle
\clearpage \tableofcontent \clearpage

\section{Full CI \citeprocitem{1}{[1]}, \citeprocitem{2}{[2]}}
\label{sec:org0a30d83}
The full Hamiltonian of the \(N\) -particle system is given by

\begin{align*}
    \hat{H} = \sum_{i=1}^N \left( \frac{\hat{\vec{p}}_i}{2m} + V(\hat{\vec{r}}_i) \right) + \frac{1}{2} \sum_{i,j = 1}^{N} \frac{e^2}{ | \hat{\vec{r}}_i - \hat{\vec{r}}_j | } = \hat{H}_0 + \hat{H}_{\rm I} \; .
\end{align*}

The non-interacting one-particle wave function \(\ket{\psi_0}\) of the non-interacting part of the Schr``odinger equation \(\hat{H}_0 \ket{\psi_0} = E_0\ket{\psi_0}\) can be expended in one particle basis \(\{\ket{\phi_n}\}\) of the one-particle Hilbert space \(\mathcal{H}_1\)
\begin{align*}
    \ket{\psi_0} = \sum_n c_n \ket{\phi_n}
\end{align*}
The \(N\) -particle Hilbert space \(\mathcal{H}_N\) can be constructed by \(N\) -fold tensor product space of non-interacting one-particle Hilbert spaces \(\mathcal{H}_N = \mathcal{H}_1 \otimes \mathcal{H}_1 \otimes \dots \otimes \mathcal{H}_1\)
and the \(N\) -paricle basis set is then accordingly constructed by the tensor product of the one-particle basis vectors \(\{\ket{\phi_1 \phi_2 \dots \phi_N}\}\).
The \(N\) -particle wave function \(\ket{\psi}\) of the full Schr``odinger equation \(\hat{H} \ket{\psi} = E \ket{\psi}\) can then be written as
\begin{align*}
    \ket{\psi} = \sum_{n_1 \, \dots \, n_{n_N}} c_{n_1 \, \dots \, n_N} \ket{\phi_{n_1} \dots \phi_{n_N}}
\end{align*}
where \(\ket{\phi_{n_1} \phi_{n_2} \dots \phi_{n_N}} = \ket{\phi^{(1)}_{n_1}} \ket{\phi^{(2)}_{n_2}} \dots \ket{\phi^{(N)}_{n_N}}\) and the superscript denotes the particle number.
But this \(\ket{\psi} \in \mathcal{H}_N\) does not account for anti-symmetric property of the fermionic wave functions, which results from the following consideration.
The expectation value of observables \((\expect{A} = \sum_i p_i \, a\) where \(A \ket{\psi} = a \ket{\psi}, p_i = |\psi|^2)\) should not change when we exchange the states of two identical particles.
This leaves two possibilities: the wave function does change sign under exchange or it does not.
This two possibilities correspond to two kinds of particles: anti-symmetric fermions (wave function changes sign) and symmetric bosons (wave function does not change sign).
Thus the real physical Hilbert space for electrons \(\mathcal{H}^-_{N}\) is only a subspace of \(\mathcal{H}_N\), \(\mathcal{H}^-_{N} \subset \mathcal{H}_N\), and the correct basis set  \(\{\ket{\phi_{n_1} \dots \phi_{n_N}}^{\!-}\} \in \mathcal{H}^-_{N}\) should be anti-symmetric.
This can be achieved by constructing Slater determinants from the one-particle basis
\begin{align*}
    \ket{\phi_{n_1} \dots \phi_{n_N}}^{\!-} = \frac{1}{N} \det
        \begin{pmatrix}
            \ket{\phi_{n_1}^{(1)}} & \dots & \ket{\phi_{n_1}^{(N)}} \\
            \vdots                 &       & \vdots \\
            \ket{\phi_{n_N}^{(1)}} & \dots & \ket{\phi_{n_N}^{(N)}}
        \end{pmatrix}
\end{align*}
This also results in Pauli principle since the determinant is zero if \(\ket{\phi^{(i)}_{n_l}} = \ket{\phi^{(j)}_{n_m}}\) for \(i \neq j \land l \neq m\).
The resulting eigenvalue problem
\begin{align*}
    \sum_{n_1 <\cdots <n_N} \hat H \;  c_{n_1 \cdots n_N} |  \phi_{n_1}  \cdots  \phi_{n_N} \rangle^{\!-} = E \sum_{n_1 <\cdots <n_N} c_{n_1 \cdots n_N} |  \phi_{n_1}  \cdots  \phi_{n_N} \rangle^{\!-}
\end{align*}
can be solved numerically by exact diagonalization but only for small systems due to exponential scaling with system size.
In contrast the Hartree-Fock method uses just one Slater determinant as \(N\) -particle wave function.

\clearpage

\section{References}
\label{sec:org0a10a26}
\hypertarget{citeproc_bib_item_1}{[1] I. Wikimedia Foundation, “Configuration interaction.” https://de.wikipedia.org/wiki/Configuration\_Interaction, 2020.}

\hypertarget{citeproc_bib_item_2}{[2] I. Wikimedia Foundation, “Full configuration interaction.” https://en.wikipedia.org/wiki/Full\_configuration\_interaction, 2020.}
\end{document}
