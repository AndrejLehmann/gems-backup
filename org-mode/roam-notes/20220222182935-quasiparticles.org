:properties:
:id:       a1395879-938a-4303-bacb-ba30bf1325a9
:end:
#+title: Quasiparticles
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

* Example [[id:7422d7bb-af4a-4996-bc86-bbc7c228735d][Fermi liquid]]

Siehe in [[id:7422d7bb-af4a-4996-bc86-bbc7c228735d][Landau's Fermi Liquid Theory]] discussion on spectral function for noninteracting fermions and Fermi liquids.

* Quasiparticle weight

\begin{align*}
    Z = \left( 1 - \frac{\d \Re\Sigma(\omega)}{\d \omega} \right)^{-1}
\end{align*}

As a function of $U$, $Z$ can determine Mott transition when reaching $0$.
[[id:4a8bfdb2-7f6e-4437-9b93-0c89e82d91d7][Vanishing $Z$ is related to divergence of the effective mass]].
