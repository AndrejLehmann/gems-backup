:PROPERTIES:
:ID:       0138d2df-425a-4d10-92fe-4195ad2e2266
:END:
#+title: Command line tools
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* terminal commands
| command   | what it does                          |
|-----------+---------------------------------------|
| cd -      | go back to previous directory         |
| C-z       | send programm to the background       |
| f g ENTER | bring programm back to the foreground |
| !!        | previous command                      |
| history   | list of previous command              |
| !<n>      | run n-th command from the list        |

* hdf5
* gnuplot
** splot and plot in the same figure

#+begin_src gnuplot
splot 'data.txt' using 1:2:3, '' using 1:2:(0) with lines
#+end_src
** pass argument in command line
:src:
https://stackoverflow.com/questions/12328603/how-to-pass-command-line-argument-to-gnuplot
:end:

#+begin_src bash
gnuplot -e "filename='foo.data'" foo.plg
#+end_src

#+begin_src gnuplot
if (!exists("filename")) filename='default.dat'
plot filename
#+end_src

Multiple command line arguments

#+begin_src bash
gnuplot -e "filename='foo.data'; foo='bar'" foo.plg
#+end_src

#+begin_export latex
  \clearpage
#+end_export

* zip

#+begin_src bash
zip -r archivename.zip directory_name_1 directory_name_2 file_1 file_2
#+end_src

#+RESULTS:

* References
#+print_bibliography:
