:PROPERTIES:
:ID:       f0a3ac8c-a19f-47f6-9b63-be4d3899b798
:END:
#+title: Emacs: editing remote files
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :emacs:remote-working:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Some sources

[[https://blog.tastytea.de/posts/editing-remote-files-with-emacs-comfortably/][Editing remote files with emacs comfortably]]
[[https://blog.goldandapager.io/a-better-emacs-remote-editing-workflow/][A better emacs remote editing workflow]]
[[https://www.youtube.com/watch?v=pSjrwSI4OHk][Emacs and Remote Shell]]

#+begin_export latex
  \clearpage
#+end_export

* upload module & [[https://github.com/cjohansson/emacs-ssh-deploy][ssh-deploy]]

Basically it's simple and fast enough.
=upload= is a module that can be commented in in the doom's init.el file.
It is just a [[/Users/andrejthealien/.emacs.d/modules/tools/upload/config.el][script]] that configures the =ssh-deploy= package.
([[https://github.com/cjohansson/emacs-ssh-deploy][More configuration examples]])
One just needs to create a directory with the same name as the remote directory.
And put =.dir-locals.el= file in there with e.g. the following content

#+BEGIN_SRC emacs-lisp
((nil . ((ssh-deploy-root-local . "/local/path/to/project")
         (ssh-deploy-root-remote . "/ssh:user@server:/remote/project/")
         (ssh-deploy-on-explicit-save . t))))
#+END_SRC

Then with the doom's prefix =SPC r= one can start editing and browsing the remote directories.

Also =ssh-deploy= should be able to read the [[~/.ssh/config]] file.
But for some reason it seems not to do that.
So I can not forward X11.


* TRAMP

Run src_elisp{find-file} in emacs and type e.g. src_elisp{/ssh:alehemann@physnet.uni-hamburg.de:} .
If the connection has be made with specifying an identity file like id_rsa_hlrn, one has to create and edit ~/.ssh/config like
#+begin_src
Host blogin
    HostName blogin.hlrn.de
    User hhpal3hm
    IdentityFile ~/.ssh/id_rsa_hlrn
#+end_src
Then one can run src_elisp{find-file} in emacs and type src_elisp{/ssh:blogin:} .

#+begin_export latex
    \clearpage
#+end_export
* References
#+print_bibliography:
