:PROPERTIES:
:ID:       0dbd316b-acf3-4d92-81e0-e66039b5068f
:END:
#+title: Projects
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :coding:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* [[https://www.youtube.com/watch?v=FYTZkE5BZ-0][Making music in Haskell from Scratch]]
* Fractals
** CodeParade
- [[https://www.youtube.com/watch?v=svLzmFuSBhk][Video]]
- [[https://github.com/HackerPoet/PySpace][github]]
** Syntopia
- [[http://blog.hvidtfeldts.net/index.php/2011/06/distance-estimated-3d-fractals-part-i/][blog]]
** Inigo Quilez
- [[https://iquilezles.org/articles/][website]]
* [[https://github.com/practical-tutorials/project-based-learning#cc][project based learning]]

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
