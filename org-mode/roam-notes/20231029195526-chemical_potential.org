:PROPERTIES:
:ID:       346ec836-c16b-4df6-a71b-8eb887d49f55
:END:
#+title: Chemical potential
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :physics:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

\begin{align*}
    & E(N+1) - E(N) = \mu \\
    & \frac{\partial E}{\partial N} = \mu = \varepsilon(p_{\mathrm{F}}_{})
\end{align*}


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
