:PROPERTIES:
:ID:       94fa296d-1dd4-4083-840c-e52a885fa632
:END:
#+title: Experimental measurement of the many-body correlation functions
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:experiments:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

The  Green’s  function  provides  the  information  about  the  spectral  function  of  electrons,  which  can be measured, for instance, by [[id:c7fbfc16-036e-4672-a2a9-9e48a2ae105a][angle-resolved photo emission spectroscopy (ARPES)]].
The susceptibility describes the response of the system to external electric or magnetic fields, and can be measured by inelastic neutron and X-ray(RIXS) scattering techniques, or by electron energy loss spectroscopy (EELS) and nuclear magnetic resonance (NMR) methods.
/The electron-energy loss (EEL) is proportional to the imaginary part of the [[id:7ae28a14-0b02-482d-a03d-ba0eb446dda5][inverse dielectric function]]./ [cite:@plasmonic-physics-of-2D-materials]
In particular, this allows one to obtain plasmon and magnon spectra, and reveal other collective electronic fluctuations in the system.

* Electrodynamics of correlated electron materials [cite:@electrodynamics-corr-el-materials chap.C]

/Optical methods are emerging as a primary probe of correlations./
/Apart from monitoring the kinetic energy, experimental studies of the electromagnetic response over a broad energy range (see Sec. II.A) allow one to examine all essential energy scales in solids associated with both elementary excitations and collective modes (see Sec. III)./
/Complementary to this are insights inferred from time-domain measurements allowing one to directly investigate dynamical properties of correlated matter (see Sec. IV)./ [cite: @electrodynamics-corr-el-materials]

\begin{align*}
    A_{\vec{k}}(\omega) = - \frac{1}{\pi} \Im G_{\vec{k}}(\omega) = - \frac{1}{\pi} \Im \frac{1}{\omega - \epsilon_{\vec{k}} - \Sigma_{\vec{k}}(\omega)}
\end{align*}
/The self-energy contains information on all possible interactions of an electron with all other electrons in the system and the lattice./
/In the absence of interactions the spectral function is merely a $\delta$ at $\omega = \epsilon_{\vec{k}}$, whereas $\Re\Sigma_{\vec{k}}(\omega) = \Im\Sigma_{\vec{k}}(\omega) = 0$./
/Interactions have a twofold effect on the spectral function./
/First, the energy is shifted with respect to the noninteracting case by the amount proportional to $\Re\Sigma_{\vec{k}}(\omega)$./
/Second, the spectral function acquires a Lorentzian form with the width given by $\Im\Sigma_{\vec{k}}(\omega)$./
/The corresponding states are referred to as dressed states or [[id:a1395879-938a-4303-bacb-ba30bf1325a9][quasiparticles states]]./
/The spectral function and the complex self-energy are both experimentally accessible through photoemission measurements./
/The optical conductivity can be computed by the linear response theory./
/It is usually expressed in terms of the one-particle Green’s function, the two-particle vertex function, and electron velocities./
See [cite: @electrodynamics-corr-el-materials chap. II.C] for more detailed formulaic explanation.



#+begin_export latex
    \clearpage
#+end_export
* References
#+print_bibliography:
