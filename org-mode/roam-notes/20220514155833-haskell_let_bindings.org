:PROPERTIES:
:ID:       0270c600-88ea-4a80-9d91-7d4ed94c7f64
:END:
#+title: Haskell: let bindings
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:haskell:

[[id:a73e5dc8-8cd4-4151-aaea-2e6fefb20bde][src_haskell{where} binding]] binds variables at the end and the bindings are visible in the whole function including the [[id:a73e5dc8-8cd4-4151-aaea-2e6fefb20bde][guards]].
src_haskell{let} bindings can bind variables anywhere and are local (e.g. they don't span across the [[id:a73e5dc8-8cd4-4151-aaea-2e6fefb20bde][guards]]).
The syntax is src_haskell{let <bindings> in <expression>}.
The names that you define in the src_haskell{let} part are accessible to the expression after the src_haskell{in} part.
And the names have to be aligned.

#+begin_src haskell :exports both
:{
cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
    let sideArea = 2 * pi * r * h
        topArea = pi * r ^2
    in  sideArea + 2 * topArea
:}
cylinder 0.5 1
#+end_src

#+RESULTS:
: 4.71238898038469

src_haskell{let} bindings are expressions.
src_haskell{where} bindings are syntactic constructs.
Like the [[id:b9920c9f-aae8-4533-87f2-a2288180c5d2][src_haskell{if} statements]] src_haskell{let} expressions can be build in anywhere.

#+begin_src haskell :exports both
[if 5 > 3 then "Woo" else "Boo", if 'a' > 'b' then "Foo" else "Bar"]
["Woo", "Bar"]
#+end_src

#+RESULTS:
| Woo | Bar |

#+begin_src haskell :exports both
[let square x = x * x in (square 5, square 3, square 2)]
#+end_src

#+RESULTS:
| 25 | 9 | 4 |

Or when you want to bind multiple binds inline

#+begin_src haskell :exports both
(let a = 100; b = 200; c = 300 in a*b*c, let foo="Hey "; bar = "there!" in foo ++ bar)
#+end_src

#+RESULTS:
| 6000000 | Hey there! |

With pattern matching:

#+begin_src haskell :exports both
(let (a,b,c) = (1,2,3) in a+b+c) * 100
#+end_src

#+RESULTS:
: 600

In list comprehensions:

#+begin_src haskell :exports both
:{
calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi | (w, h) <- xs, let bmi = w/h^2, bmi >= 25.0]
:}
calcBmis [(111,2)]
#+end_src

#+RESULTS:
| 27.75 |

Note that is possible to use src_haskell{bmi} in the last part because it was defined in the src_haskell{let} part before.
And also note that here the src_haskell{in} is omitted since the visibility is predefined.
But we could use src_haskell{let} src_haskell{in} bindig before src_haskell{|}.
Then the names defines would be visible only there.
