:PROPERTIES:
:ID:       f005f729-0973-4f78-b68e-c787307b7786
:END:
#+title: C#: abstract classes vs. interfaces
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :C#:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Interfaces can inherit from one or more interfaces.
A class or struct can implement multiple interfaces.
If a base class implements an interface, any class that's derived from the base class inherits that implementation.
By using interfaces, you can, for example, include behavior from multiple sources in a class.
That capability is important in C# because the language doesn't support multiple inheritance of classes.
In addition, you must use an interface if you want to simulate inheritance for structs, because they can't actually inherit from another struct or class.

- can contain instance methods, properties, events, indexers
- may contain static constructors, fields, constants, or operators
- can't contain instance fields, instance constructors, or [[finalizers]]
- may define default implementations


* abstract classes

- can contain both declarations and implementations
- a class can only inherit from one abstract class

* interfaces

- contain only the declarations
- everything is public
- no constructors
- no static members
- a class can implement multiple interfaces


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
