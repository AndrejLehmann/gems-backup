:properties:
:ID:       973ba04e-5aad-4e4d-b427-8b25871b15d0
:end:
#+title: Correlations and Emergence
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* Emergence vs Reductionism
** More is different [cite:@more-is-different-Anderson]

#+begin_quote
The constructionist hypothesis breaks down when confronted with the twin difficulties of scale and complexity.
The behavior of large and complex aggregates of elementary particles, it turns out, is not to be understood in terms of a simple extrapolation of the properties of a few particles.
Instead, at each level of complexity entirely new properties appear, and the understanding of the new behaviors requires research which I think is as fundamental in its nature as any other.
#+end_quote

| X                 | obeys laws of Y             |
|-------------------+-----------------------------|
| many-body physics | elementary particle physics |
| chemistry         | many-body physics           |
| molecular biology | chemistry                   |
| cell biology      | molecular biology           |
| ...               | ...                         |
| psychology        | physiology                  |
| social sciences   | psychology                  |

#+begin_quote
But this hierarchy does not imply that science X is "just applied Y".
At each stage entirely new laws, concepts, and generalizations are necessary, requiring inspiration and creativity to just as great a degree as in the previous one.
Psychology is not applied biology, nor is biology applied chemistry.
#+end_quote

#+begin_quote
The chemists will tell you that ammonia "is" a triangular pyramid.
With nitrogen negatively charged and the hydrogens positively charged, so that it has an electric dipole moment ($\mu$), negative toward the apex of the pyramid.
Now this seemed very strange to me, because I was just being taught that nothing has an electric die pole moment.
The professor was really proving that no nucleus has a dipole moment, because he was teaching nuclear physics, but as his arguments were based on the symmetry of space and time they should have been correct in general.
I soon learned that, in fact, they were correct (or perhaps it would be more accurate to say not incorrect) because he had been careful to say that no stationary state of a system (that is, one which does not change in time) has an electric dipole moment.
If ammonia starts out from the above unsymmetrical state, it will not stay in it verylong.
By means of quantum mechanical tunneling, the nitrogen can leak through the triangle of hydrogens to the other side, turning the pyramid inside out, and, in fact it can do so very rapidly.
This is the so called "inversion", wich occurs at a frequency of about $3\times 10^{10}$ per second.
A truly stationary state can only be an equal superposition of the unsymmetrical pyramid and its inverse.
The mixture does not have a dipole moment. [...]
the result is that the state of the system, if it is to be stationary, must always have the same symmetry as the laws of motion which govern it.
A reason may be put very simply: In quantum mechanics there is always a way, unless symmetry forbids,to get from one state to another.
Thus, if we start from any one unsymmetrical state,the system will make transitions to others, so only by adding up all the possible unsymmetrical states in a symmetrical way can we get a stationary state.
The symmetry involved in the case of ammonia is parity, the equivalence of left- and right-handed ways of looking at things. [...]
Having seen how the ammonia molecule satisfies our theorem that there is no dipole moment, we may look into other cases and, in particular, study progressively bigger systems to see whether the sate and the symmetry are always related.
There are other similar pyramidal molecules, made of heavier atoms. Hydrogen phosphide, $PH_3$, which is twice as heavy as ammonia, inverts, but at one-tenth the ammonia frequency.
Phosphorus trifluoride, $PF_3$, in which the much heavier fluorine is substituted for hydrogen, is not observed to invert at a measurable rate, although theoretically one can be sure that a state prepared in one orientation would invert in a reasonable time.
We may then go on to more complicated molecules such as sugar, with about 40 atoms.
For these it no longer makes any sense to expect the molecule to invert itself. [...]
At this point we must forget about the possibility of inversion and ignore the parity symmetry: the symmetry laws have been, not repealed, but broken.
If on the other hand we synthesize out sugar molecules by a chemical reaction more or less in thermal equilibrium, we find that there are not, on the average more left- then right-handed ones or vice versa.
In the absence of anything more complicated than a collection of free molecules, the symmetry laws are never broken, on the average.
We needed living matter to produce an actual unsymmetry in the populations.
In really large, but still inanimate, aggregates of atoms, quite a different kind of broken symmetry can occur again leading to a net dipole moment or to a net optical rotating power, or both.
Many crystals have a net dipole moment in each elementary unit cell (pyroelectricity),and in some this moment can be reversed by an electric field (ferroelectricity).
This asymmetry is a spontaneous effect of the crystal's seeking its lowest energy state.
Of course, the sate with opposite moment also exists and has, by symmetry, just the same energy, but the system is so large that no thermal or quantum mechanical force can cause a conversion of one to the other in finite time compared to, say, the age of the universe. [...]
I would challenge you to start from the fundamental laws of quantummechanics and predict the ammonia inversion and its easily observable properties without going through the stage of using the unsymmetrical pyramidal structure, even though no "state" ever has that structure.
It is fascinating that it was not until a couple of decadesago (2) that nuclearphysicists stopped thinking of the nucleus as a featureless, symmetrical little ball and realized that while it really never has a dipole moment, it can become football-shaped or plate-shaped.
[...] no new knowledge of fundamental laws and would have been extremely difficult to derive synthetically from those laws; it was simply an inspirations based, to be sure on everyday intuition, which suddenly fitted everything together.
The basic reason why this result would have been difficult to derive is an importantone for our further thinking
If the nucleus is sufficiently small there is no real way to define its shape rigorously: Three or four or ten particles whirling about each other do not define a rotating-"plate"or "football.'
It is only as the nucleus is considered to be a many-body system - in what is often called the $N \to \infty$ limit - that such behavioris rigorously definable.
We say to ourselves: A macroscopic body of that shape would have such-and-such spectrum of rotational and vibrational excitations, completely different in nature from those which would characterize a featureless system.
When we see such a spectrum even not so separated,
and somewhat imperfect, we recognize that the nucleus is, after all, not macroscopic;
it is merely approaching macroscopic behavior.
Starting with the fundamental laws and a computer, we would have to do two impossible things - solve a problem with infinitely many bodies, and then apply the result to a finite system - before we synthesized this behavior. [...]
The essential idea is that in the so called $N \to \infty$ limit of large systems (on our own, macroscopic scale) it is not only convenient but essential to realize that matter will undergo mathematically sharp, singular "phase transitions" to states in which the microscopic symmetries, and even the microscopic equations of motion, are in a sense violated.
The symmetry leaves behind as its expression only certain characteristic behaviors, for instance, long-wavelength vibrations, of which the familiar example is sound waves; [...]
In this case we can see how the whole becomes not only more than but very different from the sum of its parts. [...]
So it is not true, as a recent article would have it (7), that we each should 'cultivate our own alley, and not attempt to build roads over the mountain ranges ... between the sciences."
Rather, we should recognize that such roads, while often the quickest shortcut to another part of our own science, are not visible.
#+end_quote

A dialog in Paris in the 1920's:
#+begin_quote
FITZGERALD: The rich are differnt from us.
HEMINGWAY: Yes, thaty have more money.
#+end_quote

** More is the Same; Phase Transitions and Mean Field Theories [cite:@more-is-the-same]
** Approximation and idealization: Why the difference matters [cite:@approximation-and-idealization]
** Infinite idealizations [cite:@infinite-idealizations]

* Model Hamiltonians

Unlike in the particle physics the full hamiltonian is well known (relativistic effects neglected)
\begin{align*}
    H = - \frac{\hbar}{2m_e} \sum_i \nabla_i - \sum_{i,I} \frac{Z_I e^2}{| \vec{r}_i - \vec{R}_I|} + \frac{1}{2} \sum_{i \neq j} \frac{e^2}{|\vec{r}_i - \vec{r}_j|} - \sum_I \frac{\hbar}{2M_I} \nabla_I^2 + \frac{1}{2} \sum_{I \neq J} \frac{Z_I Z_J e^2}{|\vec{R}_I - \vec{R}_J|}
\end{align*}
Like assume we try to solve the problem by diagonalizing the hamiltonian.
The number of Slater's determinats is $M!/N!(M-N)! \approx \e^{CN}$.
E.g. for 2 carbon atoms (C: $N=12$, $M=36$) the number of determinants is $10^{9}$.
So we reduce the full hamiltonian to a model hamiltonian with effective parameters.

* History [cite: @metal-insulator-transitions]

- (Bethe, 1928; Sommerfeld, 1928; Bloch, 1929):
  - noninteracting electrons
  - insulators: filling is such that Fermi energy is in the band gap
  - metals: filling is such taht Fermi energy is inside a band
  - (Wilson, 1931a, 1931b; Fowler, 1933a, 1933b): semiconductors: small energy gap
    - ca. 15 years later: invention of a transistor by Shockley, Brattain, and Bardeen.
- de Boer and Verwey (1937): many transition-metal oxides like $\mathrm{NiO}$ are insulators eventhough they exhibit a partially filled d-electron bands.
- Peierls (1937) argued that strong Coulomb interactions and in that contex the electron-electron correlation might be of significant importance and the reason for insulating behaviour.
- This established the studies of strongly correlated electrons
- Theoretical approaches of Mott (1949, 1956, 1961, 1990) $\to$ [[id:02b74d9c-3398-4c54-9ddd-76a534e56817][Mott insulator]]
- In contrast Slater (1951): magnetic ordering is the origin of insulating behavior since most Mott insulators exhibit magnetic ordering at least at zero temperature.
  The band gap is then formed due to superlattice structure of the magnetic periodicity.
- [[id:7422d7bb-af4a-4996-bc86-bbc7c228735d][Landau's Fermi Liquid Theory]]

* Weak Correlation

- /electron motion is influenced primarily by the Pauli principle (e.g. ${\rm Na}$)/ [cite:@review-many-body-field-theory-I]
- $GW$ -approximation applies
- Semiconductors

* [cite:@review-many-body-field-theory-I]

- Electrons are generally free to move besides it's motion being influenced mainly just by the Pauli principle.
- E.g. in the Hubbard model even if $U$ is large particles can be weakly correlated if the particle density is low since they rarely near each other and the Hubbard $U$ does not play a great role.
  One can draw an analogy to low density traffic. Eventhough two cars being in the same place is forbidden due to resulting crash, the movement of a care is weakly determined by other cares since they are far apart from each other.
  So large interaction is necessary but not sufficient for strong correlation.
- But consider [[id:2d2922b6-e599-424b-85fd-f69acb2f4fae][Wigner crystal]] : strongly correlated, very low density but long ranged Coulomb interaction.
- More generally we can consider [[id:754b4376-b02f-48a9-ab55-7afa04000606][dimensionless density parameter]] $r_{s}$.
  $r_s$ is small when the electron gas is dense.
  For $r_s \ll 1$ we can treat Coulomb interaction perturbativaly.
- [[id:7422d7bb-af4a-4996-bc86-bbc7c228735d][Landau's Fermi Liquid Theory]] for weak correlations often holds even for intermediate coupling $r_{s} \sim O(1)$

* Strong Correlations

- E.g. transition metals, lanthanide and actinide (open d- and f-subshell) compounds
- Electron motion is strongly influenced by the interaction between them.
- E.g. in the Hubbard model if the particle density is near half filling for example, large $U$ crucially determines the behavior of the particle since (for low energies) it prevents hopping of two particles on the same site that is likely in the half filled case.
  (If the filling is sparse, $U$ does not play a significant role even if $U$ is large.)
- [[id:f5324b07-857c-4f0a-a8f0-69d31d156d72][low dimension materials]] often exhibit strong correlations.

** Examples of strongly correlated multi band/orbital materials [cite: @multi-band-DTRILEX chap.1]

/Prominent examples of materials where the interplay of orbital degrees of freedom and strong correlations is believed to be of crucial importance are vanadates [..], ruthenates [..], nikelates [..], and iron-based super- conductors [..]./
/Even in the case of [[id:9d13962f-ea58-4b05-937f-cad3a3cba636][cuprate superconductors]] the question whether an effective three-band model should be used instead of a single-band one is still under debate [..]./

** Strongly Correlated Electrons [cite:@electron-correlations-in-molecules-and-solids]

/Electron correlations are strong when the on-site electron-electron repulsions $U$ are much larger than the energies associated with the hybridization of atomic orbitals belonging to different atoms (resonance energies)./
/The latter are characterized in a solid by the width $W$ of the energy band under consideration./
/A large ratio $U/W$ is expected in systems involving $4f$ or $5f$ electrons, i.e., rare earth or actinide atoms./
/Yet systems with $d$ electrons can be strongly correlated, too./
/A famous example is ${\rm CoO}$./
/If we treated it within the independent electron approximation we would find that this substance is metallic, with an odd number of electrons per unit cell and a partially filled $d$ band./
/In reality, however, ${\rm CoO}$ is an insulator./
/The same holds true for ${\rm La_2 Cu0_4}$, a prototype for a class of materials with high superconducting transition temperatures./
/${\rm CoO}$ and ${\rm La_2 Cu0_4}$ are not metallic because the strong electron correlations suppress the charge fluctuations required for a nonvanishing conductivity./
/Instead they are insulators./
[...]
/When taking the expectation value with respect to the ground state $\ket{\Phi_0}$ of $H_0$ we encounter expression of the form/
\begin{align*}
   \int \!\d \tau_1 \,... \int \!\d \tau_n \; \bra{\Phi_0}\, \mathcal{T}_{\tau} \; H_1(\tau_1)\,...\, H_1(\tau_n) \,\ket{\Phi_0}
\end{align*}
/They can be evaluated by breaking them up into products of simpler expectation values provided Wick's theorem holds./
/The theorem holds when the operators which diagonalize $H_0$ satisfy simple fermionic or bosonic commutation relations./
/On the other hand, when $H_0$ contains the strong electron interactions, the operators which diagonalize $H_0$ have complex commutation relations with the operators causing transitions between the eigenstates./
/Then the usual diagrammatic rules are not applicable and we require alternative techniques./
Also compare with comments on [[id:973ba04e-5aad-4e4d-b427-8b25871b15d0][St"orungstheorie]].

For an example of a strongly correlated molecule read the next chapter.


** [cite:@small-parameter-for-lattice-models-with-strong-interactions]

/Roughly speaking, correlations are strong if the nonlinearity and coupling are comparable, and therefore perturbative approaches make no sense./

* Charge Conservation

Charge conservation means that if one location has more charge, then some other location has less.
So electrons have to be correlated to fulfill charge conservation.
([[id:1693c925-21e9-405c-aa16-d03d2795acc6][Charge conservation can be verified at susceptibility]]).
In contrast in a [[id:02995f52-921c-43d5-b6fe-dd7befe17ba0][mean field theories]]  lattice sites are approximated to be uncorrelated.


#+begin_export latex
    \clearpage
#+end_export
* The Theory of everything [cite:@theory-of-everything]

#+begin_quote
In light of this fact it strikes a thinking person as odd that the parameters e, ℏ, and m appearing in these equations may be measured accurately in laboratory experiments involving large numbers of particles. The electron charge, for example, may be accurately measured by passing current through an electrochemical cell, plating out metal atoms, and measuring the mass deposited, the separation of the atoms in the crystal being known from x-ray diffraction (11). Simple electrical measurements performed on superconducting rings determine to high accuracy the quantity the quantum of magnetic flux hc/2e (11). A version of this phenomenon also is seen in superfluid helium, where coupling to electromagnetism is irrelevant (12). Four-point conductance measurements on semiconductors in the quantum Hall regime accurately determine the quantity e2/h (13). The magnetic field generated by a superconductor that is mechanically rotated measures e/mc (14, 15). These things are clearly true, yet they cannot be deduced by direct calculation from the Theory of Everything, for exact results cannot be predicted by approximate calculations. This point is still not understood by many professional physicists, who find it easier to believe that a deductive link exists and has only to be discovered than to face the truth that there is no link. But it is true nonetheless. Experiments of this kind work because there are higher organizing principles in nature that make them work. The Josephson quantum is exact because of the principle of continuous symmetry breaking (16). The quantum Hall effect is exact because of localization (17). Neither of these things can be deduced from microscopics, and both are transcendent, in that they would continue to be true and to lead to exact results even if the Theory of Everything were changed. Thus the existence of these effects is profoundly important, for it shows us that for at least some fundamental things in nature the Theory of Everything is irrelevant. P. W. Anderson's famous and apt description of this state of affairs is “more is different” (2).
#+end_quote

#+begin_quote
Other important quantum protectorates include superfluidity in Bose liquids such as 4He and the newly discovered atomic condensates (19–21), superconductivity (22, 23), band insulation (24), ferromagnetism (25), antiferromagnetism (26), and the quantum Hall states (27). The low-energy excited quantum states of these systems are particles in exactly the same sense that the electron in the vacuum of quantum electrodynamics is a particle: They carry momentum, energy, spin, and charge, scatter off one another according to simple rules, obey fermi or bose statistics depending on their nature, and in some cases are even “relativistic,” in the sense of being described quantitively by Dirac or Klein-Gordon equations at low energy scales. Yet they are not elementary, and, as in the case of sound, simply do not exist outside the context of the stable state of matter in which they live. These quantum protectorates, with their associated emergent behavior, provide us with explicit demonstrations that the underlying microscopic theory can easily have no measurable consequences whatsoever at low energies. The nature of the underlying theory is unknowable until one raises the energy scale sufficiently to escape protection.
#+end_quote

#+begin_quote
In either case the low-energy excitation spectrum becomes more and more generic and less and less sensitive to microscopic details as the energy scale of the measurement is lowered, until in the extreme limit of low energy all evidence of the microscopic equations vanishes away.
The emergent renormalizability of quantum critical points is formally equivalent to that postulated in the standard model of elementary particles right down to the specific phrase “relevant direction” used to describe measurable quantities surviving renormalization.
#+end_quote

#+begin_quote
As the high- $T_c$ community has learned to its sorrow, deduction from microscopics has not explained, and probably cannot explain as a matter of principle, the wealth of crossover behavior discovered in the normal state of the underdoped systems, much less the remarkably high superconducting transition temperatures measured at optimal doping.
#+end_quote

* Electrodynamics of correlated electron materials [cite:@electrodynamics-corr-el-materials]

/Because multiple interactions play equally prominent roles in correlated systems, the resulting many-body state reveals a delicate balance between localizing and delocalizing trends./
/This balance can be easily disturbed by minute changes in the chemical composition, temperature, applied pressure, and electric and/or magnetic field.
/Thus, correlated electron systems are prone to abrupt changes of properties under applied stimuli and reveal a myriad of phase transitions/
/Quite commonly, it is energetically favorable for correlated materials to form spatially nonuniform electronic and/or magnetic states occurring on diverse length scales from atomic to mesoscopic./

* Further reading
- Thinking big [cite:@thinking-big-anderson]
- When electron falls apart [cite:@when-electron-falls-apart-anderson]

* References
#+print_bibliography:
