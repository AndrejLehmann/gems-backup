:PROPERTIES:
:ID:       f3a202f1-ad06-448b-b194-a26db3e48a94
:END:
#+title: Density functional theory
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:DFT:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

- /Since the core electrons of an atom are highly localized, it would be difficult to implement them using the plane waves basis sets./
  /Actually, a very large number of plane waves is required to expand their wave functions./
  /Furthermore, the contribution of the core electrons to bonding compared to those of the valence electrons is usually negligible./
  /Therefore, it is practically desirable to replace the atomic potential owing to the core electrons with a pseudopotential that has the same effect on the valence electrons./ [cite:@plasmonic-physics-of-2D-materials]

- About $5\%$ (some times $10\%$) difference to the experiment. [cite:@practical-DFT-1]

- Close to the previous X alpha approximation.

- Even though DFT is rather simple, it works so well due to 1) error cancellation of exchange and correlation terms, 2) fulfilled sum rules like $\int \d \vec{r}' n_{\rm xc}(\vec{r}, \vec{r}' - \vec{r}) = -1$ where $n_{\rm xc}$ is the density of exchange-correlation hole and 3) that the exchange-correlation energy depends only on the angle-average exchange-correlation hole (the non spherical parts of the exact $n_{\rm xc}$ and LDA approximation are very different, but when averaged over the angle they become similar within $5\%$ difference)

- DFT fails for correlated electron systems like Mott insulators, heavy fermions etc.
[cite:@electronic-structure-and-DFT]

- One has to keep in mind that the [[id:b5a08c58-15c9-445c-b4b8-75c1f871e3c8][Kohn-Sham theorem]] is only about the ground state (not the excited states) and the Kohn-Sham equations are single particle equations.

#+begin_export latex
  \clearpage
#+end_export
* References
#+print_bibliography:
