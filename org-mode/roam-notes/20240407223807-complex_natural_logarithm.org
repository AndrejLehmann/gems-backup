:PROPERTIES:
:ID:       a48a73d7-40aa-4fe1-b237-de9d12c22469
:END:
#+title: Complex natural logarithm
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Definition

The complex natural logarithm needs to be the inverse of $\exp(z)$ $z \in \mathbb{C}$.
But $\exp(z)$ is not injective since $\exp(z + \i 2\pi k) = \exp(z)$ where $k \in \mathbb{Z}$.
In order to have bijective maps we have to reduce the domain of $\exp(z)$ to be on a strip in the complex plain with $\Re(z) \in (-\infty , + \infty)$ and e.g. $\Im(z) \in (-\pi , \pi]$.
This way $\exp(z)$ maps to all complex numbers but the slit with $\Re\!\left(\exp(z)\right) \le 0$ and $\Im(\exp(z)) = 0$ (since the $\Im(z) = -\pi$ boundary is not included and $\exp(-\infty)=0$).
Lets call the domain of $\exp(z)$, as defined above, the $\text{stripe}$ and the image $D_{-\pi}$.
Then $\log: D_{-\pi} \to \text{stripe}$ and is called the principle value.
Note that you can choose any other stripe.
In order to understand why we need to exclude the slit consider
\begin{align*}
    &\log(\epx(\i \phi)) \xrightarrow{\phi \to \pi} \i \pi \\
    &\log(\epx(\i \phi)) \xrightarrow{\phi \to -\pi} -\i \pi \\
\end{align*}
So there is a jump we want to exclude.

* Properties

$\log(r \e^{\i \phi}) = \log(r) + \i \phi \; , \quad \phi \in (-\pi, \pi]$

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
