:PROPERTIES:
:ID:       1f7bf9ee-6d24-41bd-930b-75db6364061a
:END:
#+title: C#: classes
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :C#:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Every [[.NET type]] has a default value. Typically, that value is =0= for number types, and =null= for all
reference types. You can rely on that default value when it's reasonable in your app.

* primary constructor as part of the class declaration

#+begin_src csharp
public class Container(int capacity)
{
    private int _capacity = capacity;
}
#+end_src

* =required= modifier on a property

#+begin_src csharp
public class Person
{
    public required string LastName { get; set; }
    public required string FirstName { get; set; }
}
#+end_src

The addition of the required keyword mandates that callers must set those properties as part of a new expression:

#+begin_src csharp
var p1 = new Person(); // Error! Required properties not set
var p2 = new Person() { FirstName = "Grace", LastName = "Hopper" };
#+end_src

* inheritance

- You can inherit from any other class that isn't defined as sealed.
- You can implement one or more interfaces.
- You can only directly *inherite from one base class*

* visibility modifiers in classes

- public: for everyone everywhere
- private: same class/struct
- protected: same class and classes inherited to
- internal: same assembly
- file: same file


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
