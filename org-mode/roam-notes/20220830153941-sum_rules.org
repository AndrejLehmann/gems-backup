:PROPERTIES:
:ID:       221d040f-d5df-4901-a0b0-b4acb905fd55
:END:
#+title: Sum rules
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Introduction

- /The response functions including optical constants of materials obey numerous sum rules./[cite:@electrodynamics-corr-el-materials]
- /In actual calculations for real systems, one is usually only able to get approximate results for such quantities, which may not satisfy the sum rule exactly./
  /In such cases the extent to which the sum rule is approximately satisfied can be a useful measure of the quality of the approximations made./

* f-sum rule [cite:@electrodynamics-corr-el-materials]

/The most frequently used sum rule is the f-sum rule for the real part of the optical conductivity $\Re \,\sigma(\omega)$/
\begin{align*}
    \int_0^{\infty} \Re\,\sigma(\omega) \,\d \omega = \frac{\pi n e^2}{2 m_e}
\end{align*}
/This expression relates the integral of the dissipative part of the optical conductivity to the density of particles participating in optical absorption and their bare mass./
/The optical conductivity of a solid is dominated by the electronic response, and therefore an integration of the data using the equation can be compared to the total number of electrons including both core and valence electrons./

* Spectral function

\begin{align*}
    \int_{-\infty}^{\infty} \d \omega A(\vec{k},\omega) = 1
\end{align*}

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
