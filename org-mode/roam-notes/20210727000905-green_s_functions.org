:properties:
:ID:       79a5b733-0037-47db-9e2a-ec14cd518946
:end:
#+title: Green's functions
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Mathematical Green's functions for solution of linear differential equation $\hat{L}_{\vec{x}} \,  y(\vec{x}) = f(\vec{x})$

/The name physical Green's functions comes from the mathematical Green's functions used to solve inhomogeneous differential equations, to which they are loosely related.
/(Specifically, only two-point 'Green's functions' in the case of a non-interacting system are Green's functions in the mathematical sense; the linear operator that they invert is the Hamiltonian operator, which in the non-interacting case is quadratic in the fields.)/ [cite:@Greens-function-many-body-theory-wiki]
\begin{align*}
    \hat{L}_{\vec{x}} \, G(\vec{x}, \xi) &= \delta(\vec{x} - \xi) \quad \text{definition of the Green's function} \\
    f(\xi) \hat{L}_{\vec{x}} \, G(\vec{x}, \xi) &= f(\xi) \delta(\vec{x} - \xi)\\
    \hat{L}_{\vec{x}} \, f(\xi) G(\vec{x}, \xi) &= f(\xi) \delta(\vec{x} - \xi) \quad \text{since $\hat{L}_{\vec{x}}$ operates only on quatities depending on $\vec{x}$} \\
    \int_{\mathbb{R}} \hat{L}_{\vec{x}} \, f(\xi) G(\vec{x}, \xi) \, \d{\xi} &= \int_{\mathbb{R}} f(\xi) \delta(\vec{x} - \xi) \, \d{\xi} \\
    \hat{L}_{\vec{x}} \int_{\mathbb{R}} f(\xi) G(\vec{x}, \xi)\,\d{\xi} &= \int_{\mathbb{R}} f(\xi) \delta(\vec{x} - \xi) \, \d{\xi} \quad \text{since $\hat{L}_{\vec{x}}$ is linear}
\end{align*}
So finally
#+name: eq:solution-as-int-with-Green-func
\begin{align*}
    \hat{L}_{\vec{x}} \int_{\mathbb{R}} f(\xi) G(\vec{x}, \xi) \, \d{\xi} &= f(\vec{x}) \\
\end{align*}
Now if we define $\int_{\mathbb{R}} f(\xi) G(\vec{x}, \xi) \, \d{\xi} \coloneqq y(\vec{x})$ we see how $G(\vec{x},\xi)$ can be used to solve $\hat{L}_{\vec{x}} \, y(\vec{x}) = f(\vec{x})$ .
We see that Green's functions are very usefull general tool for finding solutions of linear differential equations.
For some given $f(x)$ we can calculate a solution independed of the linear operator.
Thus the Green's function can be viewed as the main ingredient in the general solution $\int f(\xi) G(x,\xi) \; \d\xi$ of any linear differential equation for a given $f(x)$.
Boundary/initial conditions ensure that $G(\vec{x})$ is unique.
Green's functions are especially usefull when the boundary/initial conditions are homogeneous.
If boundary/initial conditions are /not/ homogeneous Green's functions can not help directly but one can consider Green's 3rd identity for finding the solution.

* Green's functions in condensed matter theory

In condensed matter theory Green's function arise in the [[id:f090b365-061e-4aa6-a2d3-d6bf6302d110][linear response theory]] as linear response functions (also called susceptibility).
Green's functions can also be introduced more elegantly via path integral formalism in QFT without linear response theory by using the generating functionals.

Propagators are for example Green's functions and are geometric sums of diagrams with two external lines
\begin{align*}
    G = G_0 + G_0 \, \Sigma \, G_0 + G_0 \, \Sigma \, G_0 \, \Sigma \, G_0 + \dots = \frac{G_0}{1-\Sigma G_0}
\end{align*}
$\Sigma$ is the so called [[id:49cfde4b-3f5d-4fd1-8e4f-05865be74310][self energy]], a classification of certain diagrams.

In physics the Green's functions can be [[id:94fa296d-1dd4-4083-840c-e52a885fa632][measured experimentally]].

** One particle retarded Green's function

The one particle retarded Green's function is the generalization of the [[id:f090b365-061e-4aa6-a2d3-d6bf6302d110][linear response functions]] since the operators we insert into the Kubo formula are not linear.
\begin{align*}
    G_{ab}(t-t') = -i \Theta(t-t') \left\langle\{ c_a(t) , c^\dagger_b(t') \} \right\rangle\
\end{align*}
where $a$ and $b$ are quantum numbers corresponding to some states of a complete orthonormal set.
E.g. $a$ and $b$ can be [[id:64f63790-fdd3-42ca-a4a3-9239137c9c6e][Wannier functions]] on different sites.
Note that compared to the operators $A$ and $B$ in the Kubo formula noted [[id:f090b365-061e-4aa6-a2d3-d6bf6302d110][here]], $c_a$ and $c^{\dagger}_b$ are not hermitian and you can not calculate average value of.
Analogously to the analysis in [[id:f090b365-061e-4aa6-a2d3-d6bf6302d110][linear response theory]] we obtain for the retarded Green's function in the frequency domain
\begin{align*}
    G_{ab}(\omega) = \sum_{n,m} (p_n+p_m) \frac{\bra{n}c_a\ket{m} \bra{m}c^{\dagger}_b\ket{n}}{\omega^+ - \Delta E_{mn}}
\end{align*}
and from it's imaginary part the spectral function
\begin{align*}
    A_{ab}(\omega) = - \frac{1}{\pi} \Im \, G_{ab}(\omega^+) = \sum_{n,m} (p_n+p_m) \bra{n}c_a\ket{m} \bra{m}c^{\dagger}_b\ket{n} \; \delta(\omega - \Delta E_{mn})
\end{align*}
On can also express spectral function in terms of [[id:49cfde4b-3f5d-4fd1-8e4f-05865be74310][self energy]], where one can see that $\Im \Sigma_{ab}(\omega) \le 0$  (e.g. see [[eq:spectral-function-in-terms-of-Sigma-for-free-fermions]]), which can be used as generall check for calculations.
We can also verify that the autocorrelator $A_{aa}(\omega)$ satisfies the characteristic property of probability density
\begin{align*}
    \int\limits_{-\infty}^{\infty} A_{ab}(\omega) = \sum_{n,m} (p_n+p_m) \bra{n}c_a\ket{m} \bra{m}c^{\dagger}_b\ket{n} = \sum_{n} p_n \bra{n} \underbrace{c_a c^{\dagger}_b + c^{\dagger}_b c_a}_{=\,\delta_{ab}} \ket{n} = \delta_{ab}
\end{align*}
and thus
\begin{align*}
    \int\limits_{-\infty}^{\infty} A_{aa}(\omega) = 1
\end{align*}
One can consider $A_{aa}(\omega)$ which allows for easier interpretation
\begin{align*}
    A_{aa}(\omega) = \sum_{n,m} (p_n+p_m) \left|\bra{m}c^{\dagger}_a\ket{n}\right|^2 \delta(\omega - \Delta E_{nm})
\end{align*}
Green's functions can be introduced more elegantly via path integral formalism in QFT without linear response theory by using the generating functionals.
There one can see that Green's function can also be used for calculating non-linear responses (correct?).

Note that no assumptions about the Hamiltonian have been made.

*** Free fermions [?]

\begin{align*}
    H = \sum_{\vec{k}} \underbrace{(\frac{\vec{k}^2}{2} - \mu)}_{\coloneqq \xi(\vec{k})} c^{\dagger}_{\vec{k}\sigma} c_{\vec{k}'\sigma'}
\end{align*}

States $\ket{n}$ of free particles are all possible Slater-determinats made out of plane waves for any number of particles.

\begin{align*}
    G_{\vec{k}\sigma,\vec{k}'\sigma'}(t-t') = -i \Theta(t-t') \expect{\{ c_{\vec{k}\sigma}(t) , c^{\dagger}_{\vec{k}'\sigma'}(t')\}}
\end{align*}

\begin{align*}
    & c_{\vec{k}\sigma}(t) \ket{n} = \e^{iHt} c_{\vec{k}\sigma} \underbrace{\e^{-iHt} \ket{n}}_{= \, \e^{-iE_nt} \ket{n}} = \e^{-iE_{n}t} \underbrace{\e^{iHt} c_{\vec{k}\sigma} \ket{n}}_{= \, \e^{i(E_n-\xi_{\vec{k}})} \ket{n}} \\
    & \Rightarrow  c_{\vec{k}\sigma}(t) = \e^{-i\xi_{\vec{k}}} c_{\vec{k}\sigma}
\end{align*}

\begin{align*}
    G_{\vec{k}\sigma,\vec{k}'\sigma'}(t-t') = -i \; \Theta(t-t') \; \e^{-i\xi_{\vec{k}}t} \e^{-i\xi_{\vec{k}'}t'} \expect{\underbrace{\{ c_{\vec{k}\sigma} , c^{\dagger}_{\vec{k}'\sigma'}\}}_{=\,\delta_{\vec{k}\vec{k}'} \delta_{\sigma\sigma'}}} & = -i \; \Theta(t-t') \; \e^{-i\xi_{\vec{k}}(t-t')} = G_{\vec{k}\sigma}(t-t') \quad \\ &\text{? Vergleiche mit Potthoffs condense matter script Folie 103}
\end{align*}
(compare to time evolution $\e^{-iEt/\hbar}$)
\begin{align*}
    A_{\vec{k}\sigma,\vec{k}'\sigma'}(\omega) = - \frac{1}{\pi} \Im \frac{1}{\omega^+ - \xi(t)}  \delta_{\vec{k}\vec{k}'} \delta_{\sigma\sigma'} = \delta(\omega - \xi(\vec{k})) = A_{\vec{k}\sigma}(\omega)
\end{align*}
Now we see that we obtain a collection of energies (DOS).
In term of [[id:49cfde4b-3f5d-4fd1-8e4f-05865be74310][self energy]] we can also express $A_{\vec{k}\sigma}(\omega)$ as
#+name: eq:spectral-function-in-terms-of-Sigma-for-free-fermions
\begin{align*}
    A_{\vec{k}\sigma}(\omega) = - \frac{1}{\pi} \frac{\Im \Sigma_{\vec{k}\sigma}(\omega)}{\left( \omega - \xi(\vec{k}) - \Re\Sigma_{\vec{k}\sigma}(\omega) \right)^2 + \left( \Im \Sigma_{\vec{k}\sigma} \right)^2}
\end{align*}
where we can see the that $\Im \Sigma(\omega)_{\vec{k}\sigma} \le 0$ in order to $A_{\vec{k}\sigma}(\omega) \ge 0$, that can be used as a generall check for calculations.

** Imaginary time Green's functions

\begin{align*}
    G_{ab}(\tau_a,\tau_b) = - \left\langle \mathcal{T} \hat{c}_a(\tau_a) \hat{c}^{\dagger}_b(\tau_b) \right\rangle
\end{align*}

* Further reading

- [[id:674d7235-bb35-44fd-b0be-b890dc009c14][Relation between spectral function and Green's function in low temperature limit]]
- [[https://folk.ntnu.no/johnof/green-2013.pdf][Introduction to Green functions and many-body perturbation theory - Good, short read]]


#+begin_export latex
  \clearpage
#+end_export


* References
#+print_bibliography:
