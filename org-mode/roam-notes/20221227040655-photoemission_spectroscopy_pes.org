:PROPERTIES:
:ID:       f8e4b7dd-b0af-4ab9-820d-e14c2d5e2536
:END:
#+title: Photoemission spectroscopy (PES)
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:experiments:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

#+caption: Schematic depiction of the physics behind photoemission spectroscopy
#+attr_org: :width 800
#+attr_latex: :width 10cm :placement [!htpb]
#+name:   fig:PES
[[./images/photoemission-spectroscopy.png]]

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
