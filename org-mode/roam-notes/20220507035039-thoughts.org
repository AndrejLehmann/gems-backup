:PROPERTIES:
:ID:       088344f1-cd5b-40d6-bc86-e9f0c4418dc7
:END:
#+title: Thoughts
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* In sich selbst investieren

Warum ist Investieren in eigene F"ahigkeiten (Mathe, Programmieren, ...) macht mehr Spass als Videospiele (oder Sport).
Wenn du Videospiele spielst dann lernst du auch.
Man lernt Mechaniken des Spiels und wird geschickter darin.
Je mehr man spielt, desto mehr Zeit investiert man in Entwicklung dieser F"ahigkeiten.
Und das Spiel mach dadurch /Spass/, dass /man besser wird/.
Denn innerhalb des Spiels sind diese F"ahigkeiten wertvoll.
Aber sie sind eben nur innerhalb des Spiels wertvoll.
Wenn man aber etwas lernt, was man in der echten Welt eine wertvolle Verwendung hat, (Mathe, Programmieren, ...) dann macht es eig. noch mehr Spass.
Aber die meisten solcher Dinge sind Sachen, die man lange lernen muss bis man sie anwenden kann.
Sodass der /positive Feedback/ erst recht sp"at kommt.
Bei Videospielen kommt der positive Feedback aber sehr schnell (z.B. man besiegt einen Gegner).
Videospiele machen deswegen /schneller/ Spass, (aber nicht so /viel/ wie real-world-F"ahigkeiten).

* Frank Thelen, Elon Mask die shaper der Welt

Frank Thelen, Elon Mask als Shaper der Welt
-> reich -> viel Investitionspotential = Macht
-> treffen alleine Entscheidungen wohin sie investieren = wie sie die Welt ver"andern
-> Arroganz so was selber am besten wissen zu k"onnen
=> W"are es nicht besser sich ein System (Menge von Regeln nach dennen die Menschen zusammen arbeiten) zu "uberlegen,
   durch den man gemeinschaftlich die Welt formt,
   und in dieses System (und dessen Entwicklung) zu investieren.
-> in Entwicklung von Demokratien zu investieren

* Lernen die selben Dinge aus verschiedenen Perspektiven zu verstehen

Lernen die selben (wichtigen) Dinge aus verschiedenen Perspektiven zu verstehen, statt neue Dinge zu lernen.

#+attr_org: :width 300
[[./images/perspectivas.jpeg]]

Wie viele Perspektiven sind notwendig um das Problem des Rassismus zu l"osen?
