:PROPERTIES:
:ID:       b429cfe8-4bf6-45d9-8181-c1a4a7d628bc
:END:
#+title: Haskell: Case expressions vs pattern matching
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :programming:haskell:

#+begin_src haskell :exports both
head' :: [a] -> a
head' xs = case xs of [] -> error "No head for empty lists!"
                      (x:_) -> x
#+end_src

is equivalent to

#+begin_src haskell :exports both
head' :: [a] -> a
head' [] = error "No head for empty lists!"
head' (x:_) = x
#+end_src

(If it falls through the whole case expression and no suitable pattern is found, a runtime error occurs.)
But src_haskell{case} expressions can be used everywhere not just to define a function.

#+begin_src haskell :exports both
describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of []  -> "empty."
                                               [x] -> "a singleton list."
                                               xs  -> "a longer list."
#+end_src

vs

#+begin_src haskell :exports both
describeList :: [a] -> String
describeList xs = "The list is " ++ what xs
    where what [] = "empty."
          what [x] = "a singleton list."
          what xs = "a longer list."
#+end_src
