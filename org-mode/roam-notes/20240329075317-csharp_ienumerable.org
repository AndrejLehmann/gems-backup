:PROPERTIES:
:ID:       b6b732ce-077e-4441-af83-bef928644fca
:END:
#+title: C#: =IEnumerable=
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :C#:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Iteratable objects with =yield= (on demand item creation)

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
