% Created 2024-11-11 Mon 04:28
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{mathtools}
\usepackage{cancel}
\usepackage{stackrel}
\usepackage{dsfont}
\usepackage{bbold}
\usepackage{mathtools}
\usepackage{tikz}
\usepackage{amsmath}
\usetikzlibrary{shapes.geometric, arrows, positioning, automata, fit}
\tikzstyle{stdnode} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=grey!78]
\tikzstyle{arrow} = [thick, ->, >=stealth]%, red]
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\renewcommand{\mat}[1]{\boldsymbol{#1}}
\renewcommand{\d}{\mathrm{d}}
\renewcommand{\Tr}{\mathrm{Tr}}
\newcommand{\bra}[1]{\langle #1 \rvert}
\newcommand{\ket}[1]{\lvert #1 \rangle}
\newcommand{\braket}[2]{\langle #1 \rvert #2 \rangle}
\newcommand{\expect}[1]{\left\langle #1 \right\rangle}
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\e}{\mathrm{e}}
\renewcommand{\i}{\mathrm{i}}
\newcommand{\ln}{\mathrm{ln}}
\newcommand{\T}{\mathrm{T}}
\renewcommand{\Re}{\mathrm{Re}\,}
\renewcommand{\Im}{\mathrm{Im}\,}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\up}{\uparrow}
\newcommand{\dn}{\downarrow}
\newcommand{\implies}{\Rightarrow}
\author{Andrej Lehmann}
\date{\today}
\title{Fatigue Loads}
\hypersetup{
 pdfauthor={Andrej Lehmann},
 pdftitle={Fatigue Loads},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.2 (Org mode 9.6.1)}, 
 pdflang={English}}
\usepackage{calc}
\newlength{\cslhangindent}
\setlength{\cslhangindent}{1.5em}
\newlength{\csllabelsep}
\setlength{\csllabelsep}{0.6em}
\newlength{\csllabelwidth}
\setlength{\csllabelwidth}{0.45em * 3}
\newenvironment{cslbibliography}[2] % 1st arg. is hanging-indent, 2nd entry spacing.
 {% By default, paragraphs are not indented.
  \setlength{\parindent}{0pt}
  % Hanging indent is turned on when first argument is 1.
  \ifodd #1
  \let\oldpar\par
  \def\par{\hangindent=\cslhangindent\oldpar}
  \fi
  % Set entry spacing based on the second argument.
  \setlength{\parskip}{\parskip +  #2\baselineskip}
 }%
 {}
\newcommand{\cslblock}[1]{#1\hfill\break}
\newcommand{\cslleftmargin}[1]{\parbox[t]{\csllabelsep + \csllabelwidth}{#1}}
\newcommand{\cslrightinline}[1]
  {\parbox[t]{\linewidth - \csllabelsep - \csllabelwidth}{#1}\break}
\newcommand{\cslindent}[1]{\hspace{\cslhangindent}#1}
\newcommand{\cslbibitem}[2]
  {\leavevmode\vadjust pre{\hypertarget{citeproc_bib_item_#1}{}}#2}
\makeatletter
\newcommand{\cslcitation}[2]
 {\protect\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother\begin{document}

\maketitle
\clearpage \tableofcontent \clearpage

\section{Stress or Load \(S\)}
\label{sec:orgf3eb66b}
\(S = F / A\), where \(F\) is resisting force and \(A\) is resisting area. \([\sigma] = {\rm N / m^{2}}\)

\section{Strain \(\varepsilon\)}
\label{sec:orgee6e647}
\(\varepsilon = \delta D_{0} / D_{0}\), where \(D_{0}\) is the original dimension before deformation (e.g. length) and \(\delta D_{0}\) is the change in dimension

\section{Young's Modulus or Elastic Constant \(K\)}
\label{sec:org5d40e73}
\(K = \sigma/\epsilon\). \([K] = {\rm N/m^2}\)

\section{Stress-Strain Curve}
\label{sec:org2ddacb8}
\begin{figure}[!htpb]
\centering
\includegraphics[width=10cm]{./images/stress-strain-curve-example.png}
\caption{\label{fig:name}Stress-strain curve typical of low-carbon steel \cslcitation{1}{[1]}}
\end{figure}

\clearpage

\section{\(S\text{-}N\) Curve}
\label{sec:org7981a81}
\begin{figure}[!htpb]
\centering
\includegraphics[width=10cm]{./images/S-N-fatigue-curve.jpeg}
\caption{\label{fig:name}\(S\text{-}N\) curve. Relationship between the stress amplitude \(S\) and the fatigue life \(N\), number of cycles to failure}
\end{figure}

\begin{itemize}
\item \(N(S) \propto S^{-m}\) where \(m\) is called Woehler parameter.
\item \(N_2 S_2^m = N_1 S_1^m\)
\end{itemize}

\section{Miner's Rule}
\label{sec:org9fa500b}
Consider stress magnitudes in a spectrum, \(S_i\) \((1 \le i \le k)\), each contributing \(n_i\) cycles, and stress mean value \(\mu=0\) then if \(N_i(S_i)\) is the number of cycles to failure of a constant stress reversal \(S_i\) (determined by uni-axial fatigue tests), failure occurs when damage \(D = 1\):
\begin{align*}
    D = \sum_{i=1}^k \frac {n_i} {N_i} = 1
\end{align*}

\section{Fatigue Limit}
\label{sec:orgbcfcc0e}
Does not actually exist. The number of cycles till failure is just large.

\section{Damage-Equivalent Load (DEL)}
\label{sec:org757b82c}
Stress amplitude \(S_{\rm DEL}\) constantly applied for \(N_{\rm ref}\) cycles that makes the same damage \(D\) as the load spectrum \(n_i\)
\begin{align*}
    D = \sum_{i=1}^k \frac {n_i} {N_i} = \frac{N_{\rm ref}}{N(S_{\rm DEL})}
\end{align*}
where \(N(S_{\rm DEL})\) is the fatigue life of the constant stress amplitude \(S_{\rm DEL}\).
From s\(N_2 S_2^m = N_1 S_1^m\) one can derive
\begin{align*}
    &N_{\rm ref} S_{\rm DEL}^{m} = \sum_{i=1}^{k} n_i S_i^m \\
    &S_{\rm DEL} = \left( \sum_{i=1}^{k} \frac{n_i}{N_{\rm ref}} S_i^m \right)^{\frac{1}{m}}
\end{align*}
One can rewrite the equation in terms of time. Translate \(n_i\) to time \(T_i\) and choose \(N_{\rm ref}\) according to total life time of interest e.g. \(20\) years.
Then we obtain
\begin{align*}
    S_{\rm MEQn} = \left( \sum_{i=1}^{k} \frac{T_i}{20\,\text{years}} S_i^m \right)^{\frac{1}{m}}
\end{align*}
Or similar in terms of number of revolutions
\begin{align*}
    S_{\rm MREQn} = \left( \sum_{i=1}^{k} \frac{r_i}{10^7\,\text{revolutions}} S_i^m \right)^{\frac{1}{m}}
\end{align*}

\section{Fatigue Life Estimation Procedure}
\label{sec:org75d4684}
\begin{enumerate}
\item Simulate via Alaska/Bladed time series of applied load cycles. Consider time series to be periodic.
\item Count cycles via rainflow-counting algorithm
\item Bin load levels and load means in the time series  \(\to\) Markov Matrix: table with 3 columns: bined load level \(S_i\), bined load mean, number of cycles \(n_i\)
\item neglect the stress means
\item Calculate damage-equivalent load \(S_\text{DEL}\)
\end{enumerate}

\section{Crack Growth Method}
\label{sec:orgb18507c}
Alternative fatigue life estimation method based on crack growth equation.
One of the earliest crack growth equations based on the stress intensity factor (stress intensity near the tip of a crack or notch) range of a load cycle \(\Delta K\) is the Paris–Erdogan equation
\begin{align*}
    {da \over dN} = C(\Delta K)^m
\end{align*}
where \(a\) is the crack length and \({\rm d}a/{\rm d}N\) is the fatigue crack growth for a single load cycle \(N\). A variety of crack growth equations similar to the Paris–Erdogan equation have been developed to include factors that affect the crack growth rate such as stress ratio, overloads and load history effects.

\section{Questions}
\label{sec:orgbe11236}
\begin{itemize}
\item How long is the load time series? 1h?
\item Does considering the load time series to be periodic mean that we assume that the time series proceeds periodically for 20 years?
\item How is the load time series constructed? How are the wind conditions used?
\item What is the justification for periodicity?
\end{itemize}

\clearpage

\section{References}
\label{sec:org446767e}
\begin{cslbibliography}{0}{0}
\cslbibitem{1}{\cslleftmargin{[1]}\cslrightinline{I. Wikimedia Foundation, “Stress-strain curve.” https://en.wikipedia.org/wiki/Stress–strain\_curve, 2024.}}

\end{cslbibliography}
\end{document}