:PROPERTIES:
:ID:       f9d24675-4008-4400-a0cc-a426af265fef
:END:
#+title: CDMFT
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Josefs Kagome Paper [cite:@how-correlations-change-mag-structure-factor-on-kagome]

/In the conceptually simple cellular DMFT (CMDFT) approach, such clusters are constructed in real space and typically comprise a small number of sites./
/Correlations in CDMFT are still purely local, but the locality spans now the entire cluster./
/As a result, nonlocal correlations at the length scale of the cluster are included, while longer-range fluctuations are still absent./
/In addition to this sharp cutoff between the short-range (included) and long-range (omitted) correlations, CDMFT introduces a spurious disparity between the sites falling within the cluster and all other sites./
/We can illustrate this by considering the kagome lattice, where each site has four equivalent nearest neighbors./
/In the simplest possible cluster, a triangle, this equivalence is violated: only two of the four neighboring sites belong to the cluster./

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
