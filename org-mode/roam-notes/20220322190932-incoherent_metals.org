:PROPERTIES:
:ID:       b43e1056-c596-49e2-b40e-971e2ee033b7
:END:
#+title: Incoherent metals
#+setupfile: ~/gems-backup/org-mode/general-latex-header.org
#+setupfile: ~/gems-backup/org-mode/math-latex-header.org
#+bibliography: references.bib
#+cite_export: csl ../citation-styles/ieee.csl
#+filetags: :condensed-matter-theory:materials:

* [cite: @metal-insulator-transitions]

" Strong incoherence actually turned out to be a common property of most transition-metal compounds near the Mott transition point (see fig:metal-insulator-phase-diagram in [[id:0079a22e-05ef-4981-a7fd-adac6b358280][Hubbard Model]]).
  The incoherent charge dynamics as well as other anomalies in spin, charge, and orbital fluctuations all come from almost localized but barely itinerant electrons in the critical region of transition between metals and the Mott insulator.
  These critical regions are not easy to understand from either the metal or the insulator side, but their critical properties can be analyzed by the techniques for quantum critical phenomena. "
