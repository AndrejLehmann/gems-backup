:PROPERTIES:
:ID:       dec3676c-2617-4147-9984-1d32afc8acd1
:END:
#+title: Fermi surface for interacting particles
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Luttinger's theorem [cite:@luttingers-theorem]

/In the interacting case the Fermi surface must be defined according to the criteria that $G(\omega=0,\,p) \to 0$ or $\infty$ where $G$ is the single-particle [[id:79a5b733-0037-47db-9e2a-ec14cd518946][Green's function]] in terms of frequency and momentum./
And as the consequence of the [[id:4e7c4b04-c9e4-4299-bb3b-5ff576f64feb][Luttinger's theorem]]
\begin{align*}
    n = 2 \int_{G(\omega=0,p)>0}\frac{d^D k}{(2\pi)^D}
\end{align*}

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
