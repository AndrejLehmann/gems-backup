:properties:
:ID:       49cfde4b-3f5d-4fd1-8e4f-05865be74310
:end:
#+title: Self energy
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

The sum over one particle irreducible diagrams (diagrams that can not be separated by cutting one propagator).
This irreducible diagrams, can also be viewed as undressed skeleton diagrams.
Self energy is then a sum of dressed skeleton diagrams.

* Further reading

[[id:1d4e4b01-ab95-4d8c-9c09-080ef5ee983c][Why the self-energy is much more noisy than the Green's function]]
[[id:94fa296d-1dd4-4083-840c-e52a885fa632][Experimental measurement of the many-body correlation functions]]
