:PROPERTIES:
:ID:       433fca6d-d154-4bce-9137-aef14b8078ac
:END:
#+title: Empty set is subset of any set 
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:logic:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Consider $A \subseteq B$.
It is equivalent by definition of subsets to $x \in A \Rightarrow x \in B$, meaning if $x$ is in $A$ then $x$ is also in $B$.
Now consider $\emptyset \subseteq X$, where $X$ is any set.
This we can rewrite as $x \in \emptyset \Rightarrow x \in X$.
Since $x \in \emptyset$ is false by the definition of the empty set, the implication is true.
We can also express it like this:
$A$ is subset of $B$ if all elements of $A$ are also elements of $B$.
Since $\emptyset$ has no elements, all of it's elements are in any set.


#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
