:PROPERTIES:
:ID:       4cf16305-5a54-4ba0-a870-b947ab56e42f
:END:
#+title: org-mode: Figures
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :emacs:org-mode:

#+caption: caption
#+attr_org: :width 800
#+attr_latex: :width 10cm :placement [!htpb]
#+name:   fig:name
[[./images/filename.jpg]]
