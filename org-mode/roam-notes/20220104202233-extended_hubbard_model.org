:properties:
:ID:       bad98d32-a0fe-4e94-8d1b-cb8df91fb5fb
:end:
#+title: Extended Hubbard model
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* Extended Hubbard model [cite:@beyond-EDMFT_2d-extended-Hubbard]

\begin{align*}
    H = \sum_{ij\sigma} t_{ij} c_{i\sigma}^{\dagger} c_{j\sigma} + \sum_i U n_{i\up} n_{i\dn} + \frac{1}{2} \sum_{ij} V_{ij} n_i n_j
\end{align*}

Examples with nonnegligible nonlocal contribution of interaction are adatoms on semiconductor surfaces (Si) with intersite interaction being of order 30% of the on-site interaction [cite:@long-range-coulomb-int_GW-DMFT].
"Moreover, the nonlocal interaction decays slowly as $1/r$ with distance $r$, as determined by the static dielectric constant of the substrate, rendering even long-range contributions to the interaction important.
The screening effect of the nonlocal interaction can make a material appear metallic, which would be on the verge of the insulating state if only the on-site Coulomb interaction were considered, as observed in graphene [cite:@effective-coulomb-int_graphene-graphite].
For graphene, benzene and silicene, the nonlocal terms were found to reduce the effective local interaction by more than a factor of 2 [cite:@optimal-Hubbard-model_graphene-silicene-benzene].
In metals and semiconductors, the long-range Coulomb interaction leads to plasmons and can induce charge-ordering transitions."

The nearest neighbor interaction $V$ can flatten the impurity self energy and the three-leg vertex making the material less correlated.
It was also shown that corrections by the fermionic self-energy diagrams have almost no effect on the phase diagram.
Also the procedure with the outer self consistent loop does not effect the phase diagram but reduces the susceptibility.

#+begin_export latex
    \clearpage
#+end_export
* References
#+print_bibliography:
