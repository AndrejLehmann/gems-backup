% Created 2022-07-22 Fri 00:46
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{mathtools}
\usepackage{stackrel}
\usepackage{dsfont}
\usepackage{bbold}
\usepackage{mathtools}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows, positioning, automata, fit}
\tikzstyle{stdnode} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=grey!78]
\tikzstyle{arrow} = [thick, ->, >=stealth]%, red]
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\renewcommand{\mat}[1]{\boldsymbol{#1}}
\renewcommand{\d}{\mathrm{d}}
\renewcommand{\Tr}{\mathrm{Tr}}
\newcommand{\bra}[1]{\langle #1 \rvert}
\newcommand{\ket}[1]{\lvert #1 \rangle}
\newcommand{\braket}[2]{\langle #1 \rvert #2 \rangle}
\newcommand{\expect}[1]{\left\langle #1 \right\rangle}
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\e}{\mathrm{e}}
\newcommand{\ln}{\mathrm{ln}}
\newcommand{\T}{\mathrm{T}}
\renewcommand{\Re}{\mathrm{Re}}
\renewcommand{\Im}{\mathrm{Im}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\up}{\uparrow}
\newcommand{\dn}{\downarrow}
\newcommand{\implies}{\Rightarrow}
\author{Andrej Lehmann}
\date{\today}
\title{Linear response theory}
\hypersetup{
 pdfauthor={Andrej Lehmann},
 pdftitle={Linear response theory},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.6)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle

\section{Linear response}
\label{sec:org61a5b14}

Consider a Hamiltonian \(H_f\) that describes a system that experiences some stimulus
\begin{align*}
    H_f(t) = H_0 - f(t)B
\end{align*}
where \(f(t)\) is spatially constant force field that is coupled to an observable/operator \(B\) (e.g. \(B\) can be the total spin of the system and \(f(t)\) the magnetic field in the \(z\) direction).
The response of the observable \(A\) is then defined as
\begin{align*}
    \Delta A(t) = \expect{A}_{H_f(t)} - \expect{A}_{H_0}
\end{align*}
where expectation can be calculated according to \href{20210805090009-density_operator.org}{Density operator} in the \href{20210807003343-grand_canonical_ensemble.org}{Grand canonical ensemble} .

Assume linear response (example of non-linear [power law] response would be a system at critical point e.g. \href{20220323163753-mean_field_approach_to_ising_model.org}{Ising model}) then \(\Delta A(t)\) is given by the convolution
\begin{align}
\label{eq:Delta_A_of_t}
    \Delta A(t)= \int\limits_{-\infty}^{\infty} \d t' \; \chi_{AB}(t,t') \, f(t')
\end{align}
where \(\chi\) is the linear response function (also called susceptibility)  that is usually dependend only on the difference of the time arguments and then can be written as \(\chi_{AB}(t,t') = \chi_{AB}(t-t')\) .
Also the responds function has to respect causality so \(\chi_{AB}(t-t') = 0 \; \forall \; t-t'<0\).
It can be calculated via Kubo formula
\begin{align}
\label{eq:Kubo}
    \chi_{AB}(t-t')= -i \Theta(t-t') \left\langle \left[ A_t, B_{t'} \right] \right\rangle
\end{align}
with \(A_t = \e^{ iH_0t} A \e^{-iH_0t}\) and analogously for \(B_{t'}\) .
One has to note that \ref{eq:Delta_A_of_t} is general and does not depend on the system and even not on the theory being classical or quantum.
The consideration of the system and the theory come into play when calculating \(\chi_{AB}(t-t')\).
There is also classical version of \hyperref[eq:Kubo]{Kubo formula} .
Via Fourier transformation \(f(\omega) = \int\limits_{-\infty}^{\infty} \d t \; \e ^{i\omega t} f(t)\) we can rewrite \ref{eq:Delta_A_of_t} as
\begin{align*}
    \Delta A(\omega) = \chi_{AB}(\omega) f(\omega)
\end{align*}
Now let's derive how to calculate \(\chi_{AB}(\omega)\) more conveniently.
We can expand right hand side of the \hyperref[eq:Kubo]{Kubo formula} and insert the \(\mathds{1}\) from the complete set of states between \(A_t\) and \(B_{t'}\)
\begin{align*}
    -i \Theta(t-t') \sum_n p_n \bra{n} A_t B_{t'} - B_{t'} A_t \ket{n}= -i \Theta(t-t') \sum_{n,m} p_n \; \left( \bra{n} A_t \ket{m} \bra{m}B_{t'}\ket{n} - \bra{n}B_{t'}\ket{m}\bra{m}A_t\ket{n} \right)
\end{align*}
Using \(\bra{n}A_t\ket{m}= \bra{n}A\ket{m}\e^{-i \Delta E_{mn} t},\; \Delta E_{mn} = E_m-E_n\) (and renaming \(n\) and \(m\) in the second term) and analogous expression for the \(B_{t'}\) terms we obtain
\begin{align*}
    \chi_{AB}(t-t')= -i \Theta(t-t') \sum_{n,m} (p_n - p_m) \bra{n}A\ket{m}  \bra{m}B\ket{n} \e^{-i \Delta E_{mn}t}
\end{align*}
Next performing Fourier transformation and using the \href{20211105190318-lemma_of_complex_analysis.org}{lemma of complex analysis} we finally can write \(\chi_{AB}(\omega)\) in the simple form
\begin{align*}
    \chi_{AB}(\omega) = \sum_{n,m} (p_n-p_m) \frac{\bra{ n}A\ket{m}\bra{m}B\ket{n}}{\omega^+ - \Delta E_{mn}}
\end{align*}
This is the so called \emph{Lehmann representation}.
In order to understand this result let's consider as an example an autocorrelator \(\chi_{AA}(\omega^+)\) (such a correlator can describe conductivity, the current-current correlation)
The imaginary part describes the dissipation of energy
\begin{align*}
    \Im \; \chi_{AA}(\omega)= -\pi \sum_{m,n} (p_n - p_m) \; |\bra{n}A\ket{m}|^2 \; \delta(\omega - \Delta E_{mn})
\end{align*}
We see that if the driving frequency \(\omega\) matches the energy difference \(\Delta E_{mn}\) we have a non-zero value (i.e. then the stimulus \(B\) brings the system from \(n\) to \(m\) and the response \(A\) brings it back and we calculate the difference \((p_n - p_m)\) to know how much got lost).
This also can be viewed as a generalization of density of states concept.
Even more remarkably the real part of the response can be expressed through the imaginary part
\begin{align*}
    \Re \; \chi_{AA}= \frac{1}{\pi} \; \mathcal{P} \!\! \int \d \omega' \frac{\Im \; \chi_{AA}(\omega')}{\omega - \omega'}
\end{align*}
with \(\mathcal{P}\!\int\) being \href{https://en.wikipedia.org/wiki/Cauchy\_principal\_value}{Cauchy principle value integral} .
This is known as \emph{Kramers–Kronig relation}.
Thus one can conclude that the imaginary part completely characterizes the response of the system.

We can also analytically continue \(\chi_{AB}(\omega)\) to the whole complex plane by substituting \(\omega^+ \to z, \; z \in \mathbb{C}\).
Then one can show that causality guaranties that \(\chi_{AB}(z)\) is analytic for \(\Im \,z > 0\)

For processes like where electrons are pulled out of the system by e.g. photoelectric effect we need a more general theoretical framework of correlation functions, so we introduce \href{20210727000905-green_s_functions.org}{Green's functions}.
\end{document}
