:PROPERTIES:
:ID:       94ad2f03-b06f-422c-976b-6e55641187d7
:END:
#+title: Python: tuple and record types
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :python:programming:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

- immutable

* named tuples

#+begin_src python :results output :exports both
from typing import NamedTuple # python3.6
from collections import namedtuple # python2.6

print("standard tuple:")
p1 = (1.0, 5.0) # standard tuple
print(p1)
print(p1[0],p1[1])
#p1[0] = 2.0 # error since tuples are immutable
x1, y1 = p1
print(x1, y1)

print("\nnamed tuple:")

Point = namedtuple('Point', 'x y') # named tuple
p2 = Point(x=1.0, y=5.0)
print(p2) # more readable output
print(p2.x, p2.y) # object-like dereferencing
x2, y2 = p2 # backwards compatibility with standard tuples (Liskov substitutability)
print(x2, y2)

# python 3.6
class Point(NamedTuple):
    """representation of a point in a 2D space """
    x: float
    y: float

p3 = Point(x=1.0, y=5.0)
#+end_src

#+RESULTS:
: standard tuple:
: (1.0, 5.0)
: 1.0 5.0
: 1.0 5.0
:
: named tuple:
: Point(x=1.0, y=5.0)
: 1.0 5.0
: 1.0 5.0

If one needs to add new fields, one can convert the tuple to a dictionary

#+begin_src python :results output :exports both
from collections import namedtuple # python2.6

Point = namedtuple('Point', 'x y') # named tuple
p = Point(x=1.0, y=5.0)
p = p._asdict()
print(p)
#+end_src

#+RESULTS:
: {'x': 1.0, 'y': 5.0}


* record types

Just like named tuples but mutable

#+begin_src python :results output :exports both
from rcdtype import *

Point = recordtype('Point', 'x y') # named tuple
p = Point(x=1.0, y=5.0)
print(p) # more readable output
print(p.x, p.y) # object-like dereferencing
print(p[0], p[1])
#+end_src

#+RESULTS:

#+begin_export latex
  \clearpage
#+end_export


* References
#+print_bibliography:
