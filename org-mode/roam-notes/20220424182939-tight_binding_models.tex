% Created 2023-02-25 Sat 20:27
% Intended LaTeX compiler: pdflatex
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{mathtools}
\usepackage{cancel}
\usepackage{stackrel}
\usepackage{dsfont}
\usepackage{bbold}
\usepackage{mathtools}
\usepackage{tikz}
\usepackage{amsmath}
\usetikzlibrary{shapes.geometric, arrows, positioning, automata, fit}
\tikzstyle{stdnode} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=grey!78]
\tikzstyle{arrow} = [thick, ->, >=stealth]%, red]
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\renewcommand{\mat}[1]{\boldsymbol{#1}}
\renewcommand{\d}{\mathrm{d}}
\renewcommand{\Tr}{\mathrm{Tr}\;}
\newcommand{\bra}[1]{\langle #1 \rvert}
\newcommand{\ket}[1]{\lvert #1 \rangle}
\newcommand{\braket}[2]{\langle #1 \rvert #2 \rangle}
\newcommand{\expect}[1]{\left\langle #1 \right\rangle}
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\e}{\mathrm{e}}
\renewcommand{\i}{\mathrm{i}}
\newcommand{\ln}{\mathrm{ln}}
\newcommand{\T}{\mathrm{T}}
\renewcommand{\Re}{\mathrm{Re}}
\renewcommand{\Im}{\mathrm{Im}}
\renewcommand{\P}{\mathbb{P}}
\newcommand{\up}{\uparrow}
\newcommand{\dn}{\downarrow}
\newcommand{\implies}{\Rightarrow}
\author{Andrej Lehmann}
\date{\today}
\title{Tight Binding Models}
\hypersetup{
 pdfauthor={Andrej Lehmann},
 pdftitle={Tight Binding Models},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.6)}, 
 pdflang={English}}
\makeatletter
\newcommand{\citeprocitem}[2]{\hyper@linkstart{cite}{citeproc_bib_item_#1}#2\hyper@linkend}
\makeatother

\usepackage[notquote]{hanging}
\begin{document}

\maketitle

\section{Graphene \citeprocitem{1}{[1]}}
\label{sec:org9a694cc}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./images/graphene_lattice_and_Brillouin_zone.png}
\caption{\label{fig:graphene-lattice-and-BZ}Left: lattice structure of graphene with lattice vectors \(\vec{a}_1\), \(\vec{a}_2\) and nearest neighbours vectors \(\vec{\delta}_i\), \(i=1,2,3\). Right: Brillouin zone and high symmetry points.\citeprocitem{2}{[2]}}
\end{figure}

\begin{align*}
    H = -t \sum_{\left\langle ij \right\rangle} (\hat{a}^{\dagger}_i \hat{b}_j + \hat{b}^{\dagger}_j \hat{a}_i)
\end{align*}
\(i\) (\(j\)) denotes the site of the sublattice \(A\) (\(B\)) with position \(\vec{r}_i\) (\(\vec{r}_j\)).
We also write the hamiltonian in therms of the nearest neighbours vectors
\begin{align*}
    H = -t \sum_{i \in A} \sum_{\vec{\delta}} ( \hat{a}^{\dagger}_i \hat{b}_{i+\delta} + \hat{a}_i \hat{b}^{\dagger}_{i+\delta}) \; .
\end{align*}
If we shift the origin of the coordinates by \(\vec{s}\) to be between the atoms the hamiltonian is modified to
\begin{align*}
    H = -t \sum_{i \in A} \sum_{\vec{\delta}} ( \hat{a}^{\dagger}_{i - \vec{s}} \hat{b}_{i+\vec{\delta} - \vec{s}} + {\rm h.c.}) \; .
\end{align*}
We can simplify with
\begin{align*}
    \hat{a}^{\dagger}_{i - \vec{s}} &= \frac{1}{N/2} \sum_{\vec{k}} \e^{i \vec{k} \vec{r}_i} \e^{-i \vec{k} \vec{s}} a^{\dagger}_{\vec{k}} \\
    \hat{b}_{i + \vec{\delta} - \vec{s}} &= \frac{1}{N/2} \sum_{\vec{k}'} \e^{-i \vec{k}' \vec{r}_i} \e^{-i \vec{k}' \vec{\delta}} \e^{i \vec{k}' \vec{s}} b_{\vec{k}'}
\end{align*}
and
\begin{align*}
    \frac{1}{N/2} \sum_{i \in A} \e^{i(\vec{k}-\vec{k}')\vec{r}_i} = \delta_{\vec{k} \vec{k}'}
\end{align*}
to
\begin{align*}
    H &= -t \sum_{\vec{\delta}} \sum_{\vec{k} \vec{k}'} \e^{i(\vec{k}' - \vec{k}) \vec{s}} \e^{-i \vec{k}' \vec{\delta}} \delta_{\vec{k} \vec{k}'} \hat{a}^{\dagger}_{\vec{k}} b_{\vec{k}'} + {\rm h.c.} \\
      &= -t \sum_{\vec{\delta}} \left( \e^{-i \vec{k} \vec{\vec{\delta}}} \hat{a}^{\dagger}_{\vec{k}} b_{\vec{k}} + \e^{i \vec{k} \vec{\vec{\delta}}} \hat{a}_{\vec{k}} b^{\dagger}_{\vec{k}} \right) \; .
\end{align*}
So the shift is canceled due to \(\delta_{\vec{k} \vec{k}'}\).

\subsection{Alternative}
\label{sec:orgaa21161}

\begin{align*}
    \varepsilon(\vec{k}) &= \frac{1}{L} \sum_{ij} t_{ij} \; \e^{-\i\vec{k}\left( \vec{R}_i - \vec{R}_j \right)} \\
                         &= \frac{1}{L} \sum_{ij} t_{n+j,j} \; \e^{-\i\vec{k} \vec{R}_n} \qquad \vec{R}_n = \vec{R}_i - \vec{R}_j\\
                         &= \quad \sum_{ij} t_{n,0} \; \e^{-\i\vec{k} \vec{R}_n} \qquad \text{tranlation invariance}\\
\end{align*}

\clearpage
\section{Dispersion and DOS for all common 2D lattices and additionally Fermi surface for 3D lattices}
\label{sec:orgf514dd6}

\url{http://lampx.tugraz.at/\~hadley/ss1/bands/tbtable/tbtable.html}

\section{References}
\label{sec:org15fb74e}
\hypertarget{citeproc_bib_item_1}{[1] F. Utermohlen, “Tight-binding model for graphene.” https://cpb-us-w2.wpmucdn.com/u.osu.edu/dist/3/67057/files/2018/09/graphene\_tight-binding\_model-1ny95f1.pdf, 2018.}

\hypertarget{citeproc_bib_item_2}{[2] A. H. Castro Neto, F. Guinea, N. M. R. Peres, K. S. Novoselov, and A. K. Geim, “The electronic properties of graphene,” \textit{Rev. mod. phys.}, vol. 81, pp. 109–162, Jan. 2009, doi: \href{https://doi.org/10.1103/RevModPhys.81.109}{10.1103/RevModPhys.81.109}.}
\end{document}
