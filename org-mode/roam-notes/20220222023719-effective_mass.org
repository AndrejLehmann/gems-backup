:PROPERTIES:
:ID:       4a8bfdb2-7f6e-4437-9b93-0c89e82d91d7
:END:
#+title: Effective mass
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

Defined by
\begin{align*}
    m_{\rm eff}^{-1}(k) = \frac{1}{\hbar} \frac{\partial^2 \varepsilon_{k}}{\partial k^2 }
\end{align*}

In the case of free electrons, that have parabolic dispersion, effective mass is a constant.
Band theoretical calculations, that well approximate simple metals, yield (frequency independent) effective mass $m_b$ deviating from $m_{\rm eff}$.

The effective mass also relates to [[id:a1395879-938a-4303-bacb-ba30bf1325a9][quasiparticle weight]] $Z$ by
\begin{align*}
    \frac{m_{\rm eff}}{m} = \frac{1}{Z}
\end{align*}

So when $Z$ approaches 0 (e.g. in the case of Mott transition) $m_{\rm eff}$ diverges as a consequence of the [[id:7422d7bb-af4a-4996-bc86-bbc7c228735d][Landau's Fermi Liquid Theory]].
