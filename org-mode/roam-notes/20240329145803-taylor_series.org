:PROPERTIES:
:ID:       cc830d63-6d9a-4ad4-8725-1a0d8d3497e0
:END:
#+title: Taylor series
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Derivation from fundamental theorem of calculus

Starting with the fundamental theorem of calculus
\begin{align*}
    \int_a^b f(x) \d x = F(a) - F(b) \quad \text{where} \; F'(x) = f(x)
\end{align*}
Rewrite as
\begin{align*}
    F(b) = F(a) + \int_a^b F'(x) \,\d x
\end{align*}
rename $b$ as $t$
\begin{align*}
\label{eq:2}
    F(t) = F(a) + \int_a^t F'(x) \,\d x
\end{align*}
Apply fundamental theorem of calculus to $F'(x)$
\begin{align*}
    F'(x) = F'(a) + \int_a^x F''(t_1) \,\d t_1
\end{align*}
Integrate on both sides
\begin{align*}
    \int_a^t F'(x) \d x = \int_a^t F'(a) \d x + \int_a^t \int_a^x F''(t_1) \,\d t_1 \d x
\end{align*}
Solve the first integral on the right hand side
\begin{align*}
    \int_a^t F'(x) \d x = F'(a) (t-a) + \int_a^t \int_a^x F''(t_1) \,\d t_1 \d x
\end{align*}
Insert into the fundamental theorem of calculus
\begin{align*}
    F(t) = F(a) + F'(a)(t-a) + \int_a^t \int_a^x F''(t_1)\,\d t_1 \d x
\end{align*}
Repeat for $F''(x)$
\begin{align*}
    F''(t_1) &= F''(a) + \int_a^t_1 F^{(3)}(t_2) \,\d t_2 \\
    \int_a^t \int_a^x F''(t_1) \,\d t_1 \d x &= \int_a^t \int_a^x  F''(a) \,\d t_1 \d x + \int_a^t \int_a^x \int_a^t_1 F^{(3)}(t_2) \,\d t_2\d t_1 \d x \\
    \int_a^t \int_a^x F''(t_1) \,\d t_1 \d x &= F''(a) \frac{1}{2} (t-a)^2 + \int_a^t \int_a^x \int_a^t_1 F^{(3)}(t_2) \,\d t_2\d t_1 \d x \\
    F(t) &= F(a) + F'(a)(t-a) + F''(a) \frac{1}{2} (t-a)^2 + \int_a^t \int_a^x \int_a^t_1 F^{(3)}(t_2) \,\d t_2\d t_1 \d x
\end{align*}
Iterate
\begin{align*}
    F(t) = F(a) + \sum_{n=1}^k \frac{1}{n!}F^{(n)}(a)(t-a)^n + \underbrace{\int_a^t \int_a^x \dots \int_a^{t_n} F^{(n+1)} \,\d t_n \d t_{n-1} \dots \d x \d t}_{R_{n+1}}
\end{align*}

* The Remainder and Error Estimation of the Cut Off

/Recall that the $n$ -th Taylor polynomial for a function $f$ at $a$ is the $n$ -th partial sum of the Taylor series for $f$ at $a$./
/Therefore, to determine if the Taylor series converges, we need to determine whether the sequence of Taylor polynomials ${p_n}$ converges./
/However, not only do we want to know if the sequence of Taylor polynomials converges, we want to know if it converges to $f$./
/To answer this question, we define the remainder $R_n(x)$ as/
\begin{align*}
    R_n(x)=f(x)-p_n(x) \; .
\end{align*}
/For the sequence of Taylor polynomials to converge to $f$, we need the remainder $R_n$ to converge to zero./
/To determine if $R_n$ converges to zero, we introduce Taylor’s theorem with remainder./
/Not only is this theorem useful in proving that a Taylor series converges to its related function, but it will also allow us to quantify how well the $n$ -th Taylor polynomial approximates the function./
/Here we look for a bound on $|R_n|$./
/Consider the simplest case: $n=0$./
/Let $p_0$ be the $0$ -th Taylor polynomial at $a$ for a function $f$./
/The remainder $R_0$ satisfies/
\begin{align*}
    R_0(x)=f(x)-p_0(x)=f(x)-f(a) \;.
\end{align*}
/If $f$ is differentiable on an interval $I$ containing $a$ and $x$, then by the Mean Value Theorem there exists a real number $c$ between $a$ and $x$ such that $f(x)-f(a)=f'(c)(x-a)$./
/Therefore,/
\begin{align*}
    R_0(x)=f'(c)(x−a) \;.
\end{align*}
/Using the Mean Value Theorem in a similar argument, we can show that if $f$ is $n$ times differentiable on an interval $I$ containing $a$ and $x$, then the $n$ -th remainder $R_n$ satisfies/
\begin{align*}
    R_n(x)= \frac{f^{(n+1)}(c)}{(n+1)!}(x-a)^{n+1}
\end{align*}
/for some real number $c$ between $a$ and $x$./
/It is important to note that the value $c$ in the numerator above is not the center $a$, but rather an unknown value $c$ between $a$ and $x$./
/This formula allows us to get a bound on the remainder $R_n$./
/If we happen to know that $|f^{(n+1)}(x)|$ is bounded by some real number $M$ on this interval $I$, then/
\begin{align*}
    |R_n(x)| \le \frac{M}{(n+1)!}\,|x-a|^{n+1}
\end{align*}
/for all $x$ in the interval $I$./
/We now state Taylor’s theorem, which provides the formal relationship between a function $f$ and its $n$ -th degree Taylor polynomial $p_n(x)$./
/This theorem allows us to bound the error when using a Taylor polynomial to approximate a function value, and will be important in proving that a Taylor series for $f$ converges to $f$./

* The Subtle Reason Taylor Series Work | Smooth vs. Analytic Functions [cite:@the-subtle-reason-taylor-series-work-YT]

Why is it possible to improve approximation to a function by adding more and more terms of the Taylor series and even converge to the original function in the limit?
Since we approximate at some specified point, we use only local information about the function.
So it is not obvious how this can extend from something local to something global and reproduce the whole function in the limit.
In order to understand why consider the deviation of the Taylor expansion $T_n(x)$ to the function $f(x)$ it approximates
\begin{align*}
    f(x) - T_n(x) = \frac{f^{(n+1)}(t)}{(n+1)!} (x-x_0)^{n+1}\;,\quad \text{for $t$ between $x_0$ and $x$}
\end{align*}
So the deviation goes to $0$ if $f^{(n+1)}(t)$ stays bounded for any $n$ (since factorial grows faster then exponentiation) or generally if the denominator grows faster than the numerator.

* Convergence [cite:@Taylor-series-wiki]

/In general, Taylor series does not need to be convergent at all./
/And in fact the set of functions with a convergent Taylor series is a meager set in the Fréchet space of smooth functions./
/And even if the Taylor series of a function $f$ does converge, its limit need not in general be equal to the value of the function $f(x)$./
/For example, the function/
\begin{align*}
    f(x) =
        \begin{cases}
          e^{-1/x^2} & \text{if } x \neq 0 \\
          0     & \text{if } x = 0
        \end{cases}
\end{align*}
/is infinitely differentiable at $x=0$, and has all derivatives zero there./
/Consequently, the Taylor series of $f(x)$ about $x=0$ is identically zero./
/However, $f(x)$ is not the zero function, so does not equal its Taylor series around the origin./
/Thus, $f(x)$ is an example of a non-analytic smooth function./

/In real analysis, this example shows that there are infinitely differentiable functions $f(x)$ whose Taylor series are *not* $f(x)$ even if they converge./
/By contrast, the holomorphic functions studied in complex analysis always possess a convergent Taylor series,/
/and even the Taylor series of meromorphic functions, which might have singularities, never converge to a value different from the function itself./
/The complex function $1/e^{(z^{2})}$ however, does not approach 0 when $z$ approaches 0 along the imaginary axis, so it is not continuous in the complex plane and its Taylor series is undefined at 0./

/More generally, every sequence of real or complex numbers can appear as coefficients in the Taylor series of an infinitely differentiable function defined on the real line, a consequence of Borel's lemma./
/As a result, the radius of convergence of a Taylor series can be zero./
/There are even infinitely differentiable functions defined on the real line whose Taylor series have a radius of convergence 0 everywhere./

/A function cannot be written as a Taylor series centred at a singularity;/
/in these cases, one can often still achieve a series expansion if one allows also negative powers of the variable $x$; see Laurent series./
/For example, $f(x) = 1/e^{(x^{2})}$ can be written as a Laurent series./

* The $n!$ in the Coefficients [cite:@Taylor-series-physics-with-Elliot-YT]

Since the Taylor series expresses a function via a polynomial calculating a derivative of an additional order brings down an additional exponent of that order so that
\begin{align*}
    &f^{(n)}(0) = n \cdot (n-1) \cdot \dots \cdot 2 \cdot 1 \cdot C_{n} \\
    &\frac{f^{(n)}(0)}{n!} = C_{n}
\end{align*}

* Alternative Way of Writing the Taylor Series

\begin{align*}
    &f(x) = \sum_{n=0}^\infty \frac{1}{n!}f^{(n)}(x_{0})(\underbrace{x-x_{0}}_{\varepsilon})^n \\
    &f(x_{0}+\varepsilon) = \sum_{n=0}^\infty \frac{1}{n!} f^{(n)}(x_{0}) \varepsilon^n \\
    &f(x_{0}+\varepsilon) = \sum_{n=0}^\infty \frac{1}{n!}\left( \varepsilon\,\frac{\d}{\d x}\right)^n f(x) \\
    &f(x_{0}+\varepsilon) = \e^{\,\varepsilon \frac{\d}{\d x}} f(x) \\
    &f(\vec{r}+\vec{\varepsilon}) = \e^{\,\vec{\varepsilon} \cdot \nabla} f(\vec{r}) \\
\end{align*}

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
