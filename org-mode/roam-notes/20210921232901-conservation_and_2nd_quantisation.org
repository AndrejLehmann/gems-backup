:properties:
:ID:       802f96c1-4a73-4eae-8ab1-8ca0a1d4df5d
:end:
#+title: Conservation and 2nd quantisation
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

In second quantisation a physical quantity is usually conserved if the number of creation operators is equal to the number of anihilation operators.
An exception is for example creation of 2 particles with quantum number $x$ and anihilation of one particle with quantum number $2x$.
For spin the quantum number for $\ket{\uparrow}$ is 1 and for $\ket{\downarrow}$ -1.
