:PROPERTIES:
:ID:       547e2b58-c84a-4cfa-9e73-0d8e8433673c
:END:
#+title: Non many body but beyond band theory gap openings
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Non many body examples (no correlations) when the band theory predicts a metal but more sophisticated theories an insulator:
- Magnetic ordering
- [[id:b0bc8c1d-813e-4336-978c-71e7fee0957a][Crystal field splitting]]
- [[id:ed303134-e912-4a52-a64e-675dd1f2b73d][Jahn-Teller distortion]]

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
