:PROPERTIES:
:ID:       d69825ee-1f3d-455b-8bd1-49ce763f83cf
:END:
#+title: My mac
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :mac:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* [[id:6d7923c5-add8-49fc-8859-77851ab946d8][Emacs doesn't find the meta key]]
* [[id:7402767a-0192-41f2-9dd3-a6528c07a113][GDB]]
** code sign
When running src_sh{brew update --preinstall} homebrew showed the message

#+begin_src sh
==> gdb
gdb requires special privileges to access Mach ports.
You will need to codesign the binary. For instructions, see:
  https://sourceware.org/gdb/wiki/PermissionsDarwin
#+end_src

There they say

#+begin_quote
If you try to use your freshly built gdb, you might get an error message such as:

#+begin_src sh
Starting program: /x/y/foo
Unable to find Mach task port for process-id 28885: (os/kern) failure (0x5).
 (please check gdb is codesigned - see taskgated(8))
#+end_src
#+end_quote

So I followed the instuctions.

* Remote editing
** [[id:f0a3ac8c-a19f-47f6-9b63-be4d3899b798][Emacs: editing remote files]]
** [[id:e28ccfd2-1319-42d2-8f27-aeca5e5ab97e][Mac: sshfs for remote editing]]

#+begin_export latex
    \clearpage
#+end_export
* References
#+print_bibliography:
