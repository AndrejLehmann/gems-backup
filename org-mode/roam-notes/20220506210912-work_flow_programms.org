:PROPERTIES:
:ID:       5fc63b17-2957-4aa5-9dd6-7c385112d4ee
:END:
#+title: Work flow programms
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :emacs:vim:tmux:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* [[http://erick.matsen.org/2020/01/04/2nd-gen-interactive-shell.html][Second-Generation Interactive Shell Tools]]

* VIM

** [[https://francopasut-en.blogspot.com/2019/08/vim-cut-copy-and-paste-to-and-from.html][Copy/Paste to System Clipboard]]

** [[https://puremourning.github.io/vimspector-web/][Debugging With VIMspector]]

* TMUX

[[https://github.com/tmux-plugins/tmux-resurrect][tmux resurrect]]
[[https://raw.githubusercontent.com/tmux-plugins/tmux-resurrect/master/docs/restoring_programs.md][tmux resurrect]]
[[https://tmuxcheatsheet.com/tmux-plugins-tools/?full_name=tmux-plugins%2Ftmux-resurrect][tmux resurrect]]
[[https://github.com/tmux-plugins/tmux-continuum][tmux continuum]]
[[https://www.youtube.com/watch?v=DzNmUNvnB04][config]]

* EMACS
** General
*** [[https://www.youtube.com/watch?v=gfZDwYeBlO4][Play EMACS Like an Instrument]]
*** [[https://www.youtube.com/watch?v=a4uE36Lb2_I][Mein ganzes (wissenschaftliches) Leben in Reintextform]]
*** [[https://github.com/rougier/nano-emacs][nano emacs]]
** [[id:0a545615-867e-452d-b695-ff22c8eb5f23][org-mode]]
** Customize [cite: @emacs-customize-tutorial; @emacs-easy-customization-interface]

'M-x customize' to open interface.
Then press RET on buttons e.g. on [ State ] to save changes.
*Not* recommended *for doom emacs*.

** gruvbox theme

Alternative kann man in .emacs.d/.local/straight/repos/emacs-doom-themes/themes/doom-gruvbox-theme.el Anpassungen machen.
Z.B. defcustom doom-gruvbox-dark-variant nil -> defcustom doom-gruvbox-dark-variant "hard"

** TRAMP

https://www.emacswiki.org/emacs/TrampMode
https://www.reddit.com/r/emacs/comments/6qdsub/tramp_sudo_and_ssh_config/
https://github.com/hlissner/doom-emacs/issues/1703

** config

https://dotdoom.rgoswami.me/config.html
https://emacs.stackexchange.com/questions/251/line-height-with-unicode-characters
https://github.com/tam5/font-patcher

** LaTeX

https://rgoswami.me/posts/org-note-workflow/
https://tex.stackexchange.com/questions/77829/how-to-make-a-new-tex-command-in-org-mode-latex
https://www.youtube.com/watch?v=LFO2UbzbZhA&t=627s
https://emacs.stackexchange.com/questions/3375/loading-bibtex-file-in-org-mode-file
http://cachestocaches.com/2020/3/org-mode-annotated-bibliography/
https://emacs.stackexchange.com/questions/14923/label-and-reference-latex-equations-in-org-mode
https://stackoverflow.com/questions/16346622/how-can-i-reference-a-section-by-number-in-org-mode-export

** browsing
*** [[https://emacsnotes.wordpress.com/2018/08/18/why-a-minimal-browser-when-there-is-a-full-featured-one-introducingxwidget-webkit-a-state-of-the-art-browser-for-your-modern-emacs/][blog - browsing with XWidget Webkit]]
*** [[https://github.com/akirakyle/emacs-webkit][emacs-webkit]]
*** [[https://www.youtube.com/watch?v=BrTpTNEXMyY&t=651][Hyperbole]]

Power full buttons and links and more.
[[https://blog.abrochard.com/hyperbole-intro.html][Some features as gifs.]]
E.g. when you link to a youtube video, you can specify time of start and finish.
When press that button the link will be opened in the browser and the video will be played from that specified time and stopped at the specified time.

** ledger
:properties:
:id:       046f654d-be6b-4217-b3d1-3a2617fa01cc
:end:
[[https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-ledger.html][ledger source code in orgmode]]
[[https://www.ledger-cli.org/3.0/doc/ledger-mode.pdf][ledger-mode documentation]]

** hledger
[[https://github.com/narendraj9/hledger-mode][hledger mode]]
** polymode

A framework for multiple major modes inside a single Emacs buffer.
Might be useful for similar user experience as in =org-mode= when editing a source block within a markdown file, if export from org file to markdown file does not work for example.

[[https://polymode.github.io][link]]

** better org-latex-previews

[[https://abode.karthinks.com/org-latex-preview/]]

Unfortunately I could not install it without issues.
But it might be merged into Org, so the installation should be unnecessary then.

* [[id:5fc76d58-0dd2-47d3-b265-f8eb8535c098][git]]
