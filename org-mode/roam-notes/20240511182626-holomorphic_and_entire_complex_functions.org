:PROPERTIES:
:ID:       620be2e1-40e6-4708-932d-3831b098cd55
:END:
#+title: Holomorphic and Entire Complex Functions
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

For $U \subseteq \mathbb{C}$ and $z_0 \in U$, $f: U \to \mathbb{C}$  is called *holomorphic* if $f$ is [[id:35dacc9f-76b4-4990-a746-18a452453527][complex differentiable]] at every $z_0 \in U$.
If $U = \mathbb{C}$ the holomorphic function is called *entire*.
- $f$ is holomorphic $\Rightarrow$ $f$ is coutinious
- $f$, $g$ is holomorphic $\Rightarrow$ $f+g$, $f \cdot g$ are holomorphic
- sum rule, chain rule, product rule hold

#+begin_export latex
  \clearpage
#+end_export

* References
#+print_bibliography:
