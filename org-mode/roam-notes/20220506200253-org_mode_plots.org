:properties:
:ID:       e7fb724f-ee87-4337-9bac-e8600ae0c6a9
:end:
#+title: org-mode: plots
#+startup: latexpreview
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :emacs:orgmode:

#+begin_src python :results file
import matplotlib, numpy
matplotlib.use('Agg')  # anti-grain geometry backend
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(4,2))
x   = numpy.linspace(-15,15)
plt.plot(numpy.sin(x)/x)
fig.tight_layout()
plt.savefig('images/testFig.png')

return 'images/testFig.png' # return filename to org-mode
#+end_src

#+RESULTS:
[[file:images/testFig.png]]

But this does /not/ work:

#+begin_src python :session test1 :results output
import matplotlib, numpy
matplotlib.use('Agg')  # anti-grain geometry backend
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(4,2))
x   = numpy.linspace(-15,15)
#+end_src

#+RESULTS:
[[file:]]

#+begin_src python :session test1 :results file
plt.plot(numpy.sin(x)/x)
fig.tight_layout()
plt.savefig('images/testFig.png')

return 'images/testFig.png' # return filename to org-mode
#+end_src

#+RESULTS:
[[file:]]

But this does:

#+begin_src python :session test2 :results output
import matplotlib, numpy
matplotlib.use('Agg')  # anti-grain geometry backend
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(4,2))
x   = numpy.linspace(-15,15)
plt.plot(numpy.sin(x)/x)
fig.tight_layout()
plt.savefig('images/testFig.png')
#+end_src

#+RESULTS:

#+begin_src python :results file
return 'images/testFig.png' # return filename to org-mode
#+end_src

#+RESULTS:
[[file:images/testFig.png]]

For some reason :session and return file does not work here together ¯\_(ツ)_/¯ .
