:properties:
:ID:       29532b7d-0357-40be-9e0b-5248944e6881
:end:
#+title: Ising model
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:magnetism:computing-in-science:
#+begin_export latex
    \clearpage \tableofcontent \clearpage
#+end_export

* Classical

\begin{align*}
H(\sigma) = -\sum_{\langle i,j\rangle} J_{ij} \sigma_i \sigma_j - \mu \sum_j h_j \sigma_j
\end{align*}

For each lattice site $i$ the discrete variable $\sigma_i \in \{+1,-1\}$ represents the site's spin.
The first sum is the interaction between sites.
The second sum is the interaction with the external magnetic field $h_j$ where $\mu$ is the magnetic moment.
If $J_{ij}$ are different (e.g. random) we obtain something that is called /spin glass/.
Also compare with the [[id:31f7ea53-f25d-462c-974c-ad5508465a16][Heisenberg model]]

* Quantum (transverse field)

\begin{align*}
H = J \left( \sum_{ \langle i, j \rangle} \sigma^{x}_i \sigma^{x}_{j} + g \sum_j \sigma^{z}_j \right)
\end{align*}
$\sigma_j^a = \mat{I}^{\otimes j-1}\otimes \sigma^a \otimes \mat{I}^{\otimes N-j}$, where $\mat{I}$ is a $2\times 2$ unity matrix.

Representing lattice with nearest neighbor interactions $J$ determined by the alignment or anti-alignment of spin projections along the $x$ axis, as well as an external magnetic field perpendicular to the $z$ -axis which creates an energetic bias for one $z$ -axis spin direction over the other.
An important feature of this setup is that, in a quantum sense, the spin projection along the $z$ axis and the spin projection along the $x$ axis are not commuting observable quantities. That is, they can not both be observed simultaneously.
This means classical statistical mechanics cannot describe this model, and a quantum treatment is needed.

** 3 sites model with python

$2^3=8$ so $8\!\times\!8$ -matrices are needed.

\begin{align*}
H = \sigma^x_1 \otimes \sigma^x_2 + \sigma^x_2 \otimes \sigma^x_3 + \sigma^x_1 \otimes \sigma^x_3 + h (\sigma^z_1 + \sigma^z_2 + \sigma^z_3)
\end{align*}

Hilbert space of $H$ is $\{\ket{\up\up\up} \,,\, \ket{\up\up\dn} \,,\, \ket{\up\dn\up} \,,\, \ket{\up\dn\dn} \,,\, \ket{\dn\up\up} \,,\, \ket{\dn\up\dn} \,,\, \ket{\dn\dn\up} \,,\, \ket{\dn\dn\dn}\}$ where the position of spins corresponds to the site, e.g. $\ket{\up\dn\up}$ means sites 1 and 3 have spin $\up$ and site 2 has spin $\dn$ .

( [[id:38ed71f2-3515-41fd-b076-66b44f0dc19f][Python basics for scientific computations]] )
#+begin_src python :session :results output
import numpy as np

I2       = np.eye(2)
sx  = np.array( [[ 0.,  1. ],
                 [ 1.,  0. ]] )
sz  = np.array( [[ 1.,  0. ],
                 [ 0., -1. ]] )
Hxx = np.kron( np.kron( sx, sx ), I2 ) + np.kron( np.kron( I2, sx ), sx ) + np.kron( np.kron( sx, I2 ), sx )
Hz  = np.kron( np.kron(sz, I2), I2 ) + np.kron( np.kron(I2, sz), I2 ) + np.kron( np.kron(I2, I2), sz )
H   = Hxx + Hz


print("sx (X) sx = ", np.kron( sx, sx ), sep='\n')
print("\nsx (X) sx (X) I2 = ", np.kron( np.kron( sx, sx ), I2 ), sep='\n')
print("\nHxx =", Hxx, sep='\n')
print("\nHz =", Hz, sep='\n')
print("\nH =", H, sep='\n')

eigVals,eigVecs = np.linalg.eig(H)
print("\neigVals = ", EigVals)
#+end_src

#+RESULTS:
#+begin_example
sx (X) sx =
[[0. 0. 0. 1.]
 [0. 0. 1. 0.]
 [0. 1. 0. 0.]
 [1. 0. 0. 0.]]

sx (X) sx (X) I2 =
[[0. 0. 0. 0. 0. 0. 1. 0.]
 [0. 0. 0. 0. 0. 0. 0. 1.]
 [0. 0. 0. 0. 1. 0. 0. 0.]
 [0. 0. 0. 0. 0. 1. 0. 0.]
 [0. 0. 1. 0. 0. 0. 0. 0.]
 [0. 0. 0. 1. 0. 0. 0. 0.]
 [1. 0. 0. 0. 0. 0. 0. 0.]
 [0. 1. 0. 0. 0. 0. 0. 0.]]

Hxx =
[[0. 0. 0. 1. 0. 1. 1. 0.]
 [0. 0. 1. 0. 1. 0. 0. 1.]
 [0. 1. 0. 0. 1. 0. 0. 1.]
 [1. 0. 0. 0. 0. 1. 1. 0.]
 [0. 1. 1. 0. 0. 0. 0. 1.]
 [1. 0. 0. 1. 0. 0. 1. 0.]
 [1. 0. 0. 1. 0. 1. 0. 0.]
 [0. 1. 1. 0. 1. 0. 0. 0.]]

Hz =
[[ 3.  0.  0.  0.  0.  0.  0.  0.]
 [ 0.  1.  0.  0.  0.  0.  0.  0.]
 [ 0.  0.  1.  0.  0.  0.  0.  0.]
 [ 0.  0.  0. -1.  0.  0.  0.  0.]
 [ 0.  0.  0.  0.  1.  0.  0.  0.]
 [ 0.  0.  0.  0.  0. -1.  0.  0.]
 [ 0.  0.  0.  0.  0.  0. -1.  0.]
 [ 0.  0.  0.  0.  0.  0.  0. -3.]]

H =
[[ 3.  0.  0.  1.  0.  1.  1.  0.]
 [ 0.  1.  1.  0.  1.  0.  0.  1.]
 [ 0.  1.  1.  0.  1.  0.  0.  1.]
 [ 1.  0.  0. -1.  0.  1.  1.  0.]
 [ 0.  1.  1.  0.  1.  0.  0.  1.]
 [ 1.  0.  0.  1.  0. -1.  1.  0.]
 [ 1.  0.  0.  1.  0.  1. -1.  0.]
 [ 0.  1.  1.  0.  1.  0.  0. -3.]]
#+end_example

The smallest value of D is the ground state.

Now lets create an array of $h$ values and plot the ground state energy for the Ising model for this values.

[[id:e7fb724f-ee87-4337-9bac-e8600ae0c6a9][Plot in orgmode]]
#+begin_src python :session :results output
size = 20
h    = np.linspace( 0, 2, size )
e0  = np.zeros(size) # ground state energies
for i in range(size):
    H   = Hxx + h[i]*Hz
    eigVals_h,eigVec = np.linalg.eig(H)
    e0[i] = min(eigVals_h).real

import matplotlib
matplotlib.use('Agg')  # anti-grain geometry backend
import matplotlib.pyplot as plt

fig = plt.figure( figsize=(4,2) )
plt.title( "Ground state energy of quantum Ising model with 3 sites", fontsize=8 )
plt.xlabel( "magnetic field h" )
plt.ylabel( "Energy" )
fig.tight_layout()
plt.plot( h, e0, marker='o')
plt.savefig( "images/Ising-model-3sites.png" )
#+end_src

#+RESULTS:

#+begin_src python :results file
return "images/Ising-model-3sites.png"
#+end_src

#+RESULTS:
[[file:images/Ising-model-3sites.png]]

* Comparison with the [[id:31f7ea53-f25d-462c-974c-ad5508465a16][Heisenberg model]] [cite: @Ising-model-vs-Heisenberg-model]

/Although the Ising and Heisenberg models are both simplified spin models of magnetic phase transitions, the models differ in their symmetry properties, and symmetry properties are crucial for determining certain "universal" characteristics of phase transitions./
/The "advantage" of the Heisenberg model is that it is useful for studying universal properties of phase transitions in spin systems with a certain symmetry: specifically, those systems where the energy of a configuration of spins is invariant to rotating every spin in the same way around the unit sphere./
/On the other hand, the Ising model is appropriate for spin systems where the energy is invariant to reflecting every spin to its opposite orientation./

#+begin_export latex
  \clearpage
#+end_export
* References
#+print_bibliography:
