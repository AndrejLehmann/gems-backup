:properties:
:ID:       9af7dd60-172c-4c02-ae7c-eeb62c7fef3e
:end:
#+title: Hermitian matrix
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :math:

A complex and square matrix $A$ is Hermitian if $A = A^{*}^{\T}$.
The eigenvalues are then real.
