:PROPERTIES:
:ID:       3f24156d-2e9e-46de-aa11-28cb5a27eef1
:END:
#+title: Time-reversal symmetry
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

* Hamiltonians, topology, and symmetry [cite: @hamiltonians-topology-and-symmetry; @hamiltonians-topology-and-symmetry-github]

Time-reversal symmetry can be expressed by an anti-unitary operator $\mathcal{T} = U \mathcal{K}$ where $U$ is a unitary matrix and $\mathcal{K}$ is complex conjugation.
If the Hamiltonian is real $\mathcal{T} = \mathcal{K}$ and $H = H^{*}$.
For spin-1/2 systems $\mathcal{T} = i \sigma^{y} \mathcal{K}$  and $\mathcal{T}^2 = 1$.
Then it holds $H = \sigma^y H^{* } \sigma^{y}$ and every energy level is doubly degenerate, Kramers’ degeneracy.

#+begin_export latex
    \clearpage
#+end_export
* References
#+print_bibliography:
