:PROPERTIES:
:ID:       a17a90a0-11d5-4429-9a9f-fc5d26214124
:END:
#+title: Green's function approach for one-particle solid state problem (T-matrix and S-matrix)
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

* Green's functions approach to solve one-particle solid state problem [cite:@electronic-structure-and-DFT]

Given
\begin{align*}
    H = H_0 + V \, , \;\; V = \sum_i V_i \quad \text{with site $i$}
\end{align*}
The Green's function is
\begin{align*}
    G = G_0 + G_0 V G = G_0 + G_0 V G_0 + G_0 V G_0 V G_0 + ...
\end{align*}
By introducing the T-matrix which describes the scatering processes
\begin{align*}
    T = V + V G_0 V + V G_0 V + ... = V + V G_0 T
\end{align*}
we can write
\begin{align*}
    G = G_0 + G_0 T G_0
\end{align*}
that can be solved by
\begin{align*}
    T^{-1} = V^{-1} + G_0
\end{align*}
In order to calculate $G_0$ we separate it into local contribution which is the local Green's function $G_0^{ii}$ and non-local contribution which is the so called structural matrix $S_{ij}$ and it depends on the structure of the solid
\begin{align*}
    G_0 = G_0^{ii} \delta_{ij} + S_{ij} (1-\delta_{ij})
\end{align*}
Then we write
\begin{align*}
    & t_i^{-1} = V_i^{-1} - G_0^{ii} \\
    & T_{ij}^{-1} = t_i^{-1} \delta_{ij} - S_{ij}
\end{align*}
For tight binding $S_{ij} = \delta_{ij}$.
But the performance of the method is rather bad, due to necessity for inversion of a big matrix (in contrast to solving just a eigenvalue problem).

#+begin_export latex
  \clearpage
#+end_export
* References
#+print_bibliography:
