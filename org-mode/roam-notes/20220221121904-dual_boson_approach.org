:PROPERTIES:
:ID:       a7ee8d4e-2e5f-4830-92c4-3bf4aba14fbe
:END:
#+title: Dual Boson approach
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

- /It is a minimal conserving approximation for correlated systems, similar to the RPA being the minimal theory for the Fermi gas./ [cite:@plasmons-in-strongly-correlated-systems-Erik]

- correctly DB exhibits [[id:1693c925-21e9-405c-aa16-d03d2795acc6][gauge invariant response in the long-wavelength limit]] ($\vec{q} \to 0$) [cite:@DB-collective-excitations]

- /We have further shown that an alternative expression for the susceptibility that emerges in the dual boson approach is equivalent to the DMFT susceptibility./
  /Such a formulation has the advantage that it circumvents numerical problems due to a divergence of the irreducible vertex close to a metal-insulator transition./
  /It also resolves the ambiguity of calculating the susceptibility in EDMFT./
  /The approach is straightforwardly generalized to treat spin excitations./ [cite:@collective-charge-excitations-of-strongly-correlated-electrons]

* Dual Boson

\begin{align*}
    U n_{\uparrow} n_{\downarrow} = \frac{1}{2} \sum_{\alpha=d,m} n^{ * \alpha} U^{\alpha} n^{\alpha} \to - \sum_{\alpha=d,m} \varphi^{* \alpha} \left( U^{\alpha} \right)^{-1} + \sum_{\alpha, \{\sigma\}} \varphi^{* \alpha} \left( c^{* }_{\sigma} \sigma^{\alpha}_{\sigma'\sigma'} c_{\sigma'} \right)
\end{align*}
$c \to f;$  $n^{\alpha} \to \varphi^{\alpha};$ $\alpha=\{d,m\}$
\begin{align*}
    \tilde{S} = - \sum_{k,\sigma} f^{* }_{k} \tilde{\mathcal{G}}^{-1}_{k\sigma} f_{k\sigma} - \frac{1}{2} \sum_{q,\alpha} \varphi^{* \alpha}_{q} \left(  \tilde{\mathcal{W}}^{\sigma}_{q}  \right)^{-1} \varphi_q^{\alpha} + \mathcal{F}[f,\varphi]
\end{align*}
