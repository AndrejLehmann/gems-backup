:properties:
:ID:       7422d7bb-af4a-4996-bc86-bbc7c228735d
:end:
#+title: Landau's Fermi Liquid Theory
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:
#+begin_export latex
  \clearpage \tableofcontent \clearpage
#+end_export

Landau's Fermi Liquid Theory develops from the idea of "adiabatic continuity".
Consider an non-interacting ground state.
Now let evolve it adiabatically into an interacting ground state (conservation law stay unchanged by the interaction).
As an example we can take an infinite square well potential as an non-interacting system and a harmonic potential as an interacting system.
The wave functions and energies are different but the topological characteristics (number of nodes) are the same and thus the quantum numbers of states.
An elementary excitation of the interacting system can be approximated by an elementary excitation of the non-interacting one.
Excitations above the sharply defined Fermi surface are quasi-particles.

The consequence of Fermi Liquid Theory is that when approaching Mott insulator transition continuously the [[id:4a8bfdb2-7f6e-4437-9b93-0c89e82d91d7][single-quasiparticle mass]] $m^*$ divergies (or respectively [[id:4a8bfdb2-7f6e-4437-9b93-0c89e82d91d7][renormalization factor]] $Z$ vanishes) at the MIT point.
In the case of symmetry breaking ([[id:d7c7e167-ea46-421a-af23-a48e3a0e82a3][spontaneously]] or externally [e.g. crystal field splitting]) the assumption of adiabatic continuity is /not/ satisfied anymore. [cite: @metal-insulator-transitions]

In Fermi liquids interactions result in *small corrections* of the free electron gas susceptibility.[cite: @Mahan-many-particle-phys]

- /In contrast to [[id:302f3a03-c9ca-48de-98af-9e971456e87b][Drude model]] that does not take into account interactions with the underlying lattice, with electrons, or other quasiparticles, in Fermi-liquid theory Landau (1956) included electronic correlations, yielding an effective mass $m^{*}$ also an effective scattering time./ [cite:@electrodynamics-corr-el-materials]

* Fermi-liquid theory [cite:@advanced-solid-state-physics-Phillips]

/It is now well accepted that the normal state of a metal is well described by Landau-Fermi-liquid theory./
/In this account, it is claimed that the dominant effect of electron interaction in a metal is to renormalize the effective mass of the electron./
/The observed shift is on the order to $10$ to $50\%$./
/Another essential claim of Fermi-liquid theory is that there is a one-to-one correspondence between the excited states of the normal state of a metal with those of a noninteracting electron gas./
/The elementary excitations in Fermi-liquid theory are called quasi particles./
/A quasi particle is a composite particle with a lifetime./
/The lifetime stems from collisions with other quasi particles./
/When the lifetime $\tau$ of a quasi particle is infinite, the state with such a particle is an eigenstate of the system./
/However, the minimum constraint that must hold for a quasi-particle state to be an eigenstate of a system is  that $\hbar /\tau \ll \tilde{\varepsilon}_{\vec{p}}$, where $\tilde{\varepsilon}_{\vec{p}}$ is the energy of the quasi particle./
/We will see below that as the energy of a quasi particle approaches the Fermi level, it's lifetime goes to infinity./
/The stability of quasi particles at the Fermi level is a crucial tenet of Fermi-liquid theory./


* Spectral function for noninteracting fermions and Fermi liquids [cite:@electrodynamics-corr-el-materials]

In the non-interacting case
\begin{align*}
    A_0(\vec{k} \sigma, \omega) = \delta(\omega - \varepsilon_{\vec{k}})
\end{align*}
/Therefore the energy of the particle is known with certainty when the momentum is given./
/This certainty is a reflection of the fact that in the non-interacting system, the many-body wavefunction is simply given by a single Slater determinant involving products of single-particle wavefunctions $\phi_{\vec{k}\sigma}(\vec{r})$ with associated energy $\varepsilon_{\vec{k}}$./

/In a large class of systems of interacting fermions, known as Fermi liquids, the spectral function can be written as/
\begin{align*}
    A(\vec{k}\sigma,\omega) \approx \frac{Z_{\vec{k}}}{\pi} \frac{1/2\tau_{\vec{k}}}{(\omega - \varepsilon^{* }_{\vec{k}})^2 + (1/2\tau_{\vec{k}})^2} + A_{\rm incoh}(\vec{k}\sigma,\omega)
\end{align*}
/Here $\tau_{\vec{k}}$ is a lifetime, $\varepsilon_{\vec{k}}^{ * }$ is a renormalized energy, and $Z_{\vec{k}}$ is a constant which is a positive number between 0 and 1./
/Compared to the non-interacting case, there is still a peak in the spectral function, represented by the first term in./
/However, the peak is now a Lorentzian instead of a delta function./
/Also, the width of the peak has broadened, the area under the peak has decreased (from $1$ to $Z_{\vec{k}}$), and the single-particle energies are renormalized./
/There is also an additional term $A_{\rm incoh}$ representing a continuum (i.e. not a peak) which must be there if $Z_{\vec{k}} \neq 1$ for the [[id:221d040f-d5df-4901-a0b0-b4acb905fd55][sum rule]] $\int_{-\infty}^{\infty} A(\vec{k}\sigma,\omega) \d \omega = 1$ to be satisfied./
/A finite (i.e. not infinite) lifetime $\tau_{\vec{k}}$ (note that in the limit $\tau_{\vec{k}} \to \infty$ the first term in the spectral function again becomes a delta function) reflects the fact that in the presence of interactions the many-body wavefunction is a sum of many Slater determinants,
which can be thought of as resulting from the fact that the interactions make the fermions scatter between different single-particle states $\phi_{\vec{k}\sigma}(\vec{r})$ (i.e. states with different $\vec{k}$); the lifetime $\tau_{\vec{k}}$ is thus a measure of the time between such scattering events./
/In a Fermi liquid, $1 / \tau_{\vec{k}}$ approaches zero very fast as $|\vec{k}|$ approaches the Fermi momentum $\vec{k}_{\rm F}$./
/As a consequence, fermions close to the Fermi surface scatter very little, which can be shown to imply that treating them as essentially non-interacting is still qualitatively correct for many purposes./
/This is a very significant result since many experimentally measurable properties are at low temperatures essentially determined by the fermions at or near the Fermi surface./
/These properties are therefore not qualitatively changed by the electron-electron interactions./
/The entities having energy $\varepsilon_{\vec{k}}^{ *}$ and lifetime $\tau_{\vec{k}}$ are called quasi-particles./
/As already noted, fermion systems in which this picture holds are called Fermi liquids, and the theory describing them is known as Fermi liquid theory./
/We note that the identification and investigation of interacting fermionic systems which do not obey Fermi liquid theory is an important research question in current many-body physics./
/Such non-Fermi liquids by definition have a spectral function which can not be approximated by the equation above and as a result they can therefore not be qualitatively understood in terms of a picture of non-interacting fermions./
/One prominent example of a non-Fermi liquid is the so-called Luttinger liquid which occurs in one spatial dimension./


* Further reading

- Principles [cite:@Fermi-liquid-theory-principles]
- Lifshitz and Pitaevskii [cite:@statistical-physics-theory-Lifshitz-and-Pitaevskii]
- Abrikosov and Dzyaloshinski [cite:@methods-of-quantum-field-theory-in-statistical-physics]

#+begin_export latex
  \clearpage
#+end_export


* References
#+print_bibliography:
