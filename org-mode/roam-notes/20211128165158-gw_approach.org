:properties:
:ID:       20ae6109-bf27-4ce9-9df7-133b11dc039d
:end:
#+title: GW approach
#+setupfile: ~/gems-backup/org-mode/general-org-header.org
#+filetags: :condensed-matter-theory:

\begin{align*}
    \Sigma = G \cdot W
\end{align*}

- 1st order expansion of self-energy in $W$ (screened Coulomb interaction within the [[id:3ff209e3-3e15-40be-ae92-32dd8638f4cc][RPA]])
- For the excited state properties of weakly correlated materials
- Does not capture the Mott transition [cite:@plasmons-in-strongly-correlated-systems-Erik]
- /It proved to be useful for moderately correlated materials./
  /In particular, its quasiparticle self-consistent version successfully predicted band gaps of several semiconductors./
  /However, its perturbative treatment of correlations does not allow one to describe Mott insulators in a paramagnetic state, nor strongly correlated metals./ [cite:@electrodynamics-corr-el-materials]

* Further reading
- [[id:398b16eb-c520-41b1-91ce-fe7475e1633b][GW+DMFT approach]]
- [cite:@GW-approximation]
