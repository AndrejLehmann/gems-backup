"
" \newcommand{\pr}[1]{\!\left(#1\right)}
" \newcommand{\of}[1]{\!\left(#1\right)}
" \newcommand{\sbr}[1]{\!\lbrack#1\rbrack}
" \newcommand{\cbr}[1]{\!\lbrace#1\rbrace}
" \newcommand{\eqdef}{\!\stackrel{\mathrm{def}}{=}}
" \newcommand{\el}{\mathrm{e}} % euler
" \newcommand{\C}{\mathrm{C}} % Coulumb
" \newcommand{\up}{\uparrow}
" \newcommand{\down}{\downarrow}
" \renewcommand{\i}{\mathrm{i}} % imaginary unit
"

if exists('g:quicktex_tex') && !exists('g:quicktex_math')
    let g:quicktex_math = {}
    finish
elseif !exists('g:quicktex_tex') && exists('g:quicktex_math')
    let g:quicktex_tex = {}
    finish
elseif exists('g:quicktex_tex') && exists('g:quicktex_math')
    finish
endif

if !get(g:, 'quicktex_usedefault', 1)
    finish
endif

" Keyword mappings are simply a dictionary. Dictionaries are of the form
" "quicktex_" and then the filetype. The result of a keyword is either a
" literal string or a double quoted string, depending on what you want.
"
" In a literal string, the result is just a simple literal substitution
"
" In a double quoted string, \'s need to be escape (i.e. "\\"), however, you
" can use nonalphanumberical keypresses, like "\<CR>", "\<BS>", or "\<Right>"
"
" Unfortunately, comments are not allowed inside multiline vim dictionaries.
" Thus, sections and comments must be included as entries themselves. Make
" sure that the comment more than one word, that way it could never be called
" by the ExpandWord function

" Math Mode Keywords {{{

let g:quicktex_math = {
    \' ' : "\<ESC>:call search('<+.*+>')\<CR>\"_c/+>/e\<CR>",
\'Section: Lowercase Greek Letters' : 'COMMENT',
    \'alpha'   : '\alpha ',
    \'beta'    : '\beta ',
    \'gamma'   : '\gamma ',
    \'delta'   : '\delta ',
    \'epsilon' : '\epsilon ',
    \'eps'     : '\varepsilon ',
    \'zeta'    : '\zeta ',
    \'eta'     : '\eta ',
    \'theta'   : '\theta ',
    \'iota'    : '\iota ',
    \'kappa'   : '\kappa ',
    \'lambda'  : '\lambda ',
    \'gl'      : '\lambda ',
    \'mu'      : '\mu ',
    \'nu'      : '\nu ',
    \'xi'      : '\xi ',
    \'omega'   : '\omega ',
    \'pi'      : '\pi ',
    \'rho'     : '\rho ',
    \'sigma'   : '\sigma ',
    \'tau'     : '\tau ',
    \'upsilon' : '\upsilon ',
    \'gu'      : '\upsilon ',
    \'phi'     : '\varphi ',
    \'chi'     : '\chi ',
    \'psi'     : '\psi ',
    \'hbar'     : '\hbar ',
    \
\'Section: Uppercase Greek Letters' : 'COMMENT',
    \'Alpha'   : '\Alpha ',
    \'Beta'    : '\Beta ',
    \'Gamma'   : '\Gamma ',
    \'Delta'   : '\Delta ',
    \'Epsilon' : '\Epsilon ',
    \'Zeta'    : '\Zeta ',
    \'Eta'     : '\Eta ',
    \'Theta'   : '\Theta ',
    \'Iota'    : '\Iota ',
    \'Kappa'   : '\Kappa ',
    \'Lambda'  : '\Lambda ',
    \'Mu'      : '\Mu ',
    \'Nu'      : '\Nu ',
    \'Xi'      : '\Xi ',
    \'Omega'   : '\Omega ',
    \'Pi'      : '\Pi ',
    \'Rho'     : '\Rho ',
    \'Sigma'   : '\Sigma ',
    \'Tau'     : '\Tau ',
    \'Upsilon' : '\Upsilon ',
    \'Phi'     : '\Phi ',
    \'Chi'     : '\Chi ',
    \'Psi'     : '\Psi ',
    \
\'Section: Set Theory' : 'COMMENT',
    \'|N'    : '\mathbb{N}',
    \'|Z'    : '\mathbb{Z}',
    \'|Q'    : '\mathbb{Q}',
    \'|R'    : '\mathbb{R}',
    \'|C'    : '\mathbb{C}',
    \'|A'    : '\mathbb{A}',
    \'|F'    : '\mathbb{F}',
    \'sub'  : '\subset',
    \'subeq'  : '\subseteq',
    \'in'    : '\in ',
    \'nin'   : '\not\in ',
    \'cup'   : '\cup ',
    \'or'    : '\lor ',
    \'cap'   : '\cap ',
    \'and'   : '\land ',
    \'uni'   : '\cup ',
    \'conc'  : '\circ ',
    \'circ'  : '\circ ',
    \'smin'  : '\setminus ',
    \'neg'   : '\neg ',
    \'set'   : '\{<+++>\}<++>',
    \'empty' : '\varnothing ',
    \'pair'  : '(<+++>, <++>)<++>',
    \'dots'  : '\dots ',
    \
\'Section: Logic' : 'COMMENT',
    \'exists'  : '\exists ',
    \'nexists' : '\nexists ',
    \'forall'  : '\forall ',
    \'imp'     : '\implies ',
    \'biimp'   : '\Longleftrightarrow ',
    \'equiv'   : '\equiv ',
    \'iff'     : '\iff ',
    \
\'Section: Relations' : 'COMMENT',
    \'lt'      : '< ',
    \'gt'      : '> ',
    \'apr'     : '\approx',
    \'leq'     : '\leq ',
    \'geq'     : '\geq ',
    \'nl'      : '\nless ',
    \'ng'      : '\ngtr ',
    \'nleq'    : '\nleq ',
    \'ngeq'    : '\ngeq ',
    \'neq'     : '\neq ',
    \
\'Section: Arrows' : 'in combination with \newcommand in tex file',
    \'up'     : '\up ',
    \'down'   : '\down ',
    \
\'Section: Operations' : 'COMMENT',
    \'frac'  : '\frac{<+++>}{<++>}<++>',
    \'recip' : '\frac{1}{<+++>}<++>',
    \'dot'   : '\cdot ',
    \'pm'    : '\pm ',
    \'grad'  : '\grad',
    \'pw'    : "\<BS>^{<+++>}<++>",
    \'dag'    : "\<BS>^{\\dagger}<+++>",
    \'sq'    : "\<BS>^2 ",
    \'inv'   : "\<BS>^{-1} ",
    \'times' : '\times ',
    \
\'Section: Delimiters' : 'in combination with \newcommand in tex file',
    \'pr'    : '\pr{<+++>}<++>',
    \'sbr'   : '\sbr{<+++>}<++>',
    \'cbr'   : '\cbr{<+++>}<++>',
    \'abs'   : '\abs{<+++>}<++>',
    \'det'   : '\mqty|\, <+++> & <++> \\ <++> & <++> \,|<++>',
    \'tr'    : '\Tr{<+++>}<++>',
    \'sp'    : '\, <+++>',
    \
\'Section: Group Theory' : 'COMMENT',
    \'sdp'   : '\rtimes ',
    \'niso'  : '\niso ',
    \'subg'  : '\leq ',
    \'nsubg' : '\trianglelefteq ',
    \'mod'   : '/ ',
    \
\'Section: Functions' : '\dv, \pdv, \fdv are in physics package; \of is latex newcommand',
    \'to'     : '\to ',
    \'mapsto' : '\mapsto ',
    \'comp'   : '\circ ',
    \'of'     : "\<BS>\\of{<+++>}<++>",
    \'sin'    : '\sin{\pr{<+++>}}<++>',
    \'cos'    : '\cos{\pr{<+++>}}<++>',
    \'tan'    : '\tan{\pr{<+++>}}<++>',
    \'tanh'   : '\tanh{\pr{<+++>}}<++>',
    \'sech'   : '\sech{\pr{<+++>}}<++>',
    \'sinh'   : '\sinh{\pr{<+++>}}<++>',
    \'gcd'    : '\gcd(<+++> ,<++>)<++>',
    \'ln'     : '\ln{<+++>}<++>',
    \'log'    : '\log{\pr{<+++>}}<++>',
    \'sqrt'   : '\sqrt{<+++>}<++>',
    \'case'   : '\begin{cases} <+++> \end{cases}<++>',
    \
\'Section: LaTeX commands' : 'COMMENT',
    \'_'     : "\<BS>_{<+++>}<++>",
    \'txt'   : '\,\text{<+++>}\,<++>',
    \'rm'    : '\rm <+++>',
    \'_rm'     : "\<BS>_{\\rm<+++>}<++>",
    \'ald'   : "\\begin{aligned}\<CR><+++>\<CR>\\end{aligned}",
    \'lb'    : "\<BS>\\label{<+++>}",
    \
\'Section: Fancy Variables' : 'COMMENT',
    \'fA' : '\mathcal{A}',
    \'fD' : '\mathcal{D}',
    \'fE' : '\mathcal{E}',
    \'fG' : '\mathcal{G}',
    \'fO' : '\mathcal{O}',
    \'fN' : '\mathcal{N}',
    \'fP' : '\mathcal{P}',
    \'fT' : '\mathcal{T}',
    \'fC' : '\mathcal{C}',
    \'fM' : '\mathcal{M}',
    \'fF' : '\mathcal{F}',
    \'fZ' : '\mathcal{Z}',
    \'fI' : '\mathcal{I}',
    \'fB' : '\mathcal{B}',
    \'fL' : '\mathcal{L}',
    \'fV' : '\mathcal{V}',
    \
\'Section: Encapsulating keywords' : '\vb, \va, \vu  are in physics package',
    \'hat'   : "\<ESC>Bi\\hat{\<ESC>Els}",
    \'bar'   : "\<ESC>Bi\\overline{\<ESC>Els}",
    \'tild'  : "\<ESC>Bi\\tild{\<ESC>Els}",
    \'vec'   : "\<ESC>Bi\\vec{\<ESC>Els}",
    \'vb*'   : "\\vb{<+++>}<++>",
    \'vb'    : "\\vb*{<+++>}<++>",
    \'va'    : "\\va*{<+++>}<++>",
    \'va*'   : "\\va{<+++>}<++>",
    \'vu'    : "\\vu*{<+++>}<++>",
    \'vu*'   : "\\vu{<+++>}<++>",
    \'bra'   : "\\bra{<+++>}<++>",
    \'ket'   : "\\ket{<+++>}<++>",
    \'braket': '\braket{<+++>}{<++>}<++>',
    \'ev'    : '\ev{<+++>}{<++>}<++>',
    \'expval': '\expval{<+++>}<++>',
    \'xval'  : '\expval{<+++>}<++>',
    \'mel'   : '\mel{<+++>}{<++>}{<++>}<++>',
    \' *'    : "\<BS>^*",
    \
\'Section: Linear Algebra' : 'COMMENT',
    \'GL'     : '\text{GL} ',
    \'SL'     : '\text{SL} ',
    \'com'    : "\<BS>^c ",
    \'matrix' : "\<CR>\\begin{bmatrix}\<CR><+++>\<CR>\\end{bmatrix}\<CR><++>",
    \'vdots'  : '\vdots & ',
    \'ddots'  : '\ddots & ',
    \
\'Section: Constants' : '\e, \i are defined as newcommand',
    \'aleph' : '\aleph ',
    \'inf'   : '\infty ',
    \'eu'    : '\e ',
    \'im'    : '\i ',
    \
\'Section: Operators' : 'COMMENT',
    \'int'    : '\int <+++>\mathop{d <++>}<++>',
    \'lims'   : '\limits_{<+++>}^{<++>} <++>',
    \'intlims': '\int\limits_{<+++>}^{<++>} <++> \mathop{d <++>}<++>',
    \'dv'     : '\dv{<+++>}{<++>}<++>',
    \'dv2'    : '\dv[2]{<+++>}{<++>}<++>',
    \'dvn'    : '\dv[<+++>]{<++>}{<++>}<++>',
    \'pdv'    : '\pdv{<+++>}{<++>}<++>',
    \'fdv'    : '\fdv{<+++>}{<++>}<++>',
    \'lim'    : '\lim_{<+++>}<++>',
    \'sum'    : '\sum ',
    \'prod'   : '\prod ',
    \'limsup' : '\limsup ',
    \'liminf' : '\liminf ',
    \'sup'    : '\sup ',
    \'sinf'   : '\inf ',
\}

" }}}

" LaTeX Mode Keywords {{{

let g:quicktex_tex = {
    \' ' : "\<ESC>:call search('<+.*+>')\<CR>\"_c/+>/e\<CR>",
    \'m' : '\( <+++> \) <++>',
\'Section: Environments' : 'defn,thm in \newtheorem in tex file',
    \'env'   : "\<ESC>Bvedi\\begin{\<ESC>pa}\<CR><+++>\<CR>\\end{\<ESC>pa}\<CR><++>",
    \'exe'   : "\\begin{exercise}{<+++>}\<CR><++>\<CR>\\end{exercise}\<CR><++>",
    \'prf'   : "\\begin{proof}\<CR><+++>\<CR>\\end{proof}\<CR><++>",
    \'thm'   : "\\begin{thm}\<CR><+++>\<CR>\\end{thm}\<CR><++>",
    \'eqnl'  : "\\begin{equation}\\label{<+++>}\<CR><++>\<CR>\\end{equation}\<CR><++>",
    \'eqn'   : "\\begin{equation}\<CR><+++>\<CR>\\end{equation}\<CR><++>",
    \'eq'    : "\\begin{equation*}\<CR><+++>\<CR>\\end{equation*}\<CR><++>",
    \'eqnal' : "\\begin{align}\<CR><+++>\<CR>\\end{align}\<CR><++>",
    \'eqal'  : "\\begin{align*}\<CR><+++>\<CR>\\end{align*}\<CR><++>",
    \'tab'   : "\\begin{tabular}{ l | l}\<CR><+++> & <++> \\\\ \\hline\<CR> <++> & <++>\<CR>\\end{tabular}\<CR><++>",
    \'defnl' : "\\begin{defn}\\label{<+++>}\<CR><++>\<CR>\\end{defn}\<CR><++>",
    \'fig'  : "\\begin{figure}[!]\<CR><+++>\<CR>\\caption{<++>}\<CR>\\end{figure}\<CR><++>",
    \'figl'  : "\\begin{figure}[!]\\label{<+++>}\<CR><++>\<CR>\\caption{<++>}\<CR>\\end{figure}\<CR><++>",
    \'defn'  : "\\begin{defn}\<CR><+++>\<CR>\\end{defn}\<CR><++>",
    \'tikz'  : "\\begin{tikzpicture}\<CR><+++>\<CR>\\end{tikzpicture}\<CR><++>",
    \'enum'  : "\\begin{enumerate}[label=\\textbf{\\arabic*}.]\<CR>\\item <+++>\<CR>\\end{enumerate}\<CR><++>",
    \'itmz'  : "\\begin{itemize}\<CR>\\item <+++>\<CR>\\end{itemize}\<CR><++>",
    \'itm'   : '\item ',
    \'lstpy' : "\\begin{lstlisting}[language=Python]\<CR><+++>\<CR>\\end{lstlisting}\<CR><++>",
    \'lstc' : "\\begin{lstlisting}[language=C]\<CR><+++>\<CR>\\end{lstlisting}\<CR><++>",
    \'incode': '\lstinline|<+++>|',
    \
\'Section: Other Commands' : 'COMMENT',
    \'sec'     : "\\section{<+++>}\<CR><++>",
    \'subsec'  : "\\subsection{<+++>}\<CR><++>",
    \'sec*'    : "\\section*{<+++>}\<CR><++>",
    \'subsec*' : "\\subsection*{<+++>}\<CR><++>",
    \'qt'      : "``<+++>''<++>",
    \'ct'      : "\\cite{<+++>}<++>",
    \'ref'     : "\\ref{<+++>}<++>",
    \'eqref'   : "\\eqref{eq:<+++>}<++>",
    \'appxref' : "\\ref{appx:<+++>}<++>",
    \'txtb'    : '\textbf{<+++>}<++>',
    \'txti'    : '\emph{<+++>}<++>',
    \'txtib'   : '\emph{\textbf{<+++>}}<++>',
    \'txtr'    : '\textcolor{red}{<+++>} <++>',
    \'txtbl'    : '\textcolor{blue}{<+++>} <++>',
    \'lb'      : "\<BS>\\label{<+++>}",
    \'appxlb'  : "\<BS>\\label{appx:<+++>}",
    \'seclb'   : "\<BS>\\label{sec:<+++>}",
    \
\'Section: Common Sets' : 'COMMENT',
    \'bn' : '\(\mathbb{N}\) ',
    \'bz' : '\(\mathbb{Z}\) ',
    \'bq' : '\(\mathbb{Q}\) ',
    \'br' : '\(\mathbb{R}\) ',
    \'bc' : '\(\mathbb{C}\) ',
    \'ba' : '\(\mathbb{A}\) ',
    \'bf' : '\(\mathbb{F}\) ',
    \
\'Section: Greek Letters' : 'COMMENT',
    \'alpha'   : '\(\alpha\) ',
    \'ga'      : '\(\alpha\) ',
    \'beta'    : '\(\beta\) ',
    \'gamma'   : '\(\gamma\) ',
    \'delta'   : '\(\delta\) ',
    \'epsilon' : '\(\varepsilon\) ',
    \'ge'      : '\(\varepsilon\) ',
    \'zeta'    : '\(\zeta\) ',
    \'eta'     : '\(\eta\) ',
    \'theta'   : '\(\theta\) ',
    \'iota'    : '\(\iota\) ',
    \'kappa'   : '\(\kappa\) ',
    \'lambda'  : '\(\lambda\) ',
    \'gl'      : '\(\lambda\) ',
    \'mu'      : '\(\mu\) ',
    \'nu'      : '\(\nu\) ',
    \'xi'      : '\(\xi\) ',
    \'omega'   : '\(\omega\) ',
    \'pi'      : '\(\pi\) ',
    \'rho'     : '\(\rho\) ',
    \'sigma'   : '\(\sigma\) ',
    \'tau'     : '\(\tau\) ',
    \'upsilon' : '\(\upsilon\) ',
    \'gu'      : '\(\upsilon\) ',
    \'phi'     : '\(\varphi\) ',
    \'chi'     : '\(\chi\) ',
    \'psi'     : '\(\psi\) ',
    \
\}

" }}}
