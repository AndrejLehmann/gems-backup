
= Wikis =
* [[wiki1:toDo]]
* [[wiki2:vimwiki]]
* [[wiki3:vim]]
* [[wiki4:my_linux]]
* [[wiki5:my_macOS]]
* [[wiki6:python]]
* [[wiki7:my_programs]]
* [[wiki8:git]]
* [[wiki9:links]]


(:h vimwiki-syntax-links)
(:h Interwiki)

