= Contents =
  - [[#General|General]]
  - [[#Plugins|Plugins]]
  - [[#Tags|Tags]]
  - [[#Spell checking|Spell checking]]




= General =




= Plugins =

* [[vim-gutentags]]
* [[tagbar]]
* [[fzf-tags]]
* [[quicktex]]




= Tags =

tags = methods, classes, variables, and other identifiers

  | keys           | action                                                       |
  |----------------|--------------------------------------------------------------|
  | <C-]>          | jump to tag under the cursor                                 |
  | <C-W>]         | open the tag in a new window                                 |
  | g]             | list all tag matches under the cursor                        |
  | <C-W> g]       | in new window list of tags under the cursor                  |
  | <C-t>          | jump back                                                    |
  | <C-W> g <C-]>  | in a new window jump to tag under the cursor or list matches |
  | <C-W> }        | preview tag under the cursor                                 |
  | <C-W> g}       | preview tag under the cursor or list of tags                 |
  | :tf, :trewind, | #                                                            |
  | :tl, :tn,      | # browse through a list of multiple tag matches              |
  | :tp/:tNext     | #                                                            |

For use with regex (<TAB> completion):
:tag, :sta[g], :pta[g], :ts[elect], :tj[ump], :pts[elect]




= Spell checking =

  | keys        | action                                           |
  |-------------|--------------------------------------------------|
  | <leader>s   | on/off spell checking                            |
  | <C-n>/<C-p> | complete word (commented out in .vimrc)          |
  | z=          | list of alternatives                             |
  | zg          | save word under the cursor in the dictionary     |
  | zug         | undo zg                                          |
  | zw          | mark word as incorrect                           |
  | zuw         | undo zw                                          |
  | ]s          | move the cursor to the next missspelled word     |
  | [s          | move the cursor to the previous missspelled word |




= Profile vim =

  :profile start profile.log
  :profile func *
  :profile file *

do whate make vim act slowly

  :profdel

  $ vim profile.log


== Profile plugins at start up ==
https://github.com/hyiltiz/vim-plugins-profile

Use python to profile (add -p flat to plot a bar chart)
$ python <(curl -sSL https://raw.githubusercontent.com/hyiltiz/vim-plugins-profile/master/vim-plugins-profile.py)

or R
$ bash <(curl -sSL https://raw.githubusercontent.com/hyiltiz/vim-plugins-profile/master/vim-plugins-profile.sh)
