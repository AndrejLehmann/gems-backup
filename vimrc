
runtime! plugin/sensible.vim  " run sensible.vim at the beginning, so one can put overrides below

autocmd! bufwritepost .vimrc source %  " source .vimrc after saving



" ----- Remap Esc to Caps Lock -----

"if $DISPLAY
"Maps Esc to the Caps lock key when Vim entered
"au VimEnter * !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'  " now included in .bashrc

"Returns normal functionality to caps lock when Vim quited
"au VimLeave * !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'

"endif



" ----- Allow us to use Ctrl-s and Ctrl-q as keybinds -----

silent !stty -ixon
autocmd VimLeave * silent !stty ixon  " Restore default behaviour when leaving



" ----- Enable syntax and plugins (for netrw) -----

syntax enable
filetype plugin on



" ---- modifiable -----
set ma


" ----- vim-vinegar initializes dot files hidden -----

let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
let g:netrw_bufsettings = 'noma nomod nu nobl nowrap ro'



" ----- Finding files -----

"  Kritik: tpope/vim-apathy
"Hit tab to :find by partial math
"Use * to make it fuzzy
"set path+=** "Search down into subfolder. Provides tab-completion for all file-related tasks
set wildmode=longest:full
set wildmenu

set rtp+=~/.fzf " for fzf in vim



" ----- Undo settings -----

set undofile
set undodir=~/.vim/undo
set undolevels=5000 "Number of undos
set undoreload=50000 "Number of lines to save for undo
set backupdir=~/.vim/backup/
set directory=~/.vim/backup/



" ----- Leader key -----

let mapleader = ','
let maplocalleader = ','  "Takes effect only for certain types of files.
                          "Here defined equal to mapleader for vimtex.



" ----- Vundel ----- "

set nocompatible "Be iMproved
filetype off

set rtp+=~/.vim/bundle/Vundle.vim "The runtime path

" Keep Plugin commands between vundle#begin/end.

call vundle#begin()

Plugin 'tpope/vim-sensible'  " settings everyone can agree on
Plugin 'vimwiki/vimwiki'
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
"Plugin 'ludovicchabant/vim-gutentags'
Plugin 'lervag/vimtex'
Plugin 'morhetz/gruvbox'       " colorscheme
Plugin 'w0ng/vim-hybrid'       " colorscheme
"Plugin 'christoomey/vim-tmux-navigator'   " C-h,j,k,l pane movements for both vim & tmux.
                                           " Does not work when ssh :'(
"Plugin 'benmills/vimux'       " run shell commands from vim in a tmux pane
"Plugin 'kana/vim-arpeggio'    " for mapping simultaneously pressed multiple keys
Plugin 'tpope/vim-vinegar'     " enhances netrw so project drawers are partially mitigated
"Plugin 'SirVer/ultisnips'     " Track the engine. Tutorials on github.
"Plugin 'tpope/vim-unimpaired'
"Plugin 'honza/vim-snippets'   " Snippets are separated from the engine. Add this if you want them.
"Plugin 'tpope/vim-fugitive'   " git wrapper. For help and bindings :Gstatus, g? .Tutorials on github
"Plugin 'brennier/quicktex'     " ab vim 7.8
Plugin 'tpope/vim-repeat'      " enable repeating supported plugin maps with '.'
                               " Adding support to a plugin by the following command at the end of your map functions:
                               " silent! call repeat#set("\<Plug>MyWonderfulMap", v:count)
"Plugin 'tpope/vim-surround'   " find examples at github
"Plugin 'tpope/vim-commentary' " gc2j : Comment down two lines
                               " gcc  : Comment out the current line
                               " gcip : Comment out the current paragraph
                               " If a file type isn't supported, adjust 'commentstring': autocmd FileType apache setlocal commentstring=#\ %s

"Plugin 'christoomey/vim-system-copy' " cpi' : copy inside single quotes to system clipboard
"                                     " cvi' : paste inside single quotes from system clipboard
"                                     " cP   : copy the current line
"                                     " cV   : paste the content of system clipboard to the next line.
"                                     " Clipboard Utilities: OSX - pbcopy and pbpaste, Linux - xsel

Plugin 'kana/vim-textobj-user' " for the following text objects
Plugin 'bkad/CamelCaseMotion'       " indentifies CamelCase  and underscore_notation as words
"Plugin 'vim-scripts/argtextobj.vim' " text-object 'a' (argument). E.g. 'daa' :  delete around argument
"Plugin 'rbonvall/vim-textobj-latex' " a\ i\ : Inline math surrounded by \( and \). $asfasdf$
                                    " a$ i$ : Inline math surrounded by dollar signs.
                                    " aq iq : Single-quoted text `like this'.
                                    " aQ iQ : Double-quoted text ``like this''.
                                    " ae ie : Environment \begin{…} to \end{…}
"Plugin 'michaeljsmith/vim-indent-object' "V <count>ai : around Indentation level and line above.
                                         "V <count>ii : inner Indentation level (no line above).
                                         "V <count>aI : around Indentation level and lines above/below.
                                         "V <count>iI : inner Indentation level (no lines above/below).

"Plugin 'sjl/gundo.vim' " graphical undo history. Requirements: Vim 7.3+, Python support for Vim, Python 2.4+
Plugin 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " Install fzf in command line and vim
Plugin 'junegunn/fzf.vim'
Plugin 'zemja/hlnext'
"Plugin 'gioele/vim-autoswap' " autohandle .swp files:
                              " 1. Is file already open in another Vim session in some other window?
                              "    If so, swap to the window where we are editing that file.
                              " 2. Otherwise, if swapfile is older than file itself, delete it.
                              " 3. Otherwise, open file read-only so we can have a look at it and may save it.

call vundle#end()



" ----- quicktex -----

"so ~/.vim/my_quicktex/tex.vim



" ----- vimwiki -----

" e.g. settings:
"let g:vimwiki_list = [{'path': '~/notes/wiki1/', 'syntax': 'markdown'}]
"au FileType vimwiki setlocal shiftwidth=6 tabstop=6 noexpandtab

" wikis
let wiki_wikis = {}
let wiki_wikis.path = '~/dotFiles/vimwikis/'
let wiki_wikis.index = 'wikis'
let wiki_todo = {}
let wiki_todo.path = '~/dotFiles/vimwikis/toDo/'
let wiki_todo.index = 'toDo'
let wiki_vimwiki = {}
let wiki_vimwiki.path = '~/dotFiles/vimwikis/vimwiki/'
let wiki_vimwiki.index = 'vimwiki'
let wiki_vim = {}
let wiki_vim.path = '~/dotFiles/vimwikis/vim/'
let wiki_vim.index = 'vim'
let wiki_git = {}
let wiki_git.path = "~/dotFiles/vimwikis/git/"
let wiki_git.index = 'git'
let wiki_linux = {}
let wiki_linux.path = "~/dotFiles/vimwikis/my_linux/"
let wiki_linux.index = 'my_linux'
let wiki_macos = {}
let wiki_macos.path = "~/dotFiles/vimwikis/my_macOS/"
let wiki_macos.index = 'my_macOS'
let wiki_programs = {}
let wiki_programs.path = "~/dotFiles/vimwikis/my_programs/"
let wiki_programs.index = 'my_programs'
let wiki_python = {}
let wiki_python.path = "~/dotFiles/vimwikis/python/"
let wiki_python.index = 'python'
let wiki_links = {}
let wiki_links.path = "~/dotFiles/vimwikis/links/"
let wiki_links.index = 'links'
let g:vimwiki_list = [wiki_wikis, wiki_todo, wiki_vimwiki, wiki_vim, wiki_linux, wiki_macos, wiki_python, wiki_programs, wiki_git, wiki_links]

" for bold and italic text
" also read: Vim, Tmux, Italics, and Insanity :
" https://jsatk.us/vim-tmux-italics-and-insanity-9a96b69eeca6
let &t_ZH="\e[3m"
let &t_ZR="\e[23m"



" --- vimtex ---

"let g:vimtex_view_general_viewer = 'zathura'

" if issuing :VimtexView [:h vimtex_viewer_zathura]
"let g:vimtex_view_zathura_hook_view = 'ViewerPosition'
"function! ViewerPosition() abort dict
"  call self.move('0 0')
"  call self.resize('1600 876')
"endfunction


" Disable all warnings [h: vimtex]
let g:vimtex_quickfix_latexlog = {'default' : 0}

" for faster vimtex
let g:tex_fast= "cmM"
let g:tex_conceal = ""

let g:vimtex_quickfix_mode = 1


" ----- fzf -----

nnoremap <silent> <leader>o :FZF<CR>
nnoremap <silent> <leader>O :FZF!<CR>
nnoremap <silent> <leader>: :History:<CR>
nnoremap <silent> <leader>/ :History/<CR>
nnoremap <silent> <leader>m :Marks<CR>
nnoremap <silent> <leader>C :Commands<CR>
nnoremap <silent> <leader>l :BLines<CR>



" ----- UltiSnips -----

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
"let g:UltiSnipsExpandTrigger="<tab>"
"let g:UltiSnipsJumpForwardTrigger="<c-b>"
"let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
"let g:UltiSnipsEditSplit="vertical"



filetype plugin indent on

"" To ignore plugin indent changes, instead use:
"filetype plugin on



" ----- CamelCaseMotion -----

let g:camelcasemotion_key = '<leader>' " usage with leader key like \w
" v-- usual usage
"map <silent> w <Plug>CamelCaseMotion_w
"map <silent> b <Plug>CamelCaseMotion_b
"map <silent> e <Plug>CamelCaseMotion_e
"map <silent> ge <Plug>CamelCaseMotion_ge
"sunmap w
"sunmap b
"sunmap e
"sunmap ge
"omap <silent> iw <Plug>CamelCaseMotion_iw
"xmap <silent> iw <Plug>CamelCaseMotion_iw
"omap <silent> ib <Plug>CamelCaseMotion_ib
"xmap <silent> ib <Plug>CamelCaseMotion_ib
"omap <silent> ie <Plug>CamelCaseMotion_ie
"xmap <silent> ie <Plug>CamelCaseMotion_ie



" ----- argtextobj.vim -----

let g:argumentobject_force_toplevel = 0
" ^-- e.g.: function(1, (20*30)+40, somefunc2(<press 'cia' here>3, 4))
"           function(1, (20*30)+40, somefunc2(<cursor here>4))
"g:argumentobject_force_toplevel = 1
" ^-- e.g.: function(1, (20*30)+40, somefunc2(<press 'cia' here>3, 4))
"           function(1, (20*30)+40, <cursor here>) " sub-level function is deleted because it is a argument in terms of the outer



" ----- vimtex -----

"let g:vimtex_view_general_viewer = 'evince'



" ----- Display unprintable characters -----

"  v--- https://stackoverflow.com/questions/32588604/vim-airline-what-is-trailing1
set list
silent! set listchars=tab:•\ ,trail:•,extends:»,precedes:« " Unprintable chars mapping
" More on highlighting unwanted spaces: http://vim.wikia.com/wiki/Highlight_unwanted_spaces



" ----- Powerline -----

"Powerline fonts are installed in /afs/physnet.uni-hamburg.de/users/th1_li/alehmann/.local/share/fonts



"More natural split opening
set splitbelow
set splitright


"The time in milliseconds that is waited for a key code or mapped key sequence to complete.
set timeoutlen=1000

" ----- highlight the 81 column -----
highlight ColorColumn ctermbg=black
set colorcolumn=81
"                            v-- regex
"call matchadd('ColorColumn','\%81v',100) "only if line size > 81 columns



" ----- highlight search -----

set hlsearch
nnoremap <leader>c :set hlsearch!<CR>





" ----- n/N always searching forward/backward -----

"nnoremap <expr> n 'Nn'[v:searchforward]
"nnoremap <expr> N 'nN'[v:searchforward]
" or
"nnoremap <expr> n (v:searchforward ? 'n' : 'N')
"nnoremap <expr> N (v:searchforward ? 'N' : 'n')



" ----- Line numbers & rel line numbers -----

set number
set relativenumber
"highlight LineNr ctermfg=grey
"highlight CursorLineNr ctermfg=yellow

"Ctrl+n to turn hybrid line number mode (relative/absolute) on/off
function! NumberToggle()
  set number!
  set relativenumber!
endfunc
nnoremap <leader>n :call NumberToggle()<cr>



" ----- Splits -----

" Comment out when vim-tmux-navigator is activated
nnoremap <C-j> <C-W><C-j>
nnoremap <C-k> <C-W><C-k>
nnoremap <C-l> <C-W><C-l>
nnoremap <C-h> <C-W><C-h>



" ----- Tabs -----

set tabstop=8 softtabstop=0 expandtab shiftwidth=2

nnoremap <C-g> gt
nnoremap <C-s> gT
nnoremap <C-1> 1gt
nnoremap <C-2> 2gt
nnoremap <C-3> 3gt
nnoremap <C-4> 4gt
nnoremap <C-5> 5gt
nnoremap <C-6> 6gt
nnoremap <C-7> 7gt
nnoremap <C-8> 8gt
nnoremap <C-9> 9gt
nnoremap <C-0> :tablast<CR>



" ---- gundo settings ----
"         v-- press for graphical undo history
nnoremap <F5> :GundoToggle<CR>"
let g:gundo_width = 60
let g:gundo_preview_height = 40
" for more: https://sjl.bitbucket.io/gundo.vim/



" ----- use the clipboard as the default register -----

"set clipboard=unnamedplus
             "^-- the + register (X Window clipboard [Ctrl-c,Ctrl-v])
"set clipboard=unnamed
              "^-- the * register (X11 primary selection [middle mouse button])
" check for clipboard support:
" $ vim --version | grep clipboard



" https://vim.fandom.com/wiki/Remove_unwanted_spaces
autocmd BufWritePre * %s/\s\+$//e  " automatically remove trailing white spaces
set wrap                           " #
set linebreak                      " # handle desired trailing white spaces
set showbreak=>\ \ \               " #



" ----- Airline configurations -----

set ttimeoutlen=5
set laststatus=2
"let g:airline_theme='bubblegum'
"let g:airline_theme='gruvbox'
let g:airline_theme='onedark' " for atom-dark line
set noshowmode

""Makes Airline look like Poweline
"let g:airline_section_z = airline#section#create(['windowswap', '%3p%% ', 'linenr', ':%3v'])
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

"To type unixcode char: Ctrl+Shift+u => [codenumber]
"some unixcode char: ∥  ⭠ ⭡ » « 🔒  ⌥  Ξ ⮁ ⮃
"                    ⊲ ⊳ ⋖ ⋗ ≪  ≫  ≺  ≻ ⍃ ⍄ ⎨ ⎬ ▶ ◀ ▸ ◂ ▷ ◁ ▹ ◃
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_symbols.crypt = ''
let g:airline_symbols.linenr = ''
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.paste = ''
let g:airline_symbols.whitespace = ''






" ----- color scheme -----

"set cursorline
set background=dark
"set background=light
syntax on
"colorscheme apprentice
"colorscheme hybrid
let g:gruvbox_contrast_dark = 'hard'
"let g:gruvbox_contrast_dark = 'medium'
"let g:gruvbox_contrast_dark = 'soft'
colorscheme gruvbox




" ----- spell checking -----
" have these lines at the end of .vimrc

noremap <leader>s :setlocal spell! spelllang=en_us<CR>

hi clear SpellBad
"hi SpellBad cterm=underline ctermfg=red
hi SpellBad ctermfg=red

" press Ctrl-n or Ctrl-p in insert-mode to complete the word
"set complete+=kspell

" add a word to a dictionary: cursor over the word and type 'zg'



" ----- vimux ----

map <Leader>vl :VimuxRunLastCommand<CR>
map <Leader>ls :VimuxRunCommand("ls")<CR>
" jump to the vimux pane in copy mode
map <Leader>vi :VimuxInspectRunner<CR>
" Zoom the tmux runner pane. With vim-tmux-navigator press C-k to leave
map <Leader>vz :VimuxZoomRunner<CR>
