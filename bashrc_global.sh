# bashrc lines common in all my systems


# Use vi as editor in the command line.
# Can also be activated in .inputrc
set -o vi


# https://wiki.archlinux.de/title/Xmodmap
# find keycode: $ xev
#xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape' # switches Caps Lock and Esc
#                              v-- key stroke with shift
#xmodmap -e 'keycode 47 = colon semicolon' # switches ; and :
#                        ^-- key stroke alone


export PS1="\[\e[31m\]\h\[\e[m\]\[\e[32m\]:\[\e[m\]\[\e[33m\]\w\[\e[m\]\[\e[32m\]\\$\[\e[m\] "  # costom prompt http://ezprompt.net/


# If running interactively, then:
if [ "$PS1" ]; then

     # don't put duplicate lines in the history. See bash(1) for more options
     export HISTCONTROL=ignoredups
     # for setting history length see HISTSIZE and HISTFILESIZE in bash(1)$
     export HISTSIZE=1000
     export HISTFILESIZE=2000

    # check the window size after each command and, if necessary,
    # update the values of LINES and COLUMNS.
    shopt -s checkwinsize

    # append to the history file, don't overwrite it$
    shopt -s histappend


    # If this is an xterm set the title to user@host:dir
    case $TERM in
    xterm*)
        PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
        ;;
    *)
        ;;
    esac

    # enable programmable completion features (you don't need to enable
    # this, if it's already enabled in /etc/bash.bashrc).
    if [ -f /etc/bash_completion ]; then
      . /etc/bash_completion
    fi
fi


# Everytime ssh is launched this routine is runed and tmux status bar name is renamed.
#ssh() {
#    if [ "$(ps -p $(ps -p $$ -o ppid=) -o comm=)" = "tmux" ]; then
#            remote_hostname=$(echo $* | sed -e 's/^\([^\.]*\).*/\1/g')
#            echo "connect to $remote_hostname"
#            tmux rename-window $remote_hostname
#            local_hostname=$(echo $HOSTNAME | sed -e 's/^\([^\.]*\).*/\1/')
#            command ssh "$@"
#            if [ $? -eq 255 ]; then
#                 echo "SSH connection to $remote_hostname failed!"
#                 tmux rename-window "$local_hostname"
#            fi
#     else
#            echo "connect to $@ "
#            command ssh "$@"
#     fi
#}

# v-- added by fuzzy finder
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
# v-- added by myself
export FZF_DEFAULT_OPTS="--color hl+:214,hl:202 --extended --reverse" #--bind=ctrl-j:down,ctrl-k:up"



# v-- Wegen des Fehlers: "Catastrophic error: could not set locale "" to allow processing of multibyte character" beim compilieren des Otsuki-solvers
export LC_ALL=en_US.UTF-8                #
export LANG=en_US.UTF-8                  #
export LANGUAGE=en_US.UTF-8              #
# ---------------------------------------#



# enable color support of ls and also add handy aliases$
if [ -x /usr/bin/dircolors ]; then  # linux
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors/dircolors.ansi-light)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
elif [ -x /usr/local/bin/gdircolors ]; then  # macOS
    test -r ~/.dircolors && eval "$(gdircolors -b ~/.dircolors/dircolors.ansi-light)" || eval "$(gdircolors -b)"
    alias ls='gls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi



cs() { builtin cd "$@" && ls; }

alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias dir='ls -d --color=auto --format=vertical'
alias vdir='ls -d --color=auto --format=long'

export PATH="~/.local/bin/:${PATH}"


### for vterm in emacs (https://github.com/akermu/emacs-libvterm)
vterm_printf(){
    if [ -n "$TMUX" ]; then
        # Tell tmux to pass the escape sequences through
        # (Source: http://permalink.gmane.org/gmane.comp.terminal-emulators.tmux.user/1324)
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

vterm_prompt_end(){
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}
PS1=$PS1'\[$(vterm_prompt_end)\]'

vterm_cmd() {
    local vterm_elisp
    vterm_elisp=""
    while [ $# -gt 0 ]; do
        vterm_elisp="$vterm_elisp""$(printf '"%s" ' "$(printf "%s" "$1" | sed -e 's|\\|\\\\|g' -e 's|"|\\"|g')")"
        shift
    done
    vterm_printf "51;E$vterm_elisp"
}

find_file() {
    vterm_cmd find-file "$(realpath "${@:-.}")"
}

say() {
    vterm_cmd message "%s" "$*"
}

### vterm END
