#+CAPTION: logo
[[file:_media/paip-cover.gif]]

* Paradigms of Artificial Intelligence Programming
  :PROPERTIES:
  :CUSTOM_ID: paradigms-of-artificial-intelligence-programming
  :END:

#+BEGIN_QUOTE
  Case Studies in Common Lisp
#+END_QUOTE

- Peter Norvig

[[https://github.com/norvig/paip-lisp][GitHub]]
[[#paradigms-of-artificial-intelligence-programming][Get Started]]
