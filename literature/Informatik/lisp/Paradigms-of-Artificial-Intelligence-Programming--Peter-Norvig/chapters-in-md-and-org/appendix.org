* Appendix
  :PROPERTIES:
  :CUSTOM_ID: appendix
  :END:

** Obtaining the Code in this Book
   :PROPERTIES:
   :CUSTOM_ID: obtaining-the-code-in-this-book
   :END:

*** FTP: The File Transfer Protocol
    :PROPERTIES:
    :CUSTOM_ID: ftp-the-file-transfer-protocol
    :END:

FTP is a file transfer protocol that is widely accepted by computers
around the world. FTP makes it easy to transfer files between two
computers on which you have accounts. But more importantly, it also
allows a user on one computer to access files on a computer on which he
or she does not have an account, as long as both computers are connected
to the Internet. This is known as /anonymous FTP./

All the code in this book is available for anonymous FTP from the
computer =mkp.com= in files in the directory =pub/norvig=. The file
=README= in that directory gives further instructions on using the
files.

In the session below, the user =smith= retrieves the files from
=mkp.com=. Smith's input is in /slanted font./ The login name must be
/anonymous/, and Smith's own mail address is used as the password. The
command /cd pub/norvig/ changes to that directory, and the command /ls/
lists all the files. The command /mget/ * retrieves all files (the /m/
stands for "multiple"). Normally, there would be a prompt before each
file asking if you do indeed want to copy it, but the /prompt/ command
disabled this. The command /bye/ ends the FTP session.

=% *ftp mkp.com* (or *ftp 199.182.55.2*)=

=Name (mkp.com:smith): *anonymous*=

=331 Guest login ok, send ident as password=

=Password: *smith@cs.stateu.edu*=

=230 Guest login ok, access restrictions apply=

=ftp>*cd pub/norvig*=

=250 CWD command successful.=

=ftp>*ls*=

=...=

=ftp>*prompt*=

=Interactive mode off.=

=ftp>*mget**=

=...=

=ftp> bye=

=%=

Anonymous FTP is a privilege, not a right. The site administrators at
=mkp.com= and at other sites below have made their systems available out
of a spirit of sharing, but there are real costs that must be paid for
the connections, storage, and processing that makes this sharing
possible. To avoid overloading these systems, do not FTP from 7:00 a.m.
to 6:00 p.m. local time. This is especially true for sites not in your
country. If you are using this book in a class, ask your professor for a
particular piece of software before you try to FTP it; it would be
wasteful if everybody in the class transferred the same thing. Use
common sense and be considerate: none of us want to see sites start to
close down because a few are abusing their privileges.

If you do not have FTP access to the Internet, you can still obtain the
files from this book by contacting Morgan Kaufmann at the following:

Morgan Kaufmann Publishers, Inc.

340 Pine Street, Sixth Floor

San Francisco, CA 94104-3205

USA

Telephone 415/392-2665

Facsimile 415/982-2665

Internet mkp@mkp.com

800) 745-7323

Make sure to specify which format you want:

Macintosh diskette ISBN 1-55860-227-5

DOS 5.25 diskette ISBN 1-55860-228-3

DOS 3.5 diskette ISBN 1-55860-229-1

*** Available Software
    :PROPERTIES:
    :CUSTOM_ID: available-software
    :END:

In addition to the program from this book, a good deal of other software
is available. The tables below list some of the relevant AI/Lisp
programs. Each entry lists the name of the system, an address, and some
comments. The address is either a computer from which you can FTP, or a
mail address of a contact. Unless it is stated that distribution is by
/email/ or /Floppy/ or requires a /license,/ then you can FTP from the
contact's home computer. In some cases the host computer and/or
directory have been provided in italics in the comments field. However,
in most cases it should be obvious what files to transfer. First do an
=ls= command to see what files and directories are available. If there
is a file called =README=, follow its advice: do a =get README= and then
look at the file. If you still haven't found what you are looking for,
be aware that most hosts keep their public software in the directory
=pub=. Do a =cd pub= and then another =ls=, and you should find the
desired files.

If a file ends in the suffix =.Z=, then you should give the FTP command
=binary= before transferring it, and then give the UNIX command
=uncompress= to recover the original file. Files with the suffix =.tar=
contain several files that can be unpacked with the =tar= command. If
you have problems, consult your local documentation or system
administrator.

*Knowledge Representation*

| [[][]]     |                                                                           |                                             |  |  |  |  |  |  |  |
|------------+---------------------------------------------------------------------------+---------------------------------------------+--+--+--+--+--+--+--|
| System     | Address                                                                   | Comments                                    |  |  |  |  |  |  |  |
| Babbler    | [[mailto:rsfl@ra.msstate.edu][rsfl@ra.msstate.edu]]                       | /email;/Markov chains/NLP                   |  |  |  |  |  |  |  |
| BACK       | [[mailto:peltason@tubvm.cs.tu-berlin.de][peltason@tubvm.cs.tu-berlin.de]] | /3.5" floppy;/ KL-ONE family                |  |  |  |  |  |  |  |
| Belief     | [[mailto:almond@stat.washington.edu][almond@stat.washington.edu]]         | belief networks                             |  |  |  |  |  |  |  |
| Classic    | [[mailto:dlm@research.att.com][dlm@research.att.com]]                     | /license;/ KL-ONE family                    |  |  |  |  |  |  |  |
| Fol Getfol | [[mailto:fausto@irst.it][fausto@irst.it]]                                 | /tape;/ Weyrauch's FOL system               |  |  |  |  |  |  |  |
| Framekit   | [ehn+@cs.cmu.edu](mailto:ehn+@cs.cmu.edu)                                 | /floppy;/ frames                            |  |  |  |  |  |  |  |
| Framework  | [mkant+@cs.cmu.edu](mailto:mkant+@cs.cmu.edu)                             | /a.gp.cs.cmu.edu:/usr/mkant/Public;/ frames |  |  |  |  |  |  |  |
| Frobs      | [[mailto:kessler@cs.utah.edu][kessler@cs.utah.edu]]                       | frames                                      |  |  |  |  |  |  |  |
| Knowbel    | [[mailto:kramer@ai.toronto.edu][kramer@ai.toronto.edu]]                   | sorted/temporal logic                       |  |  |  |  |  |  |  |
| MVL        | [[mailto:ginsberg@t.stanford.edu][ginsberg@t.stanford.edu]]               | multivalued logics                          |  |  |  |  |  |  |  |
| OPS        | [[mailto:slisp-group@b.gp.cs.cmu.edu][slisp-group@b.gp.cs.cmu.edu]]       | Forgy's OPS-5 language                      |  |  |  |  |  |  |  |
| PARKA      | [[mailto:spector@cs.umd.edu][spector@cs.umd.edu]]                         | frames (designed for connection machine)    |  |  |  |  |  |  |  |
| Parmenides | [[mailto:pshell@cs.cmu.edu][pshell@cs.cmu.edu]]                           | frames                                      |  |  |  |  |  |  |  |
| Rhetorical | [[mailto:miller@cs.rochester.edu][miller@cs.rochester.edu]]               | planning, time logic                        |  |  |  |  |  |  |  |
| SB-ONE     | [[mailto:kobsa@cs.uni-sb.de][kobsa@cs.uni-sb.de]]                         | /license;/ in German; KL-ONE family         |  |  |  |  |  |  |  |
| SNePS      | [[mailto:shapiro@cs.buffalo.edu][shapiro@cs.buffalo.edu]]                 | /license;/ semantic net/NLP                 |  |  |  |  |  |  |  |
| SPI        | [[mailto:cs.orst.edu][cs.orst.edu]]                                       | Probabilistic inference                     |  |  |  |  |  |  |  |
| YAK        | [[mailto:franconi@irst.it][franconi@irst.it]]                             | KL-ONE family                               |  |  |  |  |  |  |  |

*Planning and Learning*

| [[][]]    |                                                                             |                                    |  |  |  |  |  |  |  |
|-----------+-----------------------------------------------------------------------------+------------------------------------+--+--+--+--+--+--+--|
| System    | Address                                                                     | Comments                           |  |  |  |  |  |  |  |
| COBWEB/3  | [[mailto:cobweb@ptolemy.arc.nasa.gov][cobweb@ptolemy.arc.nasa.gov]]         | /email;/ concept formation         |  |  |  |  |  |  |  |
| MATS      | [[mailto:kautz@research.att.com][kautz@research.att.com]]                   | /license;/ temporal constraints    |  |  |  |  |  |  |  |
| MICRO-xxx | [[mailto:waander@cs.ume.edu][waander@cs.ume.edu]]                           | case-based reasoning               |  |  |  |  |  |  |  |
| Nonlin    | [[mailto:nonlin-users-request@cs.umd.edu][nonlin-users-request@cs.umd.edu]] | Tate's planner in Common Lisp      |  |  |  |  |  |  |  |
| Prodigy   | [[mailto:prodigy@cs.cmu.edu][prodigy@cs.cmu.edu]]                           | /license;/ planning and learning   |  |  |  |  |  |  |  |
| PROTOS    | [[mailto:porter@cs.utexas.edu][porter@cs.utexas.edu]]                       | knowledge acquisition              |  |  |  |  |  |  |  |
| SNLP      | [[mailto:weld@cs.washington.edu][weld@cs.washington.edu]]                   | nonlinear planner                  |  |  |  |  |  |  |  |
| SOAR      | [soar-requests/@cs.cmu.edu](mailto:soar-requests/@cs.cmu.edu)               | /license/; integrated architecture |  |  |  |  |  |  |  |
| THEO      | [[mailto:tom.mitchell@cs.cmu.edu][tom.mitchell@cs.cmu.edu]]                 | frames, learning                   |  |  |  |  |  |  |  |
| Tileworld | [[mailto:pollack@ai.sri.com][pollack@ai.sri.com]]                           | planning testbed                   |  |  |  |  |  |  |  |
| TileWorld | [[mailto:tileworld@ptolemy.arc.nasa.gov][tileworld@ptolemy.arc.nasa.gov]]   | planning testbed                   |  |  |  |  |  |  |  |

*Mathematics*

| [[][]]    |                                                                 |                                               |  |  |  |  |  |  |  |
|-----------+-----------------------------------------------------------------+-----------------------------------------------+--+--+--+--+--+--+--|
| System    | Address                                                         | Comments                                      |  |  |  |  |  |  |  |
| JACAL     | [[mailto:jaffer@altdorf.ai.mit.edu][jaffer@altdorf.ai.mit.edu]] | algebraic manipulation                        |  |  |  |  |  |  |  |
| Maxima    | [[mailto:rascal.ics.utexas.edu][rascal.ics.utexas.edu]]         | version of Macsyma; also proof-checker, nqthm |  |  |  |  |  |  |  |
| MMA       | [[mailto:fateman@cs.berkeley.edu][fateman@cs.berkeley.edu]]     | /peoplesparc.berkeley.edu:pub/mma.*/; algebra |  |  |  |  |  |  |  |
| XLispStat | [[mailto:umnstat.stat.umn.edu][umnstat.stat.umn.edu]]           | Statistics; also S Bayes                      |  |  |  |  |  |  |  |

*Compilers and Utilities*

| [[][]]      |                                                           |                                                       |  |  |  |  |  |  |  |
|-------------+-----------------------------------------------------------+-------------------------------------------------------+--+--+--+--+--+--+--|
| System      | Address                                                   | Comments                                              |  |  |  |  |  |  |  |
| AKCL        | [[mailto:rascal.ics.utexas.edu][rascal.ics.utexas.edu]]   | Austin Koyoto Common Lisp                             |  |  |  |  |  |  |  |
| CLX, CLUE   | [[mailto:export.lcs.mit.edu][export.lcs.mit.edu]]         | Common Lisp interface to X Windows                    |  |  |  |  |  |  |  |
| Gambit      | [[mailto:gambit@cs.brandeis.edu][gambit@cs.brandeis.edu]] | /acorn.cs.brandeis.edu:dist/gambit*/; Scheme compiler |  |  |  |  |  |  |  |
| ISI Grapher | [[mailto:isi.edu][isi.edu]]                               | Graph displayer; also NLP word lists                  |  |  |  |  |  |  |  |
| PCL         | [[mailto:arisia.xerox.com][arisia.xerox.com]]             | Implementation of CLOS                                |  |  |  |  |  |  |  |
| Prolog      | [[mailto:aisun1.ai.uga.edu][aisun1.ai.uga.edu]]           | Prolog-based utilities and NLP programs               |  |  |  |  |  |  |  |
| PYTHON      | [ram+@cs.cmu.edu](mailto:ram+@cs.cmu.edu)                 | /a.gp.cs.cmu.edu:/ Common Lisp Compiler and tools     |  |  |  |  |  |  |  |
| SBProlog    | [[mailto:arizona.edu][arizona.edu]]                       | Stony Brook Prolog, Icon, Snobol                      |  |  |  |  |  |  |  |
| Scheme      | [[mailto:altdorf.ai.mit.edu][altdorf.ai.mit.edu]]         | Scheme utilities and compilers                        |  |  |  |  |  |  |  |
| Scheme      | [[mailto:scheme@nexus.yorku.ca][scheme@nexus.yorku.ca]]   | Scheme utilities and programs                         |  |  |  |  |  |  |  |
| SIOD        | [[mailto:bu.edu][bu.edu]]                                 | /users/gjc;/ small scheme interpreter                 |  |  |  |  |  |  |  |
| Utilities   | [[mailto:a.gp.cs.cmu.edu][a.gp.cs.cmu.edu]]               | //usr/mkant/Public/; profiling, def system, etc.      |  |  |  |  |  |  |  |
| XLisp       | [[mailto:cs.orst.edu][cs.orst.edu]]                       | Lisp interpreter                                      |  |  |  |  |  |  |  |
| XScheme     | [[mailto:tut.cis.ohio-state.edu][tut.cis.ohio-state.edu]] | Also mitscheme compiler; sbprolog                     |  |  |  |  |  |  |  |
