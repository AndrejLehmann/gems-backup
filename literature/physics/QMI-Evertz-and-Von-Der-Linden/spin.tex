%\documentclass[12pt,twocolumn,aps,pra,superscriptaddress,floatfix,showpacs,longbibliography]{revtex4-1}
\documentclass[12pt]{article}
\usepackage[onehalfspacing]{setspace}

%%% packages
\usepackage{lmodern} % removes font size restrictions http://ctan.org/pkg/lm
\usepackage{physics}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{txfonts}
\usepackage{lipsum}
\usepackage{color}
\usepackage{wasysym}
\usepackage{hyperref}
\usepackage{bbold}
\usepackage{appendix}
\usepackage{etoolbox} % for additional refs as footnotes
\usepackage{enumitem}
\usepackage{mathrsfs} %curly letters by \mathscr{}
\usepackage{mathtools} % for arrows with text


%%% diagrams
\usepackage{tikz}
\usetikzlibrary{arrows ,automata ,positioning}
%\usetikzlibrary{calc}
%\usetikzlibrary{decorations.pathmorphing}
%\usetikzlibrary{decorations.pathreplacing}
%\usetikzlibrary{decorations.markings}
%\usetikzlibrary{shapes.geometric}
%\usetikzlibrary{positioning}
%\usetikzlibrary{fit}
%\usetikzlibrary{snakes}
%\usetikzlibrary{arrows}
%\usetikzlibrary{arrows.meta}
%\usetikzlibrary{3d}



%%% more readable commands
\newcommand{\pr}[1]{\!\left(#1\right)}
\newcommand{\of}[1]{\!\left(#1\right)}
\newcommand{\sbr}[1]{\!\lbrack#1\rbrack}
\newcommand{\cbr}[1]{\!\lbrace#1\rbrace}
\newcommand{\el}{\mathrm{e}} % euler
\newcommand{\C}{\mathrm{C}} % Coulumb
\newcommand{\up}{\uparrow}
\newcommand{\down}{\downarrow}
\renewcommand{\i}{\mathrm{i}} % imaginary unit


\theoremstyle{plain}
\newtheorem{thm}{Theorem}[section] % theorem numbering depend on section
\theoremstyle{definition}
\newtheorem{defn}[thm]{Definition} % definition numbering depend on theorem numbers
\newtheorem{exmp}[thm]{Example} % example numbering depend on theorem numbers


\setlist{nosep}



\begin{document}

\section{Spin (Wiki)}

(\emph{Close to the original text.})\\
 Quantum mechanical spin contains information about rotational direction, but in a subtle form.
 Quantum mechanics states that the component of angular momentum measured along any direction can only take on the values

 \begin{equation}
   S_i = \hbar s_i, \quad s_i \in \{ - s, -(s-1), \dots, s-1, s \}
 \end{equation}

 For a given quantum state, one could think of a spin vector $S$ whose components are the expectation values of the spin components along each axis, i.e., $ \expval{S} = [ \expval{S_x}, \langle {S_y} \rangle, \expval{S_z}]$.
 This vector then would describe the "direction" in which the spin is pointing, corresponding to the classical concept of the axis of rotation.
 For statistically large collections of particles that have been placed in the same pure quantum state, such as through the use of a Stern–Gerlach apparatus, the spin vector does have a well-defined experimental meaning:
 It specifies the direction in ordinary space in which a subsequent detector must be oriented in order to achieve the maximum possible probability (100$\%$) of detecting every particle in the collection.
 For spin-1/2 particles, this maximum probability drops off smoothly as the angle between the spin vector and the detector increases, until at an angle of 180 $^{ \circ}$ — that is, for detectors oriented in the opposite direction to the spin vector — the expectation of detecting particles from the collection reaches a minimum of 0$\%$.

 Mathematically, quantum-mechanical spin states are described by vector-like objects known as spinors.
 There are subtle differences between the behavior of spinors and vectors under coordinate rotations.
 For example, rotating a spin-1/2 particle by 360$^{ \circ}$ does not bring it back to the same quantum state, but to the state with the opposite quantum phase; this is detectable, in principle, with interference experiments.

The quantum mechanical operators associated with spin-1/2 observables are:

\begin{equation}
  \hat{\mathbf{S}}=\frac{\hbar}{2}\boldsymbol{\sigma}
\end{equation}
where in Cartesian components:
\begin{equation}
  S_x = {\hbar \over 2} \sigma_x,\quad S_y = {\hbar \over 2} \sigma_y,\quad S_z = {\hbar \over 2} \sigma_z \,.
\end{equation}
For the special case of spin-1/2 particles, $ \sigma_{x}$, $ \sigma_{y}$, and $ \sigma_{z}$ are the three Pauli matrices, given by:
\begin{equation}
  \sigma_x =
  \begin{pmatrix}
  0 & 1\\
  1 & 0
  \end{pmatrix}\, \quad
  \sigma_y =
  \begin{pmatrix}
  0 & -i\\
  i & 0
  \end{pmatrix} \, \quad
  \sigma_z =
  \begin{pmatrix}
  1 & 0\\
  0 & -1
  \end{pmatrix}\,.
\end{equation}

Each of the (hermitian) Pauli matrices has two eigenvalues, +1 and -1.
The corresponding normalized eigenvectors are:

\begin{equation}
\begin{array}{lclc}
  \psi_{x+} = \left|\frac{1}{2}, \frac{+1}{2}\right\rangle_x =
    \displaystyle\frac{1}{\sqrt{2}} \!\!\!\!\! & \begin{pmatrix}{1}\\{1}\end{pmatrix},  &
  \psi_{x-} = \left|\frac{1}{2}, \frac{-1}{2}\right\rangle_x =
    \displaystyle\frac{1}{\sqrt{2}} \!\!\!\!\! & \begin{pmatrix}{-1}\\{1}\end{pmatrix}, \\
  \psi_{y+} = \left|\frac{1}{2}, \frac{+1}{2}\right\rangle_y =
    \displaystyle\frac{1}{\sqrt{2}} \!\!\!\!\! & \begin{pmatrix}{1}\\{i}\end{pmatrix},  &
  \psi_{y-} = \left|\frac{1}{2}, \frac{-1}{2}\right\rangle_y =
    \displaystyle\frac{1}{\sqrt{2}} \!\!\!\!\! & \begin{pmatrix}{1}\\{-i}\end{pmatrix}, \\
  \psi_{z+} = \left|\frac{1}{2}, \frac{+1}{2}\right\rangle_z =                          &
    \begin{pmatrix}{1}\\{0}\end{pmatrix}, &
  \psi_{z-} = \left|\frac{1}{2}, \frac{-1}{2}\right\rangle_z =                          &
    \begin{pmatrix}{0}\\{1}\end{pmatrix}.
\end{array}
\end{equation}

By the postulates of quantum mechanics, an experiment designed to measure the electron spin on the $x$-, $y$-, or $z$-axis can only yield an eigenvalue of the corresponding spin operator ($S_x$, $S_y$, $S_z$) on that axis, i.e. $ \hbar /2$ or $-\hbar /2$.
The quantum state of a particle (with respect to spin), can be represented by a two component spinor:

\begin{equation}
  \psi = \begin{pmatrix} a + bi \\ c + di \end{pmatrix}.
\end{equation}
When the spin of this particle is measured with respect to a given axis (in this example, the ($x$-axis),
the probability that its spin will be measured as $ \hbar /2$ is just $\left\vert \langle \psi_{x+} \vert \psi \rangle \right\vert ^2$.
Correspondingly, the probability that its spin will be measured as $- \hbar /2$ is just $\left\vert \left\langle\left. \psi_{x-} \right\vert \psi \right\rangle \right\vert ^2$.
 Following the measurement, the spin state of the particle will collapse into the corresponding eigenstate.
 As a result, if the particle's spin along a given axis has been measured to have a given eigenvalue, all measurements will yield the same eigenvalue (since $\left\vert \left\langle\left. \psi_{x+} \right\vert \psi_{x+} \right\rangle \right\vert ^2 = 1$, etc), provided that no measurements of the spin are made along other axes.

The operator to measure spin along an arbitrary axis direction is easily obtained from the Pauli spin matrices.
Let $u = (u_x, u_y, u_z)$ be an arbitrary unit vector. Then the operator for spin in this direction is simply
\begin{equation}
  S_u = \frac{\hbar}{2}(u_x\sigma_x + u_y\sigma_y + u_z\sigma_z) \, .
\end{equation}


\end{document}
