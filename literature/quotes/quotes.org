P.Colemann: Introduction to Many Body Physics. recited from ref. [3]
"Indeed, the act of creativity in physics research is very similar to the artistic process.
 Sometimes, scientific and artistic revolutions even go hand-in-hand, for the desire for change and revolution often crosses between art and the sciences."

P. W. Anderson: More is different: broken symmetry and the nature of the hierarchical structure of science.:
A dialog in Paris in the 1920's:
#+begin_quote
FITZGERALD: The rich are differnt from us.
HEMINGWAY: Yes, they have more money.
#+end_quote
