;;; equation-numbering-in-LaTeX-fragments.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Andrej Lehmann
;;
;; Author: Andrej Lehmann <https://github.com/andrejthealien>
;; Maintainer: Andrej Lehmann <alehmann@physnet.uni-hamburg.de>
;; Created: October 18, 2021
;; Modified: October 18, 2021
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/andrejthealien/equation-numbering-in-LaTeX-fragments
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;      src: [[https://kitchingroup.cheme.cmu.edu/blog/2016/11/07/Better-equation-numbering-in-LaTeX-fragments-in-org-mode/]]
;;
;;; Description:
;;      In org-mode we can use LaTeX equations, and toggle an overlay that shows what the rendered equation will look like.
;;      One thing that has always bothered me though, is that each fragment is created in isolation.
;;      That means numbering is almost always wrong, and typically with each numbered equation starting with (1).
;;      Here we fix that.
;;      Fixing it means we have to find a way to not create each fragment image in isolation; each one needs a context that enables the numbering to be correct.
;;      The idea is simple: we just figure out in advance what the numbering for each equation should be, and then figure out how to get that information to the image generation.
;;
;;; Code:

(defun org-renumber-environment (orig-func &rest args)
  (let ((results '())
        (counter -1)
        (numberp))

    (setq results (loop for (begin .  env) in
                        (org-element-map (org-element-parse-buffer) 'latex-environment
                          (lambda (env)
                            (cons
                             (org-element-property :begin env)
                             (org-element-property :value env))))
                        collect
                        (cond
                         ((and (string-match "\\\\begin{equation}" env)
                               (not (string-match "\\\\tag{" env)))
                          (incf counter)
                          (cons begin counter))
                         ((string-match "\\\\begin{align}" env)
                          (prog2
                              (incf counter)
                              (cons begin counter)
                            (with-temp-buffer
                              (insert env)
                              (goto-char (point-min))
                              ;; \\ is used for a new line. Each one leads to a number
                              (incf counter (count-matches "\\\\$"))
                              ;; unless there are nonumbers.
                              (goto-char (point-min))
                              (decf counter (count-matches "\\nonumber")))))
                         (t
                          (cons begin nil)))))

    (when (setq numberp (cdr (assoc (point) results)))
      (setf (car args)
            (concat
             (format "\\setcounter{equation}{%s}\n" numberp)
             (car args)))))

  (apply orig-func args))

(advice-add 'org-create-formula-image :around #'org-renumber-environment)

;; You can remove the advice like this.
;(advice-remove 'org-create-formula-image #'org-renumber-environment)


(provide 'equation-numbering-in-LaTeX-fragments)
;;; equation-numbering-in-LaTeX-fragments.el ends here
