;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Andrej Lehmann"
      user-mail-address "alehmann@physnet.uni-hamburg.de")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. The the default is `doom-one':
;;(setq doom-theme 'doom-one)
;;(setq doom-theme 'doom-Iosvkem)
;;(setq doom-theme 'doom-nova)
;;(setq doom-theme 'doom-tomorrow-night)
;;(setq doom-theme 'doom-vibrant)
;;(setq doom-theme 'doom-monokai-classic)
;;(setq doom-theme 'doom-dracula)
;;(setq doom-theme 'doom-molokai)
;;(setq doom-theme 'doom-horizon)
(setq doom-theme 'doom-henna)
;;(setq doom-theme 'doom-horizon)
;;(setq doom-theme 'doom-gruvbox)
(use-package! doom-themes
  ;:ensure t
  :config
  (setq doom-gruvbox-dark-variant "hard"))

;(after! doom-themes
;  (setq doom-themes-enable-bold t
;        doom-themes-enable-italic t))

(setq display-line-numbers-type 'relative) ; values: nil, t, 'visual, 'relative


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;(use-package ess :ensure t :init (require 'ess-site))

;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.



;;; ENSURE ENVIRONMENT VARIABLES INSIDE EMACS ARE THE SAME AS IN THE SHELL
;; E.g. one can just launch emacs from terminal.


;;;; START SERVER WHEN STARTING EMACS
(require 'server)
(or (server-running-p) ; server-running-p does not appear in the manual, so conceivably this could break with future versions of Emacs.
    (server-start))



;;; FULL SCREEN

(custom-set-variables
 '(initial-frame-alist (quote ((fullscreen . maximized)))))



;;; SCROLLING

(set-variable 'scroll-conservatively 3) ; smoother scrolling for C-u, C-d



;;; TEXT FORMAT

(setq-default line-spacing 0.25)
(set-face-attribute 'default nil
                    :height 165);125);175)

(setq tab-width 3)



;;; TAGS

(setq org-tag-alist '(("condensed-matter-theory" . ?t)
                      ("data-science" . ?d)
                      ("numerical-math" . ?n)
                      ("math" . ?m)
                      ("programming" . ?p)))



;;; ORG MODE

;(setq org-num-mode t) ; numbering of headings

;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/gems-backup/org-mode/")
(setq org-id-locations-file "~/gems-backup/org-mode/.orgids")

;; Where should `org-cycle' emulate TAB.
(setq org-cycle-emulate-tab 'white) ;; Only in completely white lines.
                                    ;; So I can collapse sections

(with-eval-after-load 'org (global-org-modern-mode))

(after! org
  (setq org-hide-emphasis-markers t)    ; Don't show '*' and '/' when text is wraped in '*' and '/' to make it bold and italic.

  ;; render greek letters, sup and super scripts
  (setq org-pretty-entities t)
  (setq org-pretty-entities-include-sub-superscripts nil)
  (setq org-use-sub-superscripts nil) ; values: t, nil, '{} (limit rendering to curly brackets notation)

  (setq org-todo-keywords
        '((sequence "TODO(t)" "INPROG(i)" "WAIT(w)" "|" "DONE(d)" "CANCELLED(c)")
          (sequence "[ ](T)" "[-](I)" "[?](W)" "|" "[X](D)")
         )
        org-todo-keyword-faces
        '(("WAIT" :foreground "#9f7efe")
          ("INPROG" :foreground "#0098dd")
          ("DONE" :foreground "#50a14f")
          ("CANCELLED" :foreground "#ff6480")
          ("[-]" :foreground "#0098dd")
          ("[?]" :foreground "#9f7efe")
         )
        ;; Diese TagListe wird vom #+TAGS: im file ueberschrieben.
        org-tag-alist
        '(("condensed-matter-theory")
          ("programming")
          ("python")
          ("C")
          ("C++")
          ("data-science")
          ("computing-in-science")
          ("math")
          ("emacs")
          ("org-mode")
          ("physics"))
  )

  ;; remaping
  ;(map! :map org-mode-map
  ;      :n "M-j" #'org-metadown  ; just an example, it's already implemented
  ;      :n "M-k" #'org-metaup)   ; just an example, it's already implemented

  ;; inline images
  (setq org-startup-with-inline-images t)

  ;; clocking work time
  (setq org-clock-idle-time 7)


  ;;; LATEX

  ;; For latex preview in the org file
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0)) ;1.7))

  (setq org-highlight-latex-and-related '(entities)) ; simple syntax highlighting by bold face


  ;(setq org-format-latex-options (plist-put org-format-latex-options :foreground "Violet"))
  ;(setq org-format-latex-options (plist-put org-format-latex-options :foreground "Yellow"))
  ;(setq org-format-latex-options (plist-put org-format-latex-options :foreground "Green"))
  (setq org-format-latex-options (plist-put org-format-latex-options :foreground "#00FF80")) ; yellow: F6F702, purple: EA21FF, cyan: 21FFEA, red: FB2874, yellow/green: B6E63E, red: FD371F, blue: 0098DD, coral: FF6480, orange: FEBC2E, green: 00FF80, red: FF3377, pale green: 9DF2A7, light blue: 37D2F8
  (setq org-format-latex-options (plist-put org-format-latex-options :background "Transparent"))

  ;(setq org-preview-latex-default-process 'dvipng)
  ;(setq org-latex-create-formula-image-program 'imagemagick)
  (setq org-latex-create-formula-image-program 'dvipng)

  ;(add-hook 'org-mode-hook 'org-fragtog-mode) ; Fragment previews are disabled for editing when your cursor steps onto them, and re-enabled when the cursor leaves.
  (add-hook 'org-roam-buffer-postrender-functions
            (lambda () (org--latex-preview-region (point-min) (point-max))))  ; so that latex formulas are previewed in the Backlinks buffer

  ;; Packages
  (add-to-list 'org-latex-packages-alist '("" "mathtools" t))
  (add-to-list 'org-latex-packages-alist '("" "minted"))

  ;; For code blockes
  (require 'org-tempo) ; expands snippets
  (require 'ox-latex)  ; exporter
  (setq org-latex-listings 'minted)

  (setq org-latex-pdf-process
      ;'("latexmk -f -pdf -%latex -interaction=nonstopmode -output-directory=%o %f"))
      '("latexmk -pdflatex='lualatex -shell-escape -interaction nonstopmode' -pdf -f  %f"))
      ;'("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
      ;  "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
      ;  "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
)



;;; ORG CDLATEX

;; For configuration examples see:  [[https://github.com/cdominik/cdlatex][cdlatex README]]

;; For easier writing of latex code
(add-hook 'org-mode-hook 'turn-on-org-cdlatex)

;; Modify math symbols list triggert by `
(setq cdlatex-math-symbol-alist
      '((?c ("\\chi"))
        (?C ("\\Chi"))
        ;(?< ("\\leftarrow" "\\Leftarrow" "\\longleftarrow" "\\Longleftarrow"))
        ;(?> ("\\rightarrow" "\\Rightarrow" "\\longrightarrow" "\\Longrightarrow"))
        ))

;; make math templates for LaTeX command in ~/gems-backup/org-mode/math-latex-header.org
(setq cdlatex-command-alist
      '(("ket" "Insert \\ket{}" "\\ket{?}" cdlatex-position-cursor nil nil t)
        ("bra" "Insert \\bra{}" "\\bra{?}" cdlatex-position-cursor nil nil t)
        ("braket" "Insert \\braket{}{}" "\\braket{?}{}" cdlatex-position-cursor nil nil t)
        ("mat" "Insert \\mat{}" "\\mat{?}" cdlatex-position-cursor nil nil t)
        ("vec" "Insert \\vec{}" "\\vec{?}" cdlatex-position-cursor nil nil t)
        ("dag" "Insert \\dagger" "\\dagger" cdlatex-position-cursor nil nil t)
        ("sum" "Insert \\sum_{}^{}" "\\sum_{?}^{}" cdlatex-position-cursor nil nil t)))

(setq cdlatex-math-modify-alist
      '((?m "\\mat" nil t t nil )))


(setq cdlatex-make-sub-superscript-roman-if-pressed-twice t)

;; For correct numbering of equations in Latex fragments in org-mode
;(load-file "~/gems-backup/doom-emacs/equation-numbering-in-LaTeX-fragments.el")



;;; ORG ROAM

;: how to configure
(use-package! org-roam
      :ensure t
      :config
      (org-roam-db-autosync-mode)
      :custom
      (org-roam-directory (file-truename "/Users/andrejthealien/gems-backup/org-mode/roam-notes/"))
;      :bind (("C-c n l" . org-roam-buffer-toggle)
;             ("C-c n f" . org-roam-node-find)
;             ("C-c n g" . org-roam-graph)
;             ("C-c n i" . org-roam-node-insert)
;             ("C-c n c" . org-roam-capture)
;             ;; Dailies
;             ("C-c n j" . org-roam-dailies-capture-today))
;

      (require 'org-roam-protocol) ; if using org-roam-protocol
      (require 'org-roam-export))  ; so roam nodes can be exported to latex due to issues with resolving the roam links.


;; templates
(setq org-roam-capture-templates
      '(("d" "default" plain
         ;"%?" ;(file "~/path/to/file/used/as/template")
         "#+begin_export latex\n  \\clearpage \\tableofcontent \\clearpage\n#+end_export\n\n%?\n\n#+begin_export latex\n  \\clearpage\n#+end_export\n\n* References\n#+print_bibliography:" ; %? is the point of entrance
         :if-new
         (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                    "#+title: ${title}\n#+setupfile: ~/gems-backup/org-mode/general-org-header.org\n#+filetags: %^{Tag}")
         :unnarrowed t)))

;; for pretty UI viewing of graphs
(use-package! websocket
    :after org-roam)
(use-package! org-roam-ui
    :after org-roam ;; or :after org

;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;    :hook (after-init . org-roam-ui-mode)

    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t)

    ;:bind (("C-c n r u" . org-roam-ui-mode))
    )
;(setq org-roam-ui-browser-function #'xwidget-webkit-browse-url)
(map! :leader :n "n r u" #'org-roam-ui-mode)

; On might need to run this command
(defun resolve-id-links-for-export ()
    "Add Org Roam files to org-id locations so that export works."
    (interactive)
    (org-id-update-id-locations (org-roam-list-files)))



;;; CODE BLOCKS
;(add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)
;(setq org-babel-python-command "python3")

(add-to-list 'org-structure-template-alist
             '("py" . "src python :results output :exports both")) ; <py TAB
(add-to-list 'org-structure-template-alist
             '("hs" . "src haskell :exports both")) ; <hs TAB



;;; [[ULTRA-TEX][https://www2.math.uconn.edu/~vince/tex/ultra.html]]
;; But org CDLaTex is enough for now
;(setq load-path (cons "/Users/andrejthealien/programme/ultratex/lisp" load-path))
;(require 'light)
;(require 'ultex-setup)



;;; ORG-REVEAL
;; For pretty presentaions

(require 'ox-reveal)
;(setq org-reveal-root "///Users/andrejthealien/gems-backup/org-mode/reveal.js-master")  ; For some reason it doesn't work. So use the online one.
(setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js")



;;; TRAMP

(require 'tramp)
(setq tramp-default-method "ssh")
(add-to-list 'tramp-remote-path 'tramp-own-remote-path)
;(setq tramp-remote-path '(tramp-default-remote-path "/bin" "/usr/bin" "/sbin" "/usr/sbin" "/usr/local/bin" "/usr/local/sbin" "/local/bin" "/local/freeware/bin" "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin" "/opt/bin" "/opt/sbin" "/opt/local/bin" "/opt/sw/python/3.7.4/bin"))


(setq projectile-project-search-path '("~/projects/" "~/literature/"))



;; Flyspell will run a series of predicate functions to determine if a word should be spell checked. You can add your own with set-flyspell-predicate!
(set-flyspell-predicate! '(markdown-mode gfm-mode)
  #'+markdown-flyspell-word-p)

(defun +markdown-flyspell-word-p ()
  "Return t if point is on a word that should be spell checked.
Return nil if on a link url, markup, html, or references."
  (let ((faces (doom-enlist (get-text-property (point) 'face))))
    (or (and (memq 'font-lock-comment-face faces)
             (memq 'markdown-code-face faces))
        (not (cl-loop with unsafe-faces = '(markdown-reference-face
                                            markdown-url-face
                                            markdown-markup-face
                                            markdown-comment-face
                                            markdown-html-attr-name-face
                                            markdown-html-attr-value-face
                                            markdown-html-tag-name-face
                                            markdown-code-face)
                      for face in faces
                      if (memq face unsafe-faces)
                      return t)))))



;;; LSP (Language server protocol)

; To combine LSP with TRAMP read [[https://emacs-lsp.github.io/lsp-mode/page/remote/]]

; from [[https://github.com/daviwil/emacs-from-scratch/blob/master/Emacs.org]]
(defun efs/lsp-mode-setup ()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode)) ; gives you at the top of the file a hierarhical view of where you are in the file

(use-package! lsp-mode
  :commands (lsp lsp-deferred)  ; corrected, previously it was (lsp . lsp-deferred)
  :hook
    (lsp-mode efs/lsp-mode-setup)
    ;(python-mode . lsp)
  :init     (setq lsp-keymap-prefix "C-l")  ; Or 'C-l', 's-l', 'C-c l'
  :custom   (lsp-headerline-breadcrumb-enable t)
  :config   (lsp-enable-which-key-integration t))

;(connection-local-set-profile-variables
;  'remote-bash
;  '((explicit-shell-file-name . "/bin/bash")
;    (explicit-bash-args . ("-i"))))

; UI enhancements
(use-package! lsp-ui
  :hook (lsp-mode . lsp-ui-mode) ; corrected, previously it was (lsp-mode . lsp-ui-mode)
  :config
  (setq lsp-ui-doc-show-with-cursor t)
  (setq lsp-ui-doc-position 'top)
  (setq lsp-ui-doc-delay 2)
  :commands lsp-ui-mode)

; lsp-treemacs-symbols, lsp-treemacs-references, lsp-treemacs-error-list
(use-package! lsp-treemacs
  :commands lsp-treemacs-errors-list
  :after lsp)

; Search with lsp-ivy-workspace-symbol, lsp-ivy-global-workspace-symbol
(use-package! lsp-ivy)


; Increase responsiveness
(setq gc-cons-threshold 34000000)
(setq read-process-output-max (* 1024 1024)) ;; 1mb
(setq lsp-idle-delay 1.500)





;;; COMPANY (complete anything)
;; A nicer in-buffer completion interface than completion-at-point

(use-package! company
  :after lsp-mode
  :hook  (lsp-mode . company-mode)
  ; TAB now completes the selection and initiates completion at the current location if needed
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection)
         :map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 3) ; how many charecters to type before completion suggestion are shown
  (company-idle-delay 0.7)) ; delay before completion suggestions are shown
; enhance the look of the completions with icons and better overall presentation.
(use-package! company-box
  :hook (company-mode . company-box-mode))



;;; DEBUGGING

(use-package! dap-mode
  ; Uncomment the config below if you want all UI panes to be hidden by default!
  ; :custom
  ; (lsp-enable-dap-auto-configure nil)
  ; :config
  ; (dap-ui-mode 1)

  :config
  ; Bind 'SPC c l d' to 'dap-hydra' for easy access
  (general-define-key
    :keymaps 'lsp-mode-map
    :prefix lsp-keymap-prefix
    "d" '(dap-hydra t :wk "debugger")))

(setq dap-auto-configure-features '(sessions locals controls tooltip))



;;; PYTHON

(use-package! python-mode
  :ensure t
  :hook (python-mode . lsp-deferred)
  :custom
  ; NOTE: Set these if Python 3 is called "python3" on your system!
  (python-shell-interpreter "python3"))

(require 'dap-python)
;; if you installed debugpy, you need to set this
;; https://github.com/emacs-lsp/dap-mode/issues/306
(setq dap-python-debugger 'debugpy)
(setq dap-python-executable "python3")


;(require 'py-autopep8)
;(add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
;(setq py-autopep8-options '("--max-line-length=80"))
(setq lsp-pyls-plugins-autopep8-enabled  nil)
(setq lsp-pylsp-plugins-autopep8-enabled nil)

;; You can use the pyvenv package to use virtualenv environments in Emacs.
;; The pyvenv-activate command should configure Emacs to cause lsp-mode and dap-mode to use the virtual environment when they are loaded,
;; just select the path to your virtual environment before loading your project.
; not tested
(use-package! pyvenv
  :config
  (pyvenv-mode 1))



;;; HASKELL
;; Haskell programs can be profiled and debuged by [[https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/index.html][ghc]] (Glasgow Haskell Compiler).

(after! lsp-haskell
  (setq lsp-haskell-formatting-provider "ormolu")) ; installed via cabal (cabal is part of ghcup)
  ;(setq lsp-haskell-formatting-provider "brittany"))



;;; TERMINALS

; open terminal in the current working directory
(require 'shell-here)
(define-key (current-global-map) "\C-c!!" 'shell-here)

;; Siehe (defun evil-collection-vterm-toggle-send-escape)
;(after! (vterm evil-collection)
;  (add-hook!
;   'vterm-mode-hook
;   ;; remap ESC to M-ESC. M-ESC sends then ESC to emacs
;   (evil-collection-define-key 'insert 'vterm-mode-map
;     ;; for CLI emacs
;     (kbd "ESC <escape>") (lookup-key evil-insert-state-map (kbd "<escape>"))
;     ;; for GUI emacs
;     (kbd "M-<escape>") (lookup-key evil-insert-state-map (kbd "<escape>"))
;   )
;   ;; send ESC to vterm instead of to emacs
;   (evil-collection-define-key 'insert 'vterm-mode-map
;     (kbd "<escape>") 'vterm--self-insert  ; <escape> has to be removed from evil-collection-key-blacklist and added to evil-collection-key-whitelist
;   )
;  )
;)



;; for package gdb-mi from weirdNox/emacs-gdb
;; couldnt make emacs-gdb work
;(fmakunbound 'gdb)
;(fmakunbound 'gdb-enable-debug)


;;; Avy
;; Package for advanced movement and editing
(setq avy-all-windows t) ; extend searches over all panes



;;; GNUPLOT

(use-package! gnuplot)

;; specify the gnuplot executable (if other than /usr/bin/gnuplot)
(setq gnuplot-program "/usr/local/bin/gnuplot")

;; automatically open files ending with .gp, .gnu or .gnuplot in gnuplot mode
(setq auto-mode-alist
      (append '(("\\.\\(gp\\|gnu\\|gnuplot\\)$" . gnuplot-mode)) auto-mode-alist))



;;; LEDGER

(setq ledger-schedule-file "~/gems-backup/ledger/scheduled-transactions-simplified.ledger")
; use with =(ledger-schedule-upcoming)=



;;; PROECTILE

(setq projectile-project-search-path '("~/gems-backup/projects/coding-exercises/codewars/"
                                       "~/gems-backup/projects/coding-exercises/leetcode/"
                                       "~/projects/cis/numerische-mathematik/"
                                       "~/projects/physics/Mos-bachelor-thesis/"
                                       "~/projects/physics/PhD/c2h_c2f/"
                                       "~/projects/physics/PhD/kagome/"
                                       "~/projects/physics/PhD/topological-systems/Haldande-model/"
                                       "~/teaching/JEA/"
                                       "~/gems-backup/literature/physics/"
                                       "~/gems-backup/literature/informatik/"
                                       "~/gems-backup/org-mode/roam-notes/"
                                       "~/gems-backup/ledger/"))



;;; ORG-JOURNAL

(setq org-journal-dir "~/gems-backup/org-mode/journal/"
      org-journal-date-format "%a, %Y-%m-%d"
      org-journal-date-prefix "#+TITLE: "
      org-journal-time-prefix "* "
      org-journal-file-format "%Y-%m-%d.org")



(provide 'config)

;;; config.el ends here

