#+TITLE: Accounting

[[https://devhints.io/ledger][cheet sheet]]
[[https://www.danherrera.dev/posts/191214-ledgercli-intro/][tutorial]]
[[https://rolfschr.github.io/gswl-book/latest.html][advanced tutorial]]
[[https://ledger-cli.org/doc/ledger-mode.pdf][documentation ledger mode]]

|----------------------------+-----------------------------------------------------|
| command                    | description                                         |
|----------------------------+-----------------------------------------------------|
| =(ledger-schedule-upcoming)= | opens buffer for copy-pasting recurrent transaction |
|----------------------------+-----------------------------------------------------|

* Double-entry Accounting

/Double-entry accounting is a standard bookkeeping approach./
/In accounting, every type of expense or income and every “place” which holds monetary value is called an “account” (think “category”)./
/Example accounts may be “Groceries”, “Bike”, “Holidays”, “Checking Account of Bank X”, “Salary” or “Mortgage”./
/In double-entry accounting, one tracks the flow of money from one account to another./
/An amount of money always figures twice (“double”) in the books: At the place where it came from and at the place where it was moved to./
/That is, adding $1000 here means removing $1000 from there at the same time./
/In consequence, the total balance of all accounts is always zero./
/Money is never added to an account without stating where the exact same amount came from./
/However, more than two accounts may be involved in one transaction./


* Ledger
#+name: all
#+begin_src ledger :noweb yes
2024/12/15 * Opening Balance
    assets:GLS-account                        1000.00 EUR
    equity:opening-balances                  -1000.00 EUR
#+end_src

#+RESULTS: all
:          1000.00 EUR  assets:GLS-account
:         -1000.00 EUR  equity:opening-balances
: --------------------
:                    0



* Income

#+begin_src ledger :cmdline reg -M -n Income :noweb yes
<<all>>
#+end_src

#+RESULTS:


* Expenses

#+begin_src ledger :cmdline -M -n reg Expenses :noweb yes
<<all>>
#+end_src

#+RESULTS:



#+begin_src ledger :cmdline bal -p "this month" ^Expenses :noweb yes
<<all>>
#+end_src

#+RESULTS:



#+begin_src ledger :cmdline -M -p "this month" reg Expenses and not \(Fixed or Groceries\) :noweb yes
<<all>>
#+end_src

#+RESULTS:


#+begin_src ledger :cmdline -M -p "last month" reg Expenses and not \(Fixed or Groceries\) :noweb yes
<<all>>
#+end_src

#+RESULTS:


** Groceries

#+begin_src ledger :cmdline -M -p "this month" reg Groceries :noweb yes
<<all>>
#+end_src

#+RESULTS:



#+begin_src ledger :cmdline -M -p "last month" reg Groceries :noweb yes
<<all>>
#+end_src

#+RESULTS:


#+begin_src ledger :cmdline -M -p "this month" reg Flink or Gorillas or Getir or EatOut :noweb yes
<<all>>
#+end_src

#+RESULTS:
